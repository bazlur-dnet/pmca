<?php

/**
 * @file: Create embedded content for "The Standards" Listing.
 * @desc: This script can be used to (re)create the Standards Listing Top embedded content.
 */

$entity_type = 'embeddedcontent';
$machine_name = 'standards_listing_top';

// Check to see if the entity for the Domain Listing page exists.
$query = new EntityFieldQuery();
$query->entityCondition('entity_type', $entity_type);
$query->propertyCondition('machine_name', $machine_name);
$results = $query->execute();

// Create entity if it does not exist.
if (empty($results[$entity_type])) {
  // Prepare a list of values for the new entity.
  $values = array(
    'machine_name' => $machine_name,
    'title' => 'Standards Listing Top',
    'field_embeddedcontent_body' => array(LANGUAGE_NONE => array(
      0 => array(
        'value' => 'This block is not configured yet.'
      ),
    )),
    'type' => 'text',
    'status' => TRUE,
  );

  /** @var EmbeddedContentEntity $entity */
  $entity = entity_create($entity_type, $values);
  $entity->save();
}
