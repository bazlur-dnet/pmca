<?php

/**
 * @file: Create Service Providers.
 * @desc: This script can be used to (re)create the service provider entities.
 *
 * @NOTE-TO-SELF: Next time you have 69 columns, do not do a custom import!!!
 */

/**
 * Import service providers from a CSV file.
 */
function __us_providers_import_csv($file_path, $mapping) {
  $handle = fopen($file_path, 'r');
  if (!$handle) {
    watchdog('providers-import', 'Could not open file.');
    return;
  }

  $headers = fgetcsv($handle, 8192, ',');
  if (reset($headers) != '_OC') {
    watchdog('providers-import', 'Wrong headers?');
    return;
  }

  while (($row = fgetcsv($handle, 8192, ',')) !== FALSE) {
    $row_values = array();
    $row_score_values = array();

    foreach ($headers as $col_delta => $col_key) {
      if (empty($mapping[$col_key])) {
        continue;
      }

      // Skip cols starting with "_".
      if ($mapping[$col_key][0] == '_') {
        continue;
      }

      $field_name = $mapping[$col_key];
      $field_value_key = NULL;
      if (strpos($field_name, ':')) {
        $col_parts = explode(':', $mapping[$col_key]);
        $field_name = $col_parts[0];
        $field_value_key = $col_parts[1];
      }

      $field_value = NULL;
      if (isset($field_value_key) && $field_name != 'field_prv_domain_scores') {
        // Map strings to Boolean+N/A.
        $value = str_replace("\n", " ", trim($row[$col_delta]));
        if ($value == 'N/A' || $value == '/A') {
          $field_value = '-1';
        }
        else if (empty($value) || $value == 'No') {
          $field_value = '0';
        }
        else {
          $field_value = '1';
        }
      }
      else {
        // Trim whitespace and replace new lines with space,
        // fields don't support new lines.
        $field_value = str_replace("\n", " ", trim($row[$col_delta]));
      }

      if (!isset($field_value)) {
        continue;
      }

      switch ($field_name) {
        case 'name':
          $row_values[$field_name] = $field_value;
          break;

        case 'field_prv_location':
          if ($field_value == 'N/A') {
            $_tmp_col_key = array_search('_country', $mapping);
            $_tmp_col_delta = array_search($_tmp_col_key, $headers);
            // Map strings to Boolean+N/A.
            $_tmp_field_value = str_replace("\n", " ", trim($row[$_tmp_col_delta]));
            $terms = taxonomy_term_load_multiple(array(), array('name' => $_tmp_field_value));
            if (!empty($terms)) {
              $term = reset($terms);
              $row_values[$field_name][LANGUAGE_NONE][]['tid'] = $term->tid;
            }
          }
          else {
            $terms = taxonomy_term_load_multiple(array(), array('name' => $field_value));
            if (!empty($terms)) {
              $term = reset($terms);
              $row_values[$field_name][LANGUAGE_NONE][]['tid'] = $term->tid;
            }
          }
          break;

        case 'field_prv_domain_scores':
          if ($field_value != 'N/A') {
            $score_values = array(
              'domain_id' => $field_value_key,
              'score' => $field_value,
            );
            $row_score_values[] = entity_create('score', $score_values);
          }
          break;

        case 'field_prv_website':
          $row_values[$field_name][LANGUAGE_NONE][]['url'] = $field_value;
          break;

        case 'field_prv_no_shelters_operated':
        case 'field_prv_total_no_of_staff':
          if ($field_value != 'N/A') {
            $row_values[$field_name][LANGUAGE_NONE][0]['value'] = $field_value;
          }
          break;

        default:
          if ($field_value_key == '3-STATE-BOOL') {
            switch ($field_value) {
              case '0':
                $row_values[$field_name][LANGUAGE_NONE][]['value'] = 'yes';
                break;
              case '1':
                $row_values[$field_name][LANGUAGE_NONE][]['value'] = 'no';
                break;
              case '-1':
                // Do not save any value for "N/A".
                break;
            }
          }
          else if (isset($field_value_key)) {
            if (!empty($field_value)) {
              $row_values[$field_name][LANGUAGE_NONE][]['value'] = $field_value_key;
            }
          }
          else {
            $row_values[$field_name][LANGUAGE_NONE][0]['value'] = $field_value;
          }
          break;
      }
    }

    // Save the score entities.
    if (!empty($row_score_values)) {
      /** @var ScoreEntity $score_entity */
      foreach ($row_score_values as $score_entity) {
        $score_entity->save();
        $row_values['field_prv_domain_scores'][LANGUAGE_NONE][]['target_id'] = $score_entity->identifier();
      }
    }

    $row_values = array(
        'type' => 'simple',
        'created' => REQUEST_TIME,
        'changed' => REQUEST_TIME,
      ) + $row_values;

    /** @var ProviderEntity $new_entity */
    $new_entity = entity_create('provider', $row_values);
    $new_entity->save();
  }
  fclose($handle);
}


$column_field_mapping = array(
  '_OC' => '_uuid',  // Renamed from "N/A". Unused.
  '2.4' => 'name',
  '1.4' => '_country',  // Redundant, 1.5 is a child of 1.4.
  '1.5' => 'field_prv_location',
  '2.16' => 'field_prv_catchment_area',
  '2.13.1' => 'field_prv_services_offered:rescue-services',
  '2.13.2' => 'field_prv_services_offered:institutional-care',
  '2.13.3' => 'field_prv_services_offered:repatriation',
  '2.13.4' => 'field_prv_services_offered:family-reunification',
  '2.13.5' => 'field_prv_services_offered:reintegration',
  '2.13.6' => 'field_prv_services_offered:legal-services',
  '2.13.7' => 'field_prv_services_offered:life-skill-education',
  '2.13.8' => 'field_prv_services_offered:hiv-rehabilitation',
  '2.13.9' => 'field_prv_services_offered:health-services',
  '2.13.10' => 'field_prv_services_offered:non-formal-education',
  '2.13.11' => 'field_prv_services_offered:livelihood-support',
  '2.15.1' => 'field_prv_type_clients_served:male-children-under-18',
  '2.15.2' => 'field_prv_type_clients_served:female-children-under-18',
  '2.15.3' => 'field_prv_type_clients_served:male-adults-18-or-over',
  '2.15.4' => 'field_prv_type_clients_served:female-adults-18-or-over',
  '2.15.5' => 'field_prv_type_clients_served:other-victims-of-exploitation',
  '2.15.6' => 'field_prv_type_clients_served:missing-children',
  '2.14' => 'field_prv_cost_service_provision:free-for-trafficking-survivors',
  '4.1' => 'field_prv_no_shelters_operated',
  '5.1-5.8' => 'field_prv_shelter_capacity',

  // Renamed from "5.1-5.8" -- it shows up multiple times.
  '5.1-5.8—served-1' => 'field_prv_ages_genders_served:males-under-18',
  '5.1-5.8—served-2' => 'field_prv_ages_genders_served:females-under-18',
  '5.1-5.8—served-3' => 'field_prv_ages_genders_served:males-over-18',
  '5.1-5.8—served-4' => 'field_prv_ages_genders_served:females-over-18',

  '4.8.1' => 'field_prv_shelter_serv_offered:health-services',
  '4.8.2' => 'field_prv_shelter_serv_offered:hiv-services',
  '4.8.3' => 'field_prv_shelter_serv_offered:counseling-services',
  '4.8.4' => 'field_prv_shelter_serv_offered:food-clothing-kit',
  '4.8.5' => 'field_prv_shelter_serv_offered:non-formal-education',
  '4.8.6' => 'field_prv_shelter_serv_offered:life-skill-education',
  '4.8.7' => 'field_prv_shelter_serv_offered:vocational-training',
  '4.8.8' => 'field_prv_shelter_serv_offered:repatriation',
  '4.8.9' => 'field_prv_shelter_serv_offered:legal-services',
  '4.8.10' => 'field_prv_shelter_serv_offered:family-reunification',
  '5.0.1' => 'field_prv_trafficking_surv_only:3-STATE-BOOL',
  '2.18.1' => 'field_prv_how_to_contact:through-our-website',
  '2.18.2' => 'field_prv_how_to_contact:referral-from-police-or-judiciary-courts',
  '2.18.3' => 'field_prv_how_to_contact:referral-from-other-government-agency',
  '2.18.4' => 'field_prv_how_to_contact:referral-from-other-service-providers',
  '2.18.5' => 'field_prv_how_to_contact:word-of-mouth-from-public',
  '2.18.6' => 'field_prv_how_to_contact:we-advertise-in-the-media',
  '2.18.7' => 'field_prv_how_to_contact:referral-from-other-survivors',
  '2.18.8' => 'field_prv_how_to_contact:survivors-can-approach-us-themselves',
  '2.18.9' => 'field_prv_how_to_contact:outreach-services',
  '2.5' => 'field_prv_address',
  '2.19.1' => 'field_prv_funding_sources:government',
  '2.19.2' => 'field_prv_funding_sources:private-sources',
  '2.19.3' => 'field_prv_funding_sources:public-donations',
  '2.19.4' => 'field_prv_funding_sources:ngo-donors-national',
  '2.19.5' => 'field_prv_funding_sources:ngo-donors-international',
  '2.19.6' => 'field_prv_funding_sources:fees-for-services',
  '5.9' => 'field_prv_total_no_of_staff',
  '_RS' => 'field_prv_inv_surv_repatriation:3-STATE-BOOL',  // Headers added.
  '_FR' => 'field_prv_inv_fam_reunification:3-STATE-BOOL',  // Headers added.
  '9.7.1' => 'field_prv_type_health_orgs:government-hospital',
  '9.7.2' => 'field_prv_type_health_orgs:private-hospitals',
  '9.7.3' => 'field_prv_type_health_orgs:private-practitioners',
  'Domain 1' => 'field_prv_domain_scores:1',
  'Domain 2' => 'field_prv_domain_scores:2',
  'Domain 3' => 'field_prv_domain_scores:3',
  'Domain 4' => 'field_prv_domain_scores:4',
  'Domain 5' => 'field_prv_domain_scores:5',
  'Domain 6' => 'field_prv_domain_scores:6',
  'Domain 7' => 'field_prv_domain_scores:7',
  'Domain 8' => 'field_prv_domain_scores:8',
  'Domain 9' => 'field_prv_domain_scores:9',
  'Domain 10' => 'field_prv_domain_scores:10',
  'Domain 11' => 'field_prv_domain_scores:11',
  'Domain 12' => 'field_prv_domain_scores:12',
  'Domain 13' => 'field_prv_domain_scores:13',
  'Domain 14' => 'field_prv_domain_scores:14',
  'Domain 15' => 'field_prv_domain_scores:15',
  'Domain 16' => 'field_prv_domain_scores:16',
  'Domain 17' => 'field_prv_domain_scores:17',
);

/** @var string $script_directory */
__us_providers_import_csv($script_directory . '/create.providers.csv', $column_field_mapping);
