<?php

/**
 * @file: Create "Location" taxonomy terms.
 * @desc: This script can be used to (re)create the "Location" taxonomy terms.
 */

function __us_create_location_term($values, $parent_tid = NULL) {
  $term = new stdClass();
  $term->name = $values['name'];
  $term->vocabulary_machine_name = 'location';

  // @TODO: Add bounds support?
  $term->field_loc_coordinates[LANGUAGE_NONE][] = array(
    'geo_type' => 'point',
    'lat' => $values['lat'],
    'lon' => $values['lon'],
  );

  $term->weight = 0;
  if (!empty($values['weight']) && is_numeric($values['weight'])) {
    $term->weight = $values['weight'];
  }

  $term->parent = array(0);
  if (!empty($parent_tid) && is_numeric($parent_tid)) {
    $term->parent = array($parent_tid);
  }

  // Save the term.
  $term = _us_taxonomy__term_save($term);

  if (!empty($term->tid) && !empty($values['children'])) {
    foreach ($values['children'] as $weight => $values) {
      $values['weight'] = $weight;
      __us_create_location_term($values, $term->tid);
    }
  }
}

$location_taxonomy = array(
  array(
    'name' => 'India',
    'lat' => '22.0',
    'lon' => '79.0',
    'children' => array(
      array(
        'name' => 'West Bengal',
        'lat' => '24.0',
        'lon' => '88.0',
      ),
      array(
        'name' => 'Delhi',
        'lat' => '28.65195',
        'lon' => '77.23149',
      ),
      array(
        'name' => 'Maharashtra',
        'lat' => '19.5',
        'lon' => '76.0',
      ),
      array(
        'name' => 'Bihar',
        'lat' => '25.75',
        'lon' => '85.75',
      ),
      array(
        'name' => 'Uttar Pradesh',
        'lat' => '27.25',
        'lon' => '80.75',
      ),
    ),
  ),
  array(
    'name' => 'Nepal',
    'lat' => '28.0',
    'lon' => '84.0',
    'children' => array(
      array(
        'name' => 'Kathmandu',
        'lat' => '27.70169',
        'lon' => '85.3206',
      ),
      array(
        'name' => 'Sindhupalchowk',
        'lat' => '27.77',
        'lon' => '85.7',
      ),
      array(
        'name' => 'Makawanpur',
        'lat' => '27.41187',
        'lon' => '85.14203',
      ),
    ),
  ),
  array(
    'name' => 'Bangladesh',
    'lat' => '24.0',
    'lon' => '90.0',
    'children' => array(
      array(
        'name' => 'Jessore',
        'lat' => '23.16971',
        'lon' => '89.21371',
      ),
    ),
  ),
);
foreach ($location_taxonomy as $weight => $values) {
  $values['weight'] = $weight;
  __us_create_location_term($values);
}
