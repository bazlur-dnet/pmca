<?php

/**
 * @file: Create embedded content for the homepage.
 * @desc: This script can be used to (re)create the homepage embedded content.
 */

$entity_type = 'embeddedcontent';

$entities = array(
  'home_intro' => array(
    'title' => 'Homepage Intro',
    'body' => '<em>The missing child alert</em> initiative aims to promote safe, effective and appropriate provision of care and services to survivors of child trafficking across South Asia.',
  ),
  'home_domains' => array(
    'title' => 'Map & Directory',
    'body' => 'This block is not configured. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam placerat eros at lorem interdum, vulputate volutpat nulla aliquam. Pellentesque ac iaculis turpis.',
  ),
  'home_standards' => array(
    'title' => 'The Standards',
    'body' => 'This block is not configured. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam placerat eros at lorem interdum, vulputate volutpat nulla aliquam. Pellentesque ac iaculis turpis.',
  ),
);
foreach ($entities as $machine_name => $info) {
  // Check to see if the entity for the Domain Listing page exists.
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', $entity_type);
  $query->propertyCondition('machine_name', $machine_name);
  $results = $query->execute();

  // Create entity if it does not exist.
  if (empty($results[$entity_type])) {
    // Prepare a list of values for the new entity.
    $values = array(
      'machine_name' => $machine_name,
      'title' => $info['title'],
      'field_embeddedcontent_body' => array(LANGUAGE_NONE => array(
        0 => array(
          'value' => $info['body'],
        ),
      )),
      'type' => 'text',
      'status' => TRUE,
    );

    /** @var EmbeddedContentEntity $entity */
    $entity = entity_create($entity_type, $values);
    $entity->save();
  }
}
