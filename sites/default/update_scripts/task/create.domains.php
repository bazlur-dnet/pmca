<?php

/**
 * @file: Create Domains.
 * @desc: This script can be used to (re)create the Domain entities.
 */

// Please note that domain names will be prefixed with "Domain #", where # will
// be the array key + 1.
$domains_list = array(
  array(
    'name' => 'Rescue and Rescue-Related Services',
    'icon' => 'rescue',
  ),
  array(
    'name' => 'Shelter Home Capacity and Facilities',
    'icon' => 'shelter',
  ),
  array(
    'name' => 'Location-Related Details',
    'icon' => 'location',
  ),
  array(
    'name' => 'Child Participation in Shelter Homes',
    'icon' => 'child',
  ),
  array(
    'name' => 'Safety And Security at Shelter Homes',
    'icon' => 'safety',
  ),
  array(
    'name' => 'Health Care Services at Shelter Homes',
    'icon' => 'healthcare',
  ),
  array(
    'name' => 'Psychological Counseling at Shelter Homes',
    'icon' => 'counseling',
  ),
  array(
    'name' => 'Case Management and Survivor Empowerment',
    'icon' => 'case',
  ),
  array(
    'name' => 'Education and Vocational Training',
    'icon' => 'education',
  ),
  array(
    'name' => 'Staffing',
    'icon' => 'staffing',
  ),
  array(
    'name' => 'Training/Orientation of Staff Members',
    'icon' => 'training',
  ),
  array(
    'name' => 'Staff Management',
    'icon' => 'staff',
  ),
  array(
    'name' => 'Confidentiality and Privacy',
    'icon' => 'confidentiality',
  ),
  array(
    'name' => 'Repatriation',
    'icon' => 'repatriation',
  ),
  array(
    'name' => 'Family Tracing and Reunification',
    'icon' => 'family',
  ),
  array(
    'name' => 'Reintegration/Alternative Reintegration',
    'icon' => 'reintegration',
  ),
  array(
    'name' => 'Legal Aid',
    'icon' => 'legal',
  ),
);
foreach ($domains_list as $weight => $domain_info) {
  $domain_number = $weight + 1;
  $values = $domain_info + array(
    'type' => 'simple',
    'number' => $domain_number,
  );

  /** @var DomainEntity $entity */
  $entity = entity_create('domain', $values);
  $entity->save();
}
