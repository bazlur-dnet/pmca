<?php

/**
 * @file: Create Menu Links.
 * @desc: This script can be used to (re)create the menu links.
 */


// Delete all links from menu.
_us_menu__delete_links('user-menu');

$link = array(
  'link_path' => 'admin/content',
  'link_title' => 'Content',
  'weight' => -45,
);
_us_menu__create_link($link, 'user-menu');

$link = array(
  'link_path' => 'admin/people',
  'link_title' => 'People',
  'weight' => -44,
);
_us_menu__create_link($link, 'user-menu');

$link = array(
  'link_path' => 'admin/settings',
  'link_title' => 'Settings',
  'weight' => -43,
);
_us_menu__create_link($link, 'user-menu');


// Delete all links from menu.
_us_menu__delete_links('main-menu');

$link = array(
  'link_path' => '<front>',
  'link_title' => 'Homepage',
  'weight' => -40,
);
_us_menu__create_link($link, 'main-menu');

$link = array(
  'link_path' => 'directory',
  'link_title' => 'Map & Directory',
  'weight' => -30,
);
_us_menu__create_link($link, 'main-menu');

$link = array(
  'link_path' => 'standards',
  'link_title' => 'The Standards',
  'weight' => -20,
);
_us_menu__create_link($link, 'main-menu');

$link = array(
  'link_path' => 'eform/submit/contact',
  'link_title' => 'Contact',
  'weight' => -10,
);
_us_menu__create_link($link, 'main-menu');


// Delete all links from menu.
_us_menu__delete_links('menu-footer');

$link = array(
  'link_path' => '<front>',
  'link_title' => 'Terms and Conditions',
  'weight' => 0,
);
_us_menu__create_link($link, 'main-menu');
