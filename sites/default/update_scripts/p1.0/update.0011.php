<?php

// Set the copyright text that appears at the bottom of the page.
variable_set('helpertheme_footer_copyright_text', '&#169; Copyright [current-date:custom:Y] <a href="https://plan-international.org/">Plan International</a>');
