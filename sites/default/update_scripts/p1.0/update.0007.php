<?php

// Provide a list of modules to be installed.
$modules = array(
  'taxonomy',

  'helpergeneric',
  'helpertheme',
  'view_mode_per_role',

  'embeddedcontent',
  'mcaentity_domain',
  'mcaentity_provider',
  'mcaentity_score',
);
_us_module__install($modules);

// Prepare a list of features to be installed.
$feature_names = array(
  'contactform',
  'mcaentities',
  'mcaembeddedcontent',

  'mcact_page',
  'mcaet_domain',
  'mcaet_provider',

  'mcaprovider_directory',
  'mcasettings',
  'mcaviewsettings',
);
_us_features__install($feature_names);

// Clear system caches.
drupal_flush_all_caches();

// Revert previously installed features.
_us_features__revert($feature_names);

// Clear system caches.
drupal_flush_all_caches();


// Change site homepage.
//variable_set('site_frontpage', 'homepage');

// Set the copyright text that appears at the bottom of the page.
variable_set('helpertheme_footer_copyright_text', '&#169; Copyright [current-date:custom:Y] Plan');
