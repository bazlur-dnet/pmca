<?php

// Provide a list of modules to be installed.
$modules = array(
  'colorbox',
  'geofield',
  'geophp',

  'helperhome',
);
_us_module__install($modules);

// Enable Colorbox load.
variable_set('colorbox_load', 1);

// Change when colorbox is auto-magically loaded.
variable_set('colorbox_visibility', 1);
variable_set('colorbox_pages', "domain/*\n");

// Prepare a list of field instances to be deleted.
$field_instances = array(
  'provider-simple-field_prv_involved_with',
  'provider-simple-field_prv_services_for',
);
foreach ($field_instances as $field_instance) {
  list($entity_type, $bundle, $field_name) = explode('-', $field_instance);
  // Delete the field instance.
  _us_fields__delete_instances($field_name, $entity_type, $bundle);
}
