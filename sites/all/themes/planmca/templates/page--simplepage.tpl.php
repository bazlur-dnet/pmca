<?php

/**
 * @file
 * Display a simple Drupal page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.tpl.php template in this directory.
 */
?>

<style>
  html {
    background-color: white;
    overflow-y: auto;
  }
  .main-content-inner {
    margin: 0;
  }
  .simplepage .mobject .group-above {
    margin-bottom: 0.5em;
  }
  .simplepage .mobject .group-side {
    float: left;
    margin-right: 1em;
  }
</style>
<div class="main-content-wrapper simplepage clearfix">
  <section role="main" id="main-content" class="main-content">
    <div class="main-content-inner clearfix">
      <section class="messages-wrapper clearfix">
        <?php print $messages; ?>
      </section>

      <div class="clearfix <?php print $main_content_classes; ?>">
        <?php print render($page['help']); ?>
        <?php print render($page['content']); ?>
      </div>
    </div>
  </section>
</div>
