<?php
/**
 * @file
 * mcaprovider_directory.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function mcaprovider_directory_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'service_provider';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'mcaprovider';
  $view->human_name = 'Service Provider';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['distinct'] = TRUE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['exposed_form']['options']['expose_sort_order'] = FALSE;
  $handler->display->display_options['exposed_form']['options']['autosubmit'] = TRUE;
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'entity';
  $handler->display->display_options['row_options']['view_mode'] = 'teaser';
  /* No results behavior: Global: Unfiltered text */
  $handler->display->display_options['empty']['area_text_custom']['id'] = 'area_text_custom';
  $handler->display->display_options['empty']['area_text_custom']['table'] = 'views';
  $handler->display->display_options['empty']['area_text_custom']['field'] = 'area_text_custom';
  $handler->display->display_options['empty']['area_text_custom']['empty'] = TRUE;
  $handler->display->display_options['empty']['area_text_custom']['content'] = 'There are no service providers to display.';
  /* Field: Service Provider: Service provider ID */
  $handler->display->display_options['fields']['pid']['id'] = 'pid';
  $handler->display->display_options['fields']['pid']['table'] = 'mcaprovider';
  $handler->display->display_options['fields']['pid']['field'] = 'pid';
  /* Sort criterion: Field: Name (name_field) */
  $handler->display->display_options['sorts']['name_field_value']['id'] = 'name_field_value';
  $handler->display->display_options['sorts']['name_field_value']['table'] = 'field_data_name_field';
  $handler->display->display_options['sorts']['name_field_value']['field'] = 'name_field_value';
  /* Filter criterion: Service Provider: Status */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'mcaprovider';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = '1';

  /* Display: Directory */
  $handler = $view->new_display('page', 'Directory', 'directory');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Service Providers';
  $handler->display->display_options['defaults']['css_class'] = FALSE;
  $handler->display->display_options['css_class'] = 'layout-grid-items expanded clearfix';
  $handler->display->display_options['defaults']['use_ajax'] = FALSE;
  $handler->display->display_options['use_ajax'] = TRUE;
  $handler->display->display_options['defaults']['exposed_form'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'nowhitespace';
  $handler->display->display_options['exposed_form']['options']['reset_button'] = TRUE;
  $handler->display->display_options['exposed_form']['options']['expose_sort_order'] = FALSE;
  $handler->display->display_options['defaults']['pager'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['style_options']['row_class'] = 'layout-grid-item';
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'entity';
  $handler->display->display_options['row_options']['view_mode'] = 'teaser';
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['header'] = FALSE;
  /* Header: Embedded Content: Rendered Entity from embeddedvar */
  $handler->display->display_options['header']['embeddedvar']['id'] = 'embeddedvar';
  $handler->display->display_options['header']['embeddedvar']['table'] = 'embeddedcontent__global';
  $handler->display->display_options['header']['embeddedvar']['field'] = 'embeddedvar';
  $handler->display->display_options['header']['embeddedvar']['entity_type'] = 'embeddedcontent';
  $handler->display->display_options['header']['embeddedvar']['view_mode'] = 'notitle';
  $handler->display->display_options['header']['embeddedvar']['bypass_access'] = 0;
  $handler->display->display_options['header']['embeddedvar']['machine_name'] = 'providers_listing_top';
  $handler->display->display_options['header']['embeddedvar']['replace_page_title'] = 1;
  $handler->display->display_options['defaults']['empty'] = FALSE;
  /* No results behavior: Global: Unfiltered text */
  $handler->display->display_options['empty']['area_text_custom']['id'] = 'area_text_custom';
  $handler->display->display_options['empty']['area_text_custom']['table'] = 'views';
  $handler->display->display_options['empty']['area_text_custom']['field'] = 'area_text_custom';
  $handler->display->display_options['empty']['area_text_custom']['empty'] = TRUE;
  $handler->display->display_options['empty']['area_text_custom']['content'] = 'There are no service providers matching the search criteria.';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Service Provider: Status */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'mcaprovider';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = '1';
  /* Filter criterion: Service Provider: Location (field_prv_location) */
  $handler->display->display_options['filters']['field_prv_location_tid']['id'] = 'field_prv_location_tid';
  $handler->display->display_options['filters']['field_prv_location_tid']['table'] = 'field_data_field_prv_location';
  $handler->display->display_options['filters']['field_prv_location_tid']['field'] = 'field_prv_location_tid';
  $handler->display->display_options['filters']['field_prv_location_tid']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_prv_location_tid']['expose']['operator_id'] = 'field_prv_location_tid_op';
  $handler->display->display_options['filters']['field_prv_location_tid']['expose']['label'] = 'Location';
  $handler->display->display_options['filters']['field_prv_location_tid']['expose']['operator'] = 'field_prv_location_tid_op';
  $handler->display->display_options['filters']['field_prv_location_tid']['expose']['identifier'] = 'location';
  $handler->display->display_options['filters']['field_prv_location_tid']['expose']['multiple'] = TRUE;
  $handler->display->display_options['filters']['field_prv_location_tid']['type'] = 'select';
  $handler->display->display_options['filters']['field_prv_location_tid']['vocabulary'] = 'location';
  $handler->display->display_options['filters']['field_prv_location_tid']['hierarchy'] = 1;
  /* Filter criterion: Service Provider: Services Offered (field_prv_services_offered) */
  $handler->display->display_options['filters']['field_prv_services_offered_value']['id'] = 'field_prv_services_offered_value';
  $handler->display->display_options['filters']['field_prv_services_offered_value']['table'] = 'field_data_field_prv_services_offered';
  $handler->display->display_options['filters']['field_prv_services_offered_value']['field'] = 'field_prv_services_offered_value';
  $handler->display->display_options['filters']['field_prv_services_offered_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_prv_services_offered_value']['expose']['operator_id'] = 'field_prv_services_offered_value_op';
  $handler->display->display_options['filters']['field_prv_services_offered_value']['expose']['label'] = 'Services Offered';
  $handler->display->display_options['filters']['field_prv_services_offered_value']['expose']['operator'] = 'field_prv_services_offered_value_op';
  $handler->display->display_options['filters']['field_prv_services_offered_value']['expose']['identifier'] = 'services';
  $handler->display->display_options['filters']['field_prv_services_offered_value']['expose']['multiple'] = TRUE;
  /* Filter criterion: Service Provider: Type of Clients Served (field_prv_type_clients_served) */
  $handler->display->display_options['filters']['field_prv_type_clients_served_value']['id'] = 'field_prv_type_clients_served_value';
  $handler->display->display_options['filters']['field_prv_type_clients_served_value']['table'] = 'field_data_field_prv_type_clients_served';
  $handler->display->display_options['filters']['field_prv_type_clients_served_value']['field'] = 'field_prv_type_clients_served_value';
  $handler->display->display_options['filters']['field_prv_type_clients_served_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_prv_type_clients_served_value']['expose']['operator_id'] = 'field_prv_type_clients_served_value_op';
  $handler->display->display_options['filters']['field_prv_type_clients_served_value']['expose']['label'] = 'Type of Clients Served';
  $handler->display->display_options['filters']['field_prv_type_clients_served_value']['expose']['operator'] = 'field_prv_type_clients_served_value_op';
  $handler->display->display_options['filters']['field_prv_type_clients_served_value']['expose']['identifier'] = 'clients';
  $handler->display->display_options['filters']['field_prv_type_clients_served_value']['expose']['multiple'] = TRUE;
  $handler->display->display_options['path'] = 'directory';
  $export['service_provider'] = $view;

  return $export;
}
