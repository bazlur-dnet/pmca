<?php
/**
 * @file
 * mcaprovider_directory.features.inc
 */

/**
 * Implements hook_views_api().
 */
function mcaprovider_directory_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
