<?php

/**
 * @file
 * Definition of views_handler_filter_term_node_tid_depth.
 */

/**
 * Filter handler for taxonomy terms with depth.
 *
 * This handler is actually part of the node table and has some restrictions,
 * because it uses a subquery to find nodes with.
 *
 * @ingroup views_filter_handlers
 */
class mcaprovider_directory_views_handler_filter_checkboxes_tree extends views_handler_filter_term_node_tid {
  /**
   * @HACK: Don't let exposed_translate() "FIX" the $value_form_type.
   */
  function exposed_translate(&$form, $type) {
    parent::exposed_translate($form, $type);

    // Checkboxes can work just fine in exposed forms.
    $form['#type'] = 'checkboxes';
  }

  /**
   * Check to see if input from the exposed filters should change
   * the behavior of this filter.
   */
  function accept_exposed_input($input) {
    $accepted = parent::accept_exposed_input($input);
    if (!$accepted) {
      return FALSE;
    }

    // Remove checkboxes that were not selected.
    if (!empty($this->value) && is_array($this->value)) {
      $this->value = array_filter($this->value);
    }

    return TRUE;
  }

  function value_form(&$form, &$form_state) {
    if ($this->options['type'] == 'textfield' || empty($this->options['hierarchy']) || !$this->options['limit']) {
      parent::value_form($form, $form_state);
      return;
    }

    $vocabulary = taxonomy_vocabulary_machine_name_load($this->options['vocabulary']);
    if (empty($vocabulary) && $this->options['limit']) {
      $form['markup'] = array(
        '#markup' => '<div class="form-item">' . t('An invalid vocabulary is selected. Please change it in the options.') . '</div>',
      );
      return;
    }

    $tree = taxonomy_get_tree($vocabulary->vid, 0, NULL, TRUE);
    $options = array();

    if ($tree) {
      // Translation system needs full entity objects, so we have access to label.
      foreach ($tree as $term) {
        // @HACK: Wee need to display form items in a tree like structure.
        $options[$term->tid] = new StdClass();
        $options[$term->tid]->depth = $term->depth;
        $options[$term->tid]->label = entity_label('taxonomy_term', $term);
      }
    }

    $default_value = (array) $this->value;

    if (!empty($form_state['exposed'])) {
      $identifier = $this->options['expose']['identifier'];

      if (!empty($this->options['expose']['reduce'])) {
        $options = $this->reduce_value_options($options);

        if (!empty($this->options['expose']['multiple']) && empty($this->options['expose']['required'])) {
          $default_value = array();
        }
      }

      if (empty($this->options['expose']['multiple'])) {
        if (empty($this->options['expose']['required']) && (empty($default_value) || !empty($this->options['expose']['reduce']))) {
          $default_value = 'All';
        }
        elseif (empty($default_value)) {
          $keys = array_keys($options);
          $default_value = array_shift($keys);
        }
        // Due to #1464174 there is a chance that array('') was saved in the admin ui.
        // Let's choose a safe default value.
        elseif ($default_value == array('')) {
          $default_value = 'All';
        }
        else {
          $copy = $default_value;
          $default_value = array_shift($copy);
        }
      }
    }

    $form['value'] = array(
      '#type' => 'checkboxes',
      '#title' => $this->options['limit'] ? t('Select terms from vocabulary @voc', array('@voc' => $vocabulary->name)) : t('Select terms'),
      '#multiple' => TRUE,
      '#options' => $options,
      '#default_value' => $default_value,
      '#reder_as_tree' => TRUE,
      '#process' => array(
        'mcaprovider_directory_form_process_checkboxes'
      ),
    );

    if (!empty($form_state['exposed']) && isset($identifier) && !isset($form_state['input'][$identifier])) {
      $form_state['input'][$identifier] = $default_value;
    }

    if (empty($form_state['exposed'])) {
      // Retain the helper option
      $this->helper->options_form($form, $form_state);
    }
  }


  /**
   * Sanitizes the HTML select element's options.
   *
   * The function is recursive to support optgroups.
   */
  function prepare_filter_select_options(&$options) {
    foreach ($options as $value => $label) {
      // Recurse for optgroups.
      if (is_array($label)) {
        $this->prepare_filter_select_options($options[$value]);
      }
      elseif (is_object($label) && isset($label->label)) {
        $label->label = strip_tags(decode_entities($label->label));
      }
      // FAPI has some special value to allow hierarchy.
      // @see _form_options_flatten
      elseif (is_object($label)) {
        $this->prepare_filter_select_options($options[$value]->option);
      }
      else {
        $options[$value] = strip_tags(decode_entities($label));
      }
    }
  }
}
