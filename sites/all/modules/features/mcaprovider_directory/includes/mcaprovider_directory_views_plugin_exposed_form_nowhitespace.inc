<?php

/**
 * @file
 * Definition of mcaprovider_directory_views_plugin_exposed_form_nowhitespace.
 */

/**
 * Exposed form plugin that provides a basic exposed form without whitespace
 * between form elements.
 *
 * @ingroup views_exposed_form_plugins
 */
class mcaprovider_directory_views_plugin_exposed_form_nowhitespace extends views_plugin_exposed_form_basic {
  function render_exposed_form($block = FALSE) {
    $output = parent::render_exposed_form($block);

    // Remove whitespace between items. Helps with styling in-line elements.
    $output = preg_replace('/\>\s+\</', '><', $output);

    return $output;
  }
}
