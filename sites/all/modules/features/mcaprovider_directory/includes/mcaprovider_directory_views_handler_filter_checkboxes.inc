<?php

/**
 * Extend the filter field list, to render the filters as checkboxes.
 */
class mcaprovider_directory_views_handler_filter_checkboxes extends views_handler_filter_field_list {
  var $value_form_type = 'checkboxes';

  /**
   * @HACK: Don't let exposed_translate() "FIX" the $value_form_type.
   */
  function exposed_translate(&$form, $type) {
    parent::exposed_translate($form, $type);

    // Checkboxes can work just fine in exposed forms.
    $form['#type'] = $this->value_form_type;
    unset($form['#size']);
  }

  /**
   * Check to see if input from the exposed filters should change
   * the behavior of this filter.
   */
  function accept_exposed_input($input) {
    $accepted = parent::accept_exposed_input($input);
    if (!$accepted) {
      return FALSE;
    }

    // Remove checkboxes that were not selected.
    if (!empty($this->value) && is_array($this->value)) {
      $this->value = array_filter($this->value);
    }

    return TRUE;
  }
}
