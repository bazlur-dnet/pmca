<?php
/**
 * @file
 * mcaprovider_directory.custom.inc
 */

/**
 * Implements hook_libraries_info().
 */
function mcaprovider_directory_libraries_info() {
  $libraries['leaflet'] = array(
    'name' => 'Leaflet',
    'vendor url' => 'https://github.com/Leaflet/Leaflet',
    'download url' => 'https://github.com/Leaflet/Leaflet/archive/master.tar.gz',
    'version arguments' => array(
      'file' => 'package.json',
      'pattern' => '/"version": "([0-9a-zA-Z\.-]+)"/',
      'lines' => 5,
      'cols' => 100,
    ),
    'files' => array(
      'js' => array('dist/leaflet-src.js'),
      'css' => array('dist/leaflet.css'),
    ),
    'variants' => array(
      'minified' => array(
        'files' => array(
          'js' => array('dist/leaflet.js'),
          'css' => array('dist/leaflet.css'),
        ),
      ),
    ),
  );

  $libraries['leaflet.label'] = array(
    'name' => 'Leaflet.label',
    'vendor url' => 'https://github.com/Leaflet/Leaflet.label',
    'download url' => 'https://github.com/Leaflet/Leaflet.label/archive/master.tar.gz',
    'version arguments' => array(
      'file' => 'package.json',
      'pattern' => '/"version": "([0-9a-zA-Z\.-]+)"/',
      'lines' => 5,
      'cols' => 100,
    ),
    'files' => array(
      'js' => array('dist/leaflet.label-src.js'),
      //'css' => array('dist/leaflet.label.css'),
    ),
    'variants' => array(
      'minified' => array(
        'files' => array(
          'js' => array('dist/leaflet.label.js'),
          //'css' => array('dist/leaflet.label.css'),
        ),
      ),
    ),
  );

  $libraries['leaflet.markercluster'] = array(
    'name' => 'Leaflet.markercluster',
    'vendor url' => 'https://github.com/Leaflet/Leaflet.markercluster',
    'download url' => 'https://github.com/Leaflet/Leaflet.markercluster/archive/master.tar.gz',
    'version arguments' => array(
      'file' => 'package.json',
      'pattern' => '/"version": "([0-9a-zA-Z\.-]+)"/',
      'lines' => 5,
      'cols' => 100,
    ),
    'files' => array(
      'js' => array('dist/leaflet.markercluster-src.js'),
      'css' => array('dist/MarkerCluster.css'),
    ),
    'variants' => array(
    ),
  );

  $libraries['leaflet.esri'] = array(
    'name' => 'leaflet.esri',
    'vendor url' => 'https://github.com/Esri/esri-leaflet',
    'download url' => 'https://github.com/Esri/esri-leaflet/archive/v1.0.0.tar.gz',
    'version arguments' => array(
      'file' => 'package.json',
      'pattern' => '/"version": "([0-9a-zA-Z\.-]+)"/',
      'lines' => 5,
      'cols' => 100,
    ),
    'files' => array(
      'js' => array('dist/builds/basemaps/esri-leaflet-basemaps-src.js'),
    ),
    'variants' => array(
      'minified' => array(
        'files' => array(
          'js' => array('dist/builds/basemaps/esri-leaflet-basemaps.js'),
        ),
      ),
    ),
  );

  $libraries['masonry'] = array(
    'name' => 'masonry',
    'vendor url' => 'https://github.com/desandro/masonry',
    'download url' => 'https://github.com/desandro/masonry/archive/v3.3.2.tar.gz',
    'version arguments' => array(
      'file' => 'package.json',
      'pattern' => '/"version": "([0-9a-zA-Z\.-]+)"/',
      'lines' => 5,
      'cols' => 100,
    ),
    'files' => array(
      'js' => array('dist/masonry.pkgd.min.js'),
    ),
    'variants' => array(
      'minified' => array(
        'files' => array(
          'js' => array('dist/masonry.pkgd.js'),
        ),
      ),
    ),
  );

  return $libraries;
}

/**
 * Default theme function activities search forms.
 *
 * @see template_preprocess_views_exposed_form()
 */
function mcaprovider_directory_preprocess_views_exposed_form(&$variables) {
  // @TODO: Find a proper way to only alter the activities search form.
  if ($variables['form']['#id'] != 'views-exposed-form-service-provider-directory') {
    return;
  }

  $variables['theme_hook_suggestions'][] = 'views_exposed_form__service_provider_directory';
}

/**
 * Implements hook_views_data_alter().
 */
function mcaprovider_directory_views_data_alter(&$data) {
  // Replace the filter handler; original: "views_handler_filter_field_list".
  if (isset($data['field_data_field_prv_services_offered']['field_prv_services_offered_value'])) {
    $data['field_data_field_prv_services_offered']['field_prv_services_offered_value']['filter']['handler'] = 'mcaprovider_directory_views_handler_filter_checkboxes';
  }
  if (isset($data['field_data_field_prv_type_clients_served']['field_prv_type_clients_served_value'])) {
    $data['field_data_field_prv_type_clients_served']['field_prv_type_clients_served_value']['filter']['handler'] = 'mcaprovider_directory_views_handler_filter_checkboxes';
  }

  // Use filter with depth.
  if (isset($data['field_data_field_prv_location']['field_prv_location_tid'])) {
    $data['field_data_field_prv_location']['field_prv_location_tid']['filter']['handler'] = 'mcaprovider_directory_views_handler_filter_checkboxes_tree';
  }
}

/**
 * Implements hook_theme().
 */
function mcaprovider_directory_theme($existing, $type, $theme, $path) {
  return array(
    'custom_map' => array(
      'variables' => array('view' => NULL, 'values' => NULL),
    ),
  );
}

/**
 * Returns HTML for a map.
 *
 * @param $variables
 * @return string
 */
function theme_custom_map($variables) {
  $elements = array();

  $elements['map_container'] = array(
    '#type' => 'container',
    '#attributes' => array(
      'id' => $variables['id'],
      'class' => $variables['classes_array'],
    ),
  );

  // Load the "Leaflet" JavaScript library.
  libraries_load('leaflet', 'minified');

  // Load the "Leaflet.markercluster" JavaScript library.
  libraries_load('leaflet.markercluster');

  // Load the "Leaflet.esri" JavaScript library
  libraries_load('leaflet.esri', 'minified');

  // Load the "Leaflet.label" JavaScript library.
  libraries_load('leaflet.label', 'minified');

  return drupal_render($elements);
}

/**
 * Preprocess the service directory map.
 * @param $variables
 */
function template_preprocess_custom_map(&$variables) {
  $js_setting = array();
  $js_setting['mapData'] = array();

  if (isset($variables['view'])) {
    if ($variables['view']->name == 'service_provider' && $variables['view']->current_display == 'directory') {
      $variables['id'] = 'directory-map';

      // Load map data from the current view.
      /** @var view $view */
      $view = $variables['view'];
      $js_setting['mapData']['records'] = mcaprovider_directory_extract_data_from_view($view);
      $js_setting['mapData']['setZoom'] = '4';
    }
  }


  if (isset($variables['values'])) {
    $variables['id'] = 'performance-map';

    views_add_js('ajax');

    // Load map data from the current view.
    $js_setting['mapData']['values'] = $variables['values'];
  }


  $variables['classes_array'][] = $variables['id'];

  $js_setting['mapData']['selector'] = '#' . $variables['id'];

  // Add information about the map.
  drupal_add_js($js_setting, 'setting');
}

/**
 * Implements hook_preprocess_views_view().
 */
function mcaprovider_directory_preprocess_views_view(&$variables) {
  if ($variables['view']->name != 'service_provider' || $variables['view']->current_display != 'directory') {
    return;
  }

  // @TODO: Find a decent way to differentiate between views_ajax() and views_page().
  $is_ajax_request = FALSE;
  if (stripos($_SERVER['REQUEST_URI'], 'views/ajax') !== FALSE) {
    $is_ajax_request = TRUE;
  }

  /** @var view $view */
  $view = $variables['view'];

  // If not using AJAX, we need to add the map markup.
  $variables['directory_map'] = '';
  if (!$is_ajax_request) {
    // Hide the main page title.
    embeddedcontent_alternative_page_title_set_status(TRUE);

    $variables['directory_map'] = theme('custom_map', array('view' => $view));
  }

  // Get path to the current module.
  $module_path = drupal_get_path('module', 'mcaprovider_directory');

  // Load directory specific JS.
  drupal_add_js($module_path . '/resources/mcaprovider_directory.js');

  // Load the "masonry" JavaScript library.
  libraries_load('masonry', 'minified');
}

/**
 * Returns an array with markers for the map.
 *
 * @TODO: Optimize!
 *
 * @param $view
 * @return array
 */
function mcaprovider_directory_extract_data_from_view($view) {
  // Prepare a list of entities to load.
  $provider_ids = array();
  foreach ($view->result as $result_entry) {
    $provider_ids[] = $result_entry->pid;
  }
  // We already used this entities on the current request, they are loaded from cache.
  $provider_entities = entity_load('provider', $provider_ids);

  $records = array();
  /** @var ProviderEntity $entity */
  foreach ($provider_entities as $entity) {
    if (empty($entity->field_prv_location[LANGUAGE_NONE])) {
      continue;
    }

    $value = array_pop($entity->field_prv_location[LANGUAGE_NONE]);
    if (empty($value['tid']) && empty($value['taxonomy_term'])) {
      continue;
    }

    // This changes based on the used field display settings.
    if (!empty($value['taxonomy_term'])) {
      $term = $value['taxonomy_term'];
    }
    else {
      $term = taxonomy_term_load($value['tid']);
    }

    if (empty($term) || empty($term->field_loc_coordinates[LANGUAGE_NONE][0])) {
      continue;
    }

    $uri = $entity->uri();

    $records[] = array(
      'lat' => (float) $term->field_loc_coordinates[LANGUAGE_NONE][0]['lat'],
      'lon' => (float) $term->field_loc_coordinates[LANGUAGE_NONE][0]['lon'],
      'provider_name' => $entity->label(),
      'provider_url' => $uri['path'],
    );
  }

  return $records;
}

/**
 * Implements hook_views_ajax_data_alter().
 *
 * @see views_ajax()
 */
function mcaprovider_directory_views_ajax_data_alter(&$commands, $view) {
  if ($view->name != 'service_provider' || $view->current_display != 'directory') {
    return;
  }

  // Load map data from the current view.
  $records = mcaprovider_directory_extract_data_from_view($view);
  $commands[] = mcaprovider_directory_ajax_command_update_map($records);
}

/**
 * Add or remove a single class, on all matched elements.
 */
function mcaprovider_directory_ajax_command_update_map($records) {
  return array(
    'command' => 'mcaMapUpdate',
    'records' => $records,
  );
}

/**
 * Implements hook_views_plugins().
 */
function mcaprovider_directory_views_plugins() {
  $plugins = array(
    'exposed_form' => array(
      'nowhitespace' => array(
        'title' => t('Basic (no-whitespace)'),
        'help' => t('Basic exposed form, without whitespace between form items.'),
        'handler' => 'mcaprovider_directory_views_plugin_exposed_form_nowhitespace',
        //'handler' => 'views_plugin_exposed_form_basic',
        'uses options' => TRUE,
        'help topic' => 'exposed-form-basic',
      ),
    ),
  );

  return $plugins;
}


/**
 * Processes a checkboxes form element.
 *
 * @see: form_process_checkboxes()
 */
function mcaprovider_directory_form_process_checkboxes($element) {
  if (empty($element['#reder_as_tree'])) {
    return form_process_checkboxes($element);
  }

  $value = is_array($element['#value']) ? $element['#value'] : array();
  $element['#tree'] = TRUE;
  if (count($element['#options']) > 0) {
    if (!isset($element['#default_value']) || $element['#default_value'] == 0) {
      $element['#default_value'] = array();
    }
    $weight = 0;
    $top_level_element = NULL;
    foreach ($element['#options'] as $key => $choice) {
      // Integer 0 is not a valid #return_value, so use '0' instead.
      // @see form_type_checkbox_value().
      // @todo For Drupal 8, cast all integer keys to strings for consistency
      //   with form_process_radios().
      if ($key === 0) {
        $key = '0';
      }
      // Maintain order of options as defined in #options, in case the element
      // defines custom option sub-elements, but does not define all option
      // sub-elements.
      $weight += 0.001;

      $choice_attributes = $element['#attributes'];
      if (is_object($choice)) {
        $choice_label = $choice->label;
        if (empty($choice->depth)) {
          $top_level_element = $key;
        }
        else {
          $choice_attributes['class'][] = 'checkbox-sublevel';
        }
      }
      else {
        $choice_label = $choice;
      }

      if (isset($top_level_element) && $top_level_element == $key) {
        $element += array('toplevel-' . $key => array(
          '#type' => 'container',
          '#attributes' => array(
            'class' => array(
              'toplevel-group-wrapper',
              drupal_html_class('toplevel-group-' . $choice_label),
            ),
          ),
          '#parents' => $element['#parents'],
          $key => array(),
        ));
        $element['toplevel-' . $key][$key] += array(
          '#type' => 'checkbox',
          '#title' => $choice_label,
          '#return_value' => $key,
          '#default_value' => isset($value[$key]) ? $key : NULL,
          '#attributes' => $choice_attributes,
          '#ajax' => isset($element['#ajax']) ? $element['#ajax'] : NULL,
          '#weight' => $weight,
        );
      }
      else {
        $element['toplevel-' . $top_level_element] += array($key => array());
        $element['toplevel-' . $top_level_element][$key] += array(
          '#type' => 'checkbox',
          '#title' => $choice_label,
          '#return_value' => $key,
          '#default_value' => isset($value[$key]) ? $key : NULL,
          '#attributes' => $choice_attributes,
          '#ajax' => isset($element['#ajax']) ? $element['#ajax'] : NULL,
          '#weight' => $weight,
        );
      }
    }
  }

  return $element;
}
