(function(global) {
  'use strict';
  var L = global.L;
  L.InfoBox = L.Control.extend({
    onAdd: function() {
      this._container = L.DomUtil.create('div', 'leaflet-control-layers leaflet-control-info');
      return this._container;
    },
    setContent: function(innerHTML) {
      this.getContainer().innerHTML = innerHTML;
    }
  });
})(window);


(function (global) {
  'use strict';

  var Drupal = global.Drupal,
      L = global.L,
      $ = global.jQuery;
  var map, info, markers;

  Drupal.ajax.prototype.commands.mcaMapUpdate = function(ajax, data, status) {
    Drupal.settings.mapData.records = data.records;
    Drupal.behaviors.directoryMap.updateMap(data.records, 'records');
  };

  /**
   * Customized marker cluster
   * @param {object} cluster
   * @returns {L.Icon}
   */
  function createClusterIcon(cluster) {
    return new L.DivIcon({
      className: 'cluster-icon',
      iconSize: [36, 36],
      html: '<span>' + cluster.getChildCount() + '</span>',
    });
  }

  /**
   * @param {object} record A single record.
   * @returns {L.Marker} a marker for that record.
   */
  function recordToMarker(record) {
    var position = [record.lat, record.lon],
      marker = L.circleMarker(position, {
        color: '#fff',
        fillColor: '#fe4517',
        fillOpacity: 1,
        opacity: 0.9,
        weight: 1,
      });
    marker.on('click dblclick', goTo, record);
    marker.bindLabel(record.provider_name);

    return marker;
  }

  /**
   * @param {object} value A single data point.
   * @returns {L.Marker} a marker for that record.
   */
  function valueToMarker(value) {
    var marker = L.circleMarker([value.lat, value.lon], {
        color: '#fff',
        fillColor: '#F89200',
        fillOpacity: 1,
        opacity: 0.8,
        weight: 1,
      })
      .setRadius(32)
      .bindLabel(value.average, { noHide: true, offset: [0, 0] });
    return marker;
  }

  /**
   * Marker click handler, must have context bound to a record
   */
  function goTo() {
    global.location = this.provider_url;  // bound to record
  }

  /**
   * Utility for mapping a function over values (eg., es5 Array.prototype.map)
   * @param {func} f
   * @param {array<any>} arr
   * @returns {array<any>}
   */
  function fmap(f, arr) {
    var results = [];
    for (var i = 0; i < arr.length; i++) {
      results.push(f(arr[i]));
    }
    return results;
  }

  Drupal.behaviors.directoryMap = {
    /**
     * Initialize the map container with the leaflet map, setting up the default
     * view, and preparing the layers to be filled with data on update().
     * @param {DOMElement} container An existing element on the page for the map
     * @param {object} mapSettings A list of map settings.
     * @returns {void}
     */
    initMap: function(container, mapSettings) {
      map = L.map(container, {
        minZoom: 1,
        maxZoom: 12,
        scrollWheelZoom: false
      });
      var tiles = L.esri.basemapLayer('Topographic');
      info = new L.InfoBox({position: 'bottomleft'});
      markers = new L.MarkerClusterGroup({
        iconCreateFunction: createClusterIcon,
        spiderfyDistanceMultiplier: 1.02,
        zoomToBoundsOnClick: true
      });

      // Set map default state.
      var southWest = L.latLng(8, 64),
        northEast = L.latLng(32, 96),
        bounds = L.latLngBounds(southWest, northEast);
      map.fitBounds(bounds);
      if (mapSettings.setZoom) {
        map.setZoom(mapSettings.setZoom);
      }

      tiles.addTo(map);
      info.addTo(map);
      markers.addTo(map);
    },

    /**
     * Push data to show on the map. Each record must have keys:
     * - id {string}
     * - lat {number}
     * - lon {number}
     * @param {array<object>} entries
     * @param {string} type
     * @returns {void}
     */
    updateMap: function(entries, type) {
      if (typeof map === 'undefined') {
        throw new Error('directoryMap.updateMap cannot be called before directoryMap.initMap');
      }
      markers.clearLayers();
      if (type === 'records') {
        markers.addLayers(fmap(recordToMarker, entries));
      }
      else if (type === 'values') {
        markers.addLayers(fmap(valueToMarker, entries));
      }
    },

    attach: function(context, settings) {
      if (!settings.mapData.selector) {
        return;
      }

      // Get the map element and initialize the map.
      var map_element = $(settings.mapData.selector).get(0);
      if (!map_element) {
        return;
      }
      if (typeof map === 'undefined') {
        Drupal.behaviors.directoryMap.initMap(map_element, settings.mapData);
      }

      // Update the map.
      if (settings.mapData.records && settings.mapData.records.length) {
        Drupal.behaviors.directoryMap.updateMap(settings.mapData.records, 'records');
      }
      if (settings.mapData.values && settings.mapData.values.length) {
        Drupal.behaviors.directoryMap.updateMap(settings.mapData.values, 'values');
      }
    }
  };

  Drupal.behaviors.directoryLayout = {
    attach: function(context, settings) {
      $('.view-content', context).masonry({
        itemSelector: '.layout-grid-item'
      });
    }
  };

  Drupal.behaviors.directoryFilters = {
    attach: function(context, settings) {
      $('.toplevel-group-wrapper', context).each(function() {
        var checkboxes = $(this).find('input:checkbox');

        // Bind to the change event of the first (top) element.
        $(checkboxes[0]).change(function(e) {
          $(e.currentTarget)
            .parents('.toplevel-group-wrapper')
            .find('.checkbox-sublevel')
            .attr('checked', e.currentTarget.checked);
          console.log(e.currentTarget.checked, 'e.currentTarget.checked');
        });

        // Bind to the change event of all but the first (top) element.
        $(checkboxes.slice(1)).change(function(e) {
          // Un-check the top level, when the current sublevel is un-checked.
          if (!e.currentTarget.checked) {
            $(e.currentTarget)
              .parents('.toplevel-group-wrapper')
              .find('input:checkbox').first()
              .attr('checked', false);
          }
          // Check the top level, when all sublevels are selected.
          else {
            var sublevels = $(e.currentTarget)
              .parents('.toplevel-group-wrapper')
              .find('input:checkbox').slice(1);
            var selected_sublevels = sublevels.filter(':checked');
            if (sublevels.length === selected_sublevels.length) {
              $(e.currentTarget)
                .parents('.toplevel-group-wrapper')
                .find('.form-checkbox').first()
                .attr('checked', true);
            }
          }
        });
      });

    }
  };

})(window);
