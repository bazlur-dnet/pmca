<?php
/**
 * @file
 * mcact_page.custom.inc
 */

/**
 * Define the variable prefix for storing embedded content.
 */
define('MCACT_PAGE_EMBEDDED_STORAGE_PREFIX', 'mcact_page_embedded_');

/**
 * Implements hook_node_view().
 */
function mcact_page_node_view($node, $view_mode, $langcode) {
  if ($view_mode != 'full' || $node->type != 'page') {
    return;
  }

  // Display statements on the about page.
  $about_page_nid = variable_get(MCACT_PAGE_EMBEDDED_STORAGE_PREFIX . 'about', NULL);
  if ($node->nid == $about_page_nid) {
    $node->extra_content = views_embed_view('standards', 'statements');
  }
}
