<?php
/**
 * @file
 * mcasettings.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function mcasettings_user_default_roles() {
  $roles = array();

  // Exported role: administrator user.
  $roles['administrator user'] = array(
    'name' => 'administrator user',
    'weight' => 2,
  );

  // Exported role: contact person.
  $roles['contact person'] = array(
    'name' => 'contact person',
    'weight' => 3,
  );

  return $roles;
}
