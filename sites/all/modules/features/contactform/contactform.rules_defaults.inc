<?php
/**
 * @file
 * contactform.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function contactform_default_rules_configuration() {
  $items = array();
  $items['rules_contact_form_notifications'] = entity_import('rules_config', '{ "rules_contact_form_notifications" : {
      "LABEL" : "Contact Form Notifications",
      "PLUGIN" : "rule",
      "OWNER" : "rules",
      "TAGS" : [ "entity_rules_entityform_submission" ],
      "REQUIRES" : [ "rules" ],
      "USES VARIABLES" : {
        "entity" : { "label" : "Entity", "type" : "entityform" },
        "continue" : { "label" : "Continue Rules", "type" : "boolean" }
      },
      "IF" : [
        { "entity_is_of_type" : { "entity" : [ "entity" ], "type" : "entityform" } }
      ],
      "DO" : [
        { "mail_to_users_of_role" : {
            "roles" : { "value" : { "4" : "4" } },
            "subject" : "Contact form submission notification - [site:name]",
            "message" : "Hello,\\r\\nHere is a copy of the submitted data:\\r\\n\\r\\n[entity:textexport-email]\\r\\n(message ends here)\\r\\n"
          }
        },
        { "mail" : {
            "to" : "[entity:field-contact-name] \\u003C[entity:field-contact-email]\\u003E",
            "subject" : "Contact form submission notification - [site:name]",
            "message" : "Hello [entity:field-contact-name],\\r\\nHere is a copy of the submitted data:\\r\\n[entity:textexport-email]\\r\\n(message ends here)\\r\\n\\r\\nThank you for your interest!\\r\\n",
            "language" : [ "" ]
          }
        }
      ],
      "PROVIDES VARIABLES" : [ "continue" ]
    }
  }');
  return $items;
}
