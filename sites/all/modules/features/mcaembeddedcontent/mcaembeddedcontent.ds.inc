<?php
/**
 * @file
 * mcaembeddedcontent.ds.inc
 */

/**
 * Implements hook_ds_field_settings_info().
 */
function mcaembeddedcontent_ds_field_settings_info() {
  $export = array();

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'embeddedcontent|text|default';
  $ds_fieldsetting->entity_type = 'embeddedcontent';
  $ds_fieldsetting->bundle = 'text';
  $ds_fieldsetting->view_mode = 'default';
  $ds_fieldsetting->settings = array(
    'title_field' => array(
      'formatter_settings' => array(
        'ft' => array(
          'func' => 'theme_ds_field_reset',
        ),
      ),
    ),
  );
  $export['embeddedcontent|text|default'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'embeddedcontent|text|full';
  $ds_fieldsetting->entity_type = 'embeddedcontent';
  $ds_fieldsetting->bundle = 'text';
  $ds_fieldsetting->view_mode = 'full';
  $ds_fieldsetting->settings = array(
    'embeddedcontent_icon' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'title_field' => array(
      'formatter_settings' => array(
        'ft' => array(
          'func' => 'theme_ds_field_reset',
        ),
      ),
    ),
  );
  $export['embeddedcontent|text|full'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'embeddedcontent|text|notitle';
  $ds_fieldsetting->entity_type = 'embeddedcontent';
  $ds_fieldsetting->bundle = 'text';
  $ds_fieldsetting->view_mode = 'notitle';
  $ds_fieldsetting->settings = array(
    'field_embeddedcontent_body' => array(
      'formatter_settings' => array(
        'ft' => array(),
      ),
    ),
  );
  $export['embeddedcontent|text|notitle'] = $ds_fieldsetting;

  return $export;
}

/**
 * Implements hook_ds_custom_fields_info().
 */
function mcaembeddedcontent_ds_custom_fields_info() {
  $export = array();

  $ds_field = new stdClass();
  $ds_field->api_version = 1;
  $ds_field->field = 'embeddedcontent_icon';
  $ds_field->label = 'EmbeddedContent Icon';
  $ds_field->field_type = 3;
  $ds_field->entities = array(
    'embeddedcontent' => 'embeddedcontent',
  );
  $ds_field->ui_limit = 'text|full';
  $ds_field->properties = array();
  $export['embeddedcontent_icon'] = $ds_field;

  return $export;
}

/**
 * Implements hook_ds_layout_settings_info().
 */
function mcaembeddedcontent_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'embeddedcontent|text|default';
  $ds_layout->entity_type = 'embeddedcontent';
  $ds_layout->bundle = 'text';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'simplelayout';
  $ds_layout->settings = array(
    'regions' => array(
      'main' => array(
        0 => 'title_field',
        1 => 'field_embeddedcontent_body',
      ),
    ),
    'fields' => array(
      'title_field' => 'main',
      'field_embeddedcontent_body' => 'main',
    ),
    'classes' => array(
      'layout_class' => array(
        '' => '',
      ),
    ),
    'wrappers' => array(
      'main' => 'div',
      'ds_hidden' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['embeddedcontent|text|default'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'embeddedcontent|text|full';
  $ds_layout->entity_type = 'embeddedcontent';
  $ds_layout->bundle = 'text';
  $ds_layout->view_mode = 'full';
  $ds_layout->layout = 'mobject';
  $ds_layout->settings = array(
    'regions' => array(
      'side' => array(
        0 => 'embeddedcontent_icon',
      ),
      'main' => array(
        1 => 'title_field',
        2 => 'field_embeddedcontent_body',
        3 => 'field_embeddedcontent_link',
      ),
    ),
    'fields' => array(
      'embeddedcontent_icon' => 'side',
      'title_field' => 'main',
      'field_embeddedcontent_body' => 'main',
      'field_embeddedcontent_link' => 'main',
    ),
    'classes' => array(
      'layout_class' => array(
        'clearfix' => 'clearfix',
      ),
    ),
    'wrappers' => array(
      'side' => 'div',
      'main' => 'div',
      'ds_hidden' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
    'hide_page_title' => '0',
    'page_option_title' => '',
  );
  $export['embeddedcontent|text|full'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'embeddedcontent|text|notitle';
  $ds_layout->entity_type = 'embeddedcontent';
  $ds_layout->bundle = 'text';
  $ds_layout->view_mode = 'notitle';
  $ds_layout->layout = 'simplelayout';
  $ds_layout->settings = array(
    'regions' => array(
      'main' => array(
        0 => 'field_embeddedcontent_body',
      ),
    ),
    'fields' => array(
      'field_embeddedcontent_body' => 'main',
    ),
    'classes' => array(
      'layout_class' => array(
        '' => '',
      ),
    ),
    'wrappers' => array(
      'main' => 'div',
      'ds_hidden' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['embeddedcontent|text|notitle'] = $ds_layout;

  return $export;
}
