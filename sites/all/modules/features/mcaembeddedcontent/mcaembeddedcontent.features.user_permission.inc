<?php
/**
 * @file
 * mcaembeddedcontent.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function mcaembeddedcontent_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'embeddedcontent entity access overview'.
  $permissions['embeddedcontent entity access overview'] = array(
    'name' => 'embeddedcontent entity access overview',
    'roles' => array(
      'administrator user' => 'administrator user',
    ),
    'module' => 'embeddedcontent',
  );

  // Exported permission: 'embeddedcontent entity administer'.
  $permissions['embeddedcontent entity administer'] = array(
    'name' => 'embeddedcontent entity administer',
    'roles' => array(),
    'module' => 'embeddedcontent',
  );

  // Exported permission: 'embeddedcontent entity create'.
  $permissions['embeddedcontent entity create'] = array(
    'name' => 'embeddedcontent entity create',
    'roles' => array(
      'administrator user' => 'administrator user',
    ),
    'module' => 'embeddedcontent',
  );

  // Exported permission: 'embeddedcontent entity delete'.
  $permissions['embeddedcontent entity delete'] = array(
    'name' => 'embeddedcontent entity delete',
    'roles' => array(
      'administrator user' => 'administrator user',
    ),
    'module' => 'embeddedcontent',
  );

  // Exported permission: 'embeddedcontent entity edit'.
  $permissions['embeddedcontent entity edit'] = array(
    'name' => 'embeddedcontent entity edit',
    'roles' => array(
      'administrator user' => 'administrator user',
    ),
    'module' => 'embeddedcontent',
  );

  // Exported permission: 'embeddedcontent entity view'.
  $permissions['embeddedcontent entity view'] = array(
    'name' => 'embeddedcontent entity view',
    'roles' => array(
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'embeddedcontent',
  );

  return $permissions;
}
