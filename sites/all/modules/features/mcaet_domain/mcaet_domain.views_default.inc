<?php
/**
 * @file
 * mcaet_domain.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function mcaet_domain_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'standards';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'mcadomain';
  $view->human_name = 'Standards';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'entity';
  $handler->display->display_options['row_options']['view_mode'] = 'teaser';
  /* No results behavior: Global: Unfiltered text */
  $handler->display->display_options['empty']['area_text_custom']['id'] = 'area_text_custom';
  $handler->display->display_options['empty']['area_text_custom']['table'] = 'views';
  $handler->display->display_options['empty']['area_text_custom']['field'] = 'area_text_custom';
  $handler->display->display_options['empty']['area_text_custom']['empty'] = TRUE;
  $handler->display->display_options['empty']['area_text_custom']['content'] = 'There are no domains to display.';
  /* Sort criterion: Domain: Number */
  $handler->display->display_options['sorts']['number']['id'] = 'number';
  $handler->display->display_options['sorts']['number']['table'] = 'mcadomain';
  $handler->display->display_options['sorts']['number']['field'] = 'number';

  /* Display: The Standards */
  $handler = $view->new_display('page', 'The Standards', 'page');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'The Standards';
  $handler->display->display_options['defaults']['css_class'] = FALSE;
  $handler->display->display_options['css_class'] = 'layout-grid-standards expanded clearfix';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['style_options']['row_class'] = 'layout-grid-item';
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'entity';
  $handler->display->display_options['row_options']['view_mode'] = 'teaser';
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['header'] = FALSE;
  /* Header: Embedded Content: Rendered Entity from embeddedvar */
  $handler->display->display_options['header']['embeddedvar']['id'] = 'embeddedvar';
  $handler->display->display_options['header']['embeddedvar']['table'] = 'embeddedcontent__global';
  $handler->display->display_options['header']['embeddedvar']['field'] = 'embeddedvar';
  $handler->display->display_options['header']['embeddedvar']['entity_type'] = 'embeddedcontent';
  $handler->display->display_options['header']['embeddedvar']['bypass_access'] = 0;
  $handler->display->display_options['header']['embeddedvar']['machine_name'] = 'standards_listing_top';
  $handler->display->display_options['header']['embeddedvar']['replace_page_title'] = 1;
  $handler->display->display_options['path'] = 'standards';

  /* Display: Statements */
  $handler = $view->new_display('block', 'Statements', 'statements');
  $handler->display->display_options['defaults']['css_class'] = FALSE;
  $handler->display->display_options['css_class'] = 'table-wrapper--highlighted';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['default_row_class'] = FALSE;
  $handler->display->display_options['style_options']['row_class_special'] = FALSE;
  $handler->display->display_options['style_options']['columns'] = array(
    'name' => 'name',
    'field_domain_statement' => 'field_domain_statement',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'name' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_domain_statement' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  $handler->display->display_options['style_options']['override'] = FALSE;
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Domain: Name */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'mcadomain';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  $handler->display->display_options['fields']['name']['label'] = 'Domain Name';
  $handler->display->display_options['fields']['name']['element_type'] = 'p';
  $handler->display->display_options['fields']['name']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['name']['element_default_classes'] = FALSE;
  /* Field: Domain: Statement */
  $handler->display->display_options['fields']['field_domain_statement']['id'] = 'field_domain_statement';
  $handler->display->display_options['fields']['field_domain_statement']['table'] = 'field_data_field_domain_statement';
  $handler->display->display_options['fields']['field_domain_statement']['field'] = 'field_domain_statement';
  $handler->display->display_options['fields']['field_domain_statement']['label'] = 'Minimum Standard Statement';
  $export['standards'] = $view;

  return $export;
}
