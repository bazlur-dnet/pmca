<?php
/**
 * @file
 * mcaet_domain.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function mcaet_domain_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'domain entity access overview'.
  $permissions['domain entity access overview'] = array(
    'name' => 'domain entity access overview',
    'roles' => array(
      'administrator user' => 'administrator user',
    ),
    'module' => 'mcaentity_domain',
  );

  // Exported permission: 'domain entity administer'.
  $permissions['domain entity administer'] = array(
    'name' => 'domain entity administer',
    'roles' => array(),
    'module' => 'mcaentity_domain',
  );

  // Exported permission: 'domain entity create'.
  $permissions['domain entity create'] = array(
    'name' => 'domain entity create',
    'roles' => array(
      'administrator user' => 'administrator user',
    ),
    'module' => 'mcaentity_domain',
  );

  // Exported permission: 'domain entity delete'.
  $permissions['domain entity delete'] = array(
    'name' => 'domain entity delete',
    'roles' => array(
      'administrator user' => 'administrator user',
    ),
    'module' => 'mcaentity_domain',
  );

  // Exported permission: 'domain entity edit'.
  $permissions['domain entity edit'] = array(
    'name' => 'domain entity edit',
    'roles' => array(
      'administrator user' => 'administrator user',
    ),
    'module' => 'mcaentity_domain',
  );

  // Exported permission: 'domain entity view'.
  $permissions['domain entity view'] = array(
    'name' => 'domain entity view',
    'roles' => array(
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'mcaentity_domain',
  );

  return $permissions;
}
