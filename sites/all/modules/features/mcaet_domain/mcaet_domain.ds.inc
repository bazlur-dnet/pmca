<?php
/**
 * @file
 * mcaet_domain.ds.inc
 */

/**
 * Implements hook_ds_field_settings_info().
 */
function mcaet_domain_ds_field_settings_info() {
  $export = array();

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'domain|simple|default';
  $ds_fieldsetting->entity_type = 'domain';
  $ds_fieldsetting->bundle = 'simple';
  $ds_fieldsetting->view_mode = 'default';
  $ds_fieldsetting->settings = array(
    'icon' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'icon',
      'formatter_settings' => array(
        'size' => '4em',
        'ft' => array(),
      ),
    ),
  );
  $export['domain|simple|default'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'domain|simple|full';
  $ds_fieldsetting->entity_type = 'domain';
  $ds_fieldsetting->bundle = 'simple';
  $ds_fieldsetting->view_mode = 'full';
  $ds_fieldsetting->settings = array(
    'icon' => array(
      'weight' => '1',
      'label' => 'hidden',
      'format' => 'icon',
      'formatter_settings' => array(
        'size' => '4em',
        'ft' => array(),
      ),
    ),
    'back_link' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'target' => 'directory',
        'ft' => array(
          'func' => 'theme_ds_field_reset',
        ),
      ),
    ),
    'domain_links' => array(
      'weight' => '3',
      'label' => 'hidden',
      'format' => 'links',
    ),
    'name_field' => array(
      'formatter_settings' => array(
        'ft' => array(
          'func' => 'theme_ds_field_reset',
        ),
      ),
    ),
  );
  $export['domain|simple|full'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'domain|simple|performance';
  $ds_fieldsetting->entity_type = 'domain';
  $ds_fieldsetting->bundle = 'simple';
  $ds_fieldsetting->view_mode = 'performance';
  $ds_fieldsetting->settings = array(
    'icon' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'icon',
      'formatter_settings' => array(
        'size' => '3em',
        'ft' => array(
          'func' => 'theme_ds_field_reset',
        ),
      ),
    ),
    'number' => array(
      'weight' => '1',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'prefix' => 'Domain ',
        'ft' => array(
          'func' => 'theme_ds_field_reset',
        ),
      ),
    ),
    'performance_map' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'links',
      'formatter_settings' => array(
        'ft' => array(
          'func' => 'theme_ds_field_reset',
        ),
      ),
    ),
    'name_field' => array(
      'formatter_settings' => array(
        'ft' => array(
          'func' => 'theme_ds_field_reset',
        ),
      ),
    ),
  );
  $export['domain|simple|performance'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'domain|simple|score';
  $ds_fieldsetting->entity_type = 'domain';
  $ds_fieldsetting->bundle = 'simple';
  $ds_fieldsetting->view_mode = 'score';
  $ds_fieldsetting->settings = array(
    'icon' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'icon',
      'formatter_settings' => array(
        'size' => '4em',
        'ft' => array(),
      ),
    ),
    'back_link' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'target' => 'next',
        'ft' => array(
          'func' => 'theme_ds_field_reset',
        ),
      ),
    ),
    'self_scoring' => array(
      'weight' => '3',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'ft' => array(
          'func' => 'theme_field',
        ),
      ),
    ),
    'field_domain_scoring_information' => array(
      'formatter_settings' => array(
        'ft' => array(
          'func' => 'theme_ds_field_helpertheme_block',
          'lb' => 'Score Your Organisation',
          'lb-col' => TRUE,
        ),
      ),
    ),
    'field_domain_scoring_quest' => array(
      'formatter_settings' => array(
        'ft' => array(
          'func' => 'theme_field',
          'lb' => 'Download the questionnaire with scoring details',
        ),
      ),
    ),
    'name_field' => array(
      'formatter_settings' => array(
        'ft' => array(
          'func' => 'theme_ds_field_reset',
        ),
      ),
    ),
  );
  $export['domain|simple|score'] = $ds_fieldsetting;

  return $export;
}

/**
 * Implements hook_ds_layout_settings_info().
 */
function mcaet_domain_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'domain|simple|default';
  $ds_layout->entity_type = 'domain';
  $ds_layout->bundle = 'simple';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'simplelayout';
  $ds_layout->settings = array(
    'regions' => array(
      'main' => array(
        0 => 'icon',
        1 => 'name_field',
      ),
    ),
    'fields' => array(
      'icon' => 'main',
      'name_field' => 'main',
    ),
    'classes' => array(
      'layout_class' => array(
        '' => '',
      ),
      'main' => array(
        'layout-block' => 'layout-block',
        'layout-separator' => 'layout-separator',
      ),
    ),
    'wrappers' => array(
      'main' => 'div',
      'ds_hidden' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => 'content',
    'layout_link_custom' => '',
  );
  $export['domain|simple|default'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'domain|simple|full';
  $ds_layout->entity_type = 'domain';
  $ds_layout->bundle = 'simple';
  $ds_layout->view_mode = 'full';
  $ds_layout->layout = 'mobjectstacked';
  $ds_layout->settings = array(
    'regions' => array(
      'above' => array(
        0 => 'back_link',
      ),
      'side' => array(
        1 => 'icon',
      ),
      'main' => array(
        2 => 'name_field',
        3 => 'domain_links',
      ),
      'below' => array(
        4 => 'field_domain_statement',
        5 => 'field_domain_description',
      ),
    ),
    'fields' => array(
      'back_link' => 'above',
      'icon' => 'side',
      'name_field' => 'main',
      'domain_links' => 'main',
      'field_domain_statement' => 'below',
      'field_domain_description' => 'below',
    ),
    'classes' => array(
      'layout_class' => array(
        'clearfix' => 'clearfix',
        'layout-separator' => 'layout-separator',
      ),
      'above' => array(
        'layout-back-block' => 'layout-back-block',
        'layout-separator' => 'layout-separator',
      ),
    ),
    'wrappers' => array(
      'above' => 'div',
      'side' => 'div',
      'main' => 'div',
      'below' => 'div',
      'ds_hidden' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
    'hide_page_title' => '1',
    'page_option_title' => '',
  );
  $export['domain|simple|full'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'domain|simple|performance';
  $ds_layout->entity_type = 'domain';
  $ds_layout->bundle = 'simple';
  $ds_layout->view_mode = 'performance';
  $ds_layout->layout = 'mobjectstacked';
  $ds_layout->settings = array(
    'regions' => array(
      'side' => array(
        0 => 'icon',
      ),
      'above' => array(
        1 => 'performance_map',
      ),
      'main' => array(
        2 => 'number',
        3 => 'name_field',
      ),
    ),
    'fields' => array(
      'icon' => 'side',
      'performance_map' => 'above',
      'number' => 'main',
      'name_field' => 'main',
    ),
    'classes' => array(
      'layout_class' => array(
        '' => '',
      ),
    ),
    'wrappers' => array(
      'above' => 'div',
      'side' => 'div',
      'main' => 'div',
      'below' => 'div',
      'ds_hidden' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['domain|simple|performance'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'domain|simple|score';
  $ds_layout->entity_type = 'domain';
  $ds_layout->bundle = 'simple';
  $ds_layout->view_mode = 'score';
  $ds_layout->layout = 'mobjectstacked';
  $ds_layout->settings = array(
    'regions' => array(
      'above' => array(
        0 => 'back_link',
      ),
      'side' => array(
        1 => 'icon',
      ),
      'below' => array(
        2 => 'field_domain_scoring_quest',
        3 => 'field_domain_scoring_information',
        4 => 'self_scoring',
      ),
      'main' => array(
        5 => 'name_field',
      ),
    ),
    'fields' => array(
      'back_link' => 'above',
      'icon' => 'side',
      'field_domain_scoring_quest' => 'below',
      'field_domain_scoring_information' => 'below',
      'self_scoring' => 'below',
      'name_field' => 'main',
    ),
    'classes' => array(
      'layout_class' => array(
        'clearfix' => 'clearfix',
        'layout-separator' => 'layout-separator',
      ),
      'above' => array(
        'layout-back-block' => 'layout-back-block',
        'layout-separator' => 'layout-separator',
      ),
    ),
    'wrappers' => array(
      'above' => 'div',
      'side' => 'div',
      'main' => 'div',
      'below' => 'div',
      'ds_hidden' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['domain|simple|score'] = $ds_layout;

  return $export;
}
