<?php
/**
 * @file
 * mcaet_domain.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function mcaet_domain_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_scores|domain|simple|form';
  $field_group->group_name = 'group_scores';
  $field_group->entity_type = 'domain';
  $field_group->bundle = 'simple';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Service Provider Performance Scores',
    'weight' => '3',
    'children' => array(
      0 => 'field_domain_nepal_score',
      1 => 'field_domain_bangladesh_score',
      2 => 'field_domain_india_score',
      3 => 'field_domain_whole_region_score',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Service Provider Performance Scores',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-scores field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $export['group_scores|domain|simple|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_scoring_information|domain|simple|form';
  $field_group->group_name = 'group_scoring_information';
  $field_group->entity_type = 'domain';
  $field_group->bundle = 'simple';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Organisation Scoring Information',
    'weight' => '4',
    'children' => array(
      0 => 'field_domain_scoring_quest',
      1 => 'field_domain_scoring_information',
      2 => 'field_domain_hide_link',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Organisation Scoring Information',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-scoring-information field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $export['group_scoring_information|domain|simple|form'] = $field_group;

  return $export;
}
