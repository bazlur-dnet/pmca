<?php
/**
 * @file
 * mcaet_provider.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function mcaet_provider_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_private_fields|provider|simple|form';
  $field_group->group_name = 'group_private_fields';
  $field_group->entity_type = 'provider';
  $field_group->bundle = 'simple';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Private Fields',
    'weight' => '12',
    'children' => array(
      0 => 'field_prv_inv_fam_reunification',
      1 => 'field_prv_inv_surv_repatriation',
      2 => 'field_prv_total_no_of_staff',
      3 => 'field_prv_address',
      4 => 'field_prv_domain_scores',
      5 => 'field_prv_funding_sources',
      6 => 'field_prv_type_health_orgs',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Private Fields',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-private-fields field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $export['group_private_fields|provider|simple|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_public_fields|provider|simple|form';
  $field_group->group_name = 'group_public_fields';
  $field_group->entity_type = 'provider';
  $field_group->bundle = 'simple';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Public Fields',
    'weight' => '1',
    'children' => array(
      0 => 'field_prv_location',
      1 => 'field_prv_catchment_area',
      2 => 'field_prv_services_offered',
      3 => 'field_prv_website',
      4 => 'field_prv_shelter_capacity',
      5 => 'field_prv_cost_service_provision',
      6 => 'field_prv_trafficking_surv_only',
      7 => 'field_prv_ages_genders_served',
      8 => 'field_prv_how_to_contact',
      9 => 'field_prv_no_shelters_operated',
      10 => 'field_prv_type_clients_served',
      11 => 'field_prv_shelter_serv_offered',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Public Fields',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-public-fields field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $export['group_public_fields|provider|simple|form'] = $field_group;

  return $export;
}
