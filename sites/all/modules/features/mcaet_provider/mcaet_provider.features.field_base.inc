<?php
/**
 * @file
 * mcaet_provider.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function mcaet_provider_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_loc_coordinates'
  $field_bases['field_loc_coordinates'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_loc_coordinates',
    'indexes' => array(
      'bbox' => array(
        0 => 'top',
        1 => 'bottom',
        2 => 'left',
        3 => 'right',
      ),
      'bottom' => array(
        0 => 'bottom',
      ),
      'centroid' => array(
        0 => 'lat',
        1 => 'lon',
      ),
      'geohash' => array(
        0 => 'geohash',
      ),
      'lat' => array(
        0 => 'lat',
      ),
      'left' => array(
        0 => 'left',
      ),
      'lon' => array(
        0 => 'lon',
      ),
      'right' => array(
        0 => 'right',
      ),
      'top' => array(
        0 => 'top',
      ),
    ),
    'locked' => 0,
    'module' => 'geofield',
    'settings' => array(
      'backend' => 'default',
      'srid' => 4326,
    ),
    'translatable' => 0,
    'type' => 'geofield',
  );

  // Exported field_base: 'field_prv_address'
  $field_bases['field_prv_address'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_prv_address',
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(),
    'translatable' => 0,
    'type' => 'text_long',
  );

  // Exported field_base: 'field_prv_ages_genders_served'
  $field_bases['field_prv_ages_genders_served'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_prv_ages_genders_served',
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'females-over-18' => 'Females over 18',
        'females-under-18' => 'Females under 18',
        'males-over-18' => 'Males over 18',
        'males-under-18' => 'Males under 18',
      ),
      'allowed_values_function' => '',
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  // Exported field_base: 'field_prv_catchment_area'
  $field_bases['field_prv_catchment_area'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_prv_catchment_area',
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(),
    'translatable' => 0,
    'type' => 'text_long',
  );

  // Exported field_base: 'field_prv_cost_service_provision'
  $field_bases['field_prv_cost_service_provision'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_prv_cost_service_provision',
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'free-for-trafficking-survivors' => 'Free of charge for trafficking survivors',
      ),
      'allowed_values_function' => '',
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  // Exported field_base: 'field_prv_domain_scores'
  $field_bases['field_prv_domain_scores'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_prv_domain_scores',
    'indexes' => array(
      'target_id' => array(
        0 => 'target_id',
      ),
    ),
    'locked' => 0,
    'module' => 'entityreference',
    'settings' => array(
      'handler' => 'base',
      'handler_settings' => array(
        'behaviors' => array(
          'views-select-list' => array(
            'status' => 0,
          ),
        ),
        'sort' => array(
          'direction' => 'ASC',
          'property' => 'score',
          'type' => 'property',
        ),
        'target_bundles' => array(),
      ),
      'target_type' => 'score',
    ),
    'translatable' => 0,
    'type' => 'entityreference',
  );

  // Exported field_base: 'field_prv_funding_sources'
  $field_bases['field_prv_funding_sources'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_prv_funding_sources',
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'government' => 'Government',
        'private-sources' => 'Private sources',
        'public-donations' => 'Public donations',
        'ngo-donors-national' => 'NGO donors – national',
        'ngo-donors-international' => 'NGO donors – international',
        'fees-for-services' => 'Fees for services',
      ),
      'allowed_values_function' => '',
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  // Exported field_base: 'field_prv_how_to_contact'
  $field_bases['field_prv_how_to_contact'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_prv_how_to_contact',
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'through-our-website' => 'Through our website',
        'referral-from-police-or-judiciary-courts' => 'Referral from police or judiciary/courts',
        'referral-from-other-government-agency' => 'Referral from other government agency',
        'referral-from-other-service-providers' => 'Referral from other service providers',
        'word-of-mouth-from-public' => 'Word of mouth from public',
        'we-advertise-in-the-media' => 'We advertise in the media',
        'referral-from-other-survivors' => 'Referral from other survivors',
        'survivors-can-approach-us-themselves' => 'Survivors can approach us themselves',
        'outreach-services' => 'Outreach services',
      ),
      'allowed_values_function' => '',
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  // Exported field_base: 'field_prv_inv_fam_reunification'
  $field_bases['field_prv_inv_fam_reunification'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_prv_inv_fam_reunification',
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'no' => 'No',
        'yes' => 'Yes',
      ),
      'allowed_values_function' => '',
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  // Exported field_base: 'field_prv_inv_surv_repatriation'
  $field_bases['field_prv_inv_surv_repatriation'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_prv_inv_surv_repatriation',
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'no' => 'No',
        'yes' => 'Yes',
      ),
      'allowed_values_function' => '',
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  // Exported field_base: 'field_prv_location'
  $field_bases['field_prv_location'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_prv_location',
    'indexes' => array(
      'tid' => array(
        0 => 'tid',
      ),
    ),
    'locked' => 0,
    'module' => 'taxonomy',
    'settings' => array(
      'allowed_values' => array(
        0 => array(
          'vocabulary' => 'location',
          'parent' => 0,
        ),
      ),
      'options_list_callback' => 'title_taxonomy_allowed_values',
    ),
    'translatable' => 0,
    'type' => 'taxonomy_term_reference',
  );

  // Exported field_base: 'field_prv_no_shelters_operated'
  $field_bases['field_prv_no_shelters_operated'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_prv_no_shelters_operated',
    'indexes' => array(),
    'locked' => 0,
    'module' => 'number',
    'settings' => array(),
    'translatable' => 0,
    'type' => 'number_integer',
  );

  // Exported field_base: 'field_prv_services_offered'
  $field_bases['field_prv_services_offered'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_prv_services_offered',
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'family-reunification' => 'Family reunification',
        'health-services' => 'Health services',
        'hiv-rehabilitation' => 'HIV/AIDS rehabilitation',
        'institutional-care' => 'Institutional care',
        'legal-services' => 'Legal aid and prosecution related services',
        'life-skill-education' => 'Life skill education',
        'livelihood-support' => 'Livelihood support',
        'non-formal-education' => 'Non-formal education',
        'reintegration' => 'Reintegration and/or alternative reintegration',
        'repatriation' => 'Repatriation',
        'rescue-services' => 'Rescue and post rescue services',
      ),
      'allowed_values_function' => '',
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  // Exported field_base: 'field_prv_shelter_capacity'
  $field_bases['field_prv_shelter_capacity'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_prv_shelter_capacity',
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        '1-10' => '1-10',
        '11-30' => '11-30',
        '31-70' => '31-70',
        '71-100' => '71-100',
        '101-400' => '101-400',
        '401-800' => '401-800',
        '801-1000' => '801-1000',
        '1001-plus' => '1001+',
      ),
      'allowed_values_function' => '',
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  // Exported field_base: 'field_prv_shelter_serv_offered'
  $field_bases['field_prv_shelter_serv_offered'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_prv_shelter_serv_offered',
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'health-services' => 'Health services',
        'hiv-services' => 'HIV related health services',
        'counseling-services' => 'Counseling services',
        'food-clothing-kit' => 'Food, clothing and basic introduction kit',
        'non-formal-education' => 'Non-formal education',
        'life-skill-education' => 'Life skill education',
        'vocational-training' => 'Vocational training',
        'repatriation' => 'Repatriation',
        'legal-services' => 'Legal aid and prosecution related services',
        'family-reunification' => 'Family reunification',
      ),
      'allowed_values_function' => '',
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  // Exported field_base: 'field_prv_total_no_of_staff'
  $field_bases['field_prv_total_no_of_staff'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_prv_total_no_of_staff',
    'indexes' => array(),
    'locked' => 0,
    'module' => 'number',
    'settings' => array(),
    'translatable' => 0,
    'type' => 'number_integer',
  );

  // Exported field_base: 'field_prv_trafficking_surv_only'
  $field_bases['field_prv_trafficking_surv_only'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_prv_trafficking_surv_only',
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'no' => 'No',
        'yes' => 'Yes',
      ),
      'allowed_values_function' => '',
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  // Exported field_base: 'field_prv_type_clients_served'
  $field_bases['field_prv_type_clients_served'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_prv_type_clients_served',
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'male-children-under-18' => 'Male Survivors/victims of trafficking – children (under 18)',
        'female-children-under-18' => 'Female Survivors/victims of trafficking – children (under 18)',
        'male-adults-18-or-over' => 'Male Survivors/victims of trafficking – adults (18 or over)',
        'female-adults-18-or-over' => 'Female Survivors/victims of trafficking – adults (18 or over)',
        'other-victims-of-exploitation' => 'Other victims of exploitation',
        'missing-children' => 'Missing children',
      ),
      'allowed_values_function' => '',
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  // Exported field_base: 'field_prv_type_health_orgs'
  $field_bases['field_prv_type_health_orgs'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_prv_type_health_orgs',
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'government-hospital' => 'Government hospital',
        'private-hospitals' => 'Private hospitals',
        'private-practitioners' => 'Private practitioners',
      ),
      'allowed_values_function' => '',
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  // Exported field_base: 'field_prv_website'
  $field_bases['field_prv_website'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_prv_website',
    'indexes' => array(),
    'locked' => 0,
    'module' => 'link',
    'settings' => array(
      'attributes' => array(
        'class' => '',
        'rel' => '',
        'target' => 'default',
      ),
      'display' => array(
        'url_cutoff' => 80,
      ),
      'enable_tokens' => 1,
      'title' => 'optional',
      'title_maxlength' => 128,
      'title_value' => '',
      'url' => 0,
    ),
    'translatable' => 0,
    'type' => 'link_field',
  );

  return $field_bases;
}
