<?php
/**
 * @file
 * mcaet_provider.ds.inc
 */

/**
 * Implements hook_ds_field_settings_info().
 */
function mcaet_provider_ds_field_settings_info() {
  $export = array();

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'provider|simple|default';
  $ds_fieldsetting->entity_type = 'provider';
  $ds_fieldsetting->bundle = 'simple';
  $ds_fieldsetting->view_mode = 'default';
  $ds_fieldsetting->settings = array(
    'field_prv_services_offered' => array(
      'formatter_settings' => array(
        'ft' => array(),
      ),
    ),
  );
  $export['provider|simple|default'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'provider|simple|full';
  $ds_fieldsetting->entity_type = 'provider';
  $ds_fieldsetting->bundle = 'simple';
  $ds_fieldsetting->view_mode = 'full';
  $ds_fieldsetting->settings = array(
    'field_prv_how_to_contact' => array(
      'formatter_settings' => array(
        'ft' => array(
          'func' => 'theme_field',
        ),
      ),
    ),
    'field_prv_type_clients_served' => array(
      'formatter_settings' => array(
        'ft' => array(
          'func' => 'theme_field',
        ),
      ),
    ),
    'name_field' => array(
      'formatter_settings' => array(
        'ft' => array(
          'func' => 'theme_ds_field_reset',
        ),
      ),
    ),
    'field_prv_trafficking_surv_only' => array(
      'formatter_settings' => array(
        'ft' => array(
          'func' => 'theme_field',
        ),
      ),
    ),
    'field_prv_no_shelters_operated' => array(
      'formatter_settings' => array(
        'ft' => array(
          'func' => 'theme_field',
        ),
      ),
    ),
    'field_prv_catchment_area' => array(
      'formatter_settings' => array(
        'ft' => array(
          'func' => 'theme_field',
        ),
      ),
    ),
    'field_prv_shelter_serv_offered' => array(
      'formatter_settings' => array(
        'ft' => array(
          'func' => 'theme_field',
        ),
      ),
    ),
    'field_prv_ages_genders_served' => array(
      'formatter_settings' => array(
        'ft' => array(
          'func' => 'theme_field',
        ),
      ),
    ),
    'field_prv_cost_service_provision' => array(
      'formatter_settings' => array(
        'ft' => array(
          'func' => 'theme_field',
        ),
      ),
    ),
    'field_prv_website' => array(
      'formatter_settings' => array(
        'ft' => array(
          'func' => 'theme_field',
        ),
      ),
    ),
    'field_prv_location' => array(
      'formatter_settings' => array(
        'ft' => array(
          'func' => 'theme_field',
        ),
      ),
    ),
    'field_prv_shelter_capacity' => array(
      'formatter_settings' => array(
        'ft' => array(
          'func' => 'theme_field',
        ),
      ),
    ),
    'field_prv_services_offered' => array(
      'formatter_settings' => array(
        'ft' => array(
          'func' => 'theme_field',
        ),
      ),
    ),
  );
  $export['provider|simple|full'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'provider|simple|full_private';
  $ds_fieldsetting->entity_type = 'provider';
  $ds_fieldsetting->bundle = 'simple';
  $ds_fieldsetting->view_mode = 'full_private';
  $ds_fieldsetting->settings = array(
    'field_prv_funding_sources' => array(
      'formatter_settings' => array(
        'ft' => array(
          'func' => 'theme_field',
        ),
      ),
    ),
    'field_prv_how_to_contact' => array(
      'formatter_settings' => array(
        'ft' => array(
          'func' => 'theme_field',
        ),
      ),
    ),
    'field_prv_type_clients_served' => array(
      'formatter_settings' => array(
        'ft' => array(
          'func' => 'theme_field',
        ),
      ),
    ),
    'name_field' => array(
      'formatter_settings' => array(
        'ft' => array(
          'func' => 'theme_ds_field_reset',
        ),
      ),
    ),
    'field_prv_trafficking_surv_only' => array(
      'formatter_settings' => array(
        'ft' => array(
          'func' => 'theme_field',
        ),
      ),
    ),
    'field_prv_domain_scores' => array(
      'formatter_settings' => array(
        'ft' => array(
          'func' => 'theme_field',
        ),
      ),
    ),
    'field_prv_inv_surv_repatriation' => array(
      'formatter_settings' => array(
        'ft' => array(
          'func' => 'theme_field',
        ),
      ),
    ),
    'field_prv_no_shelters_operated' => array(
      'formatter_settings' => array(
        'ft' => array(
          'func' => 'theme_field',
        ),
      ),
    ),
    'field_prv_catchment_area' => array(
      'formatter_settings' => array(
        'ft' => array(
          'func' => 'theme_field',
        ),
      ),
    ),
    'field_prv_shelter_serv_offered' => array(
      'formatter_settings' => array(
        'ft' => array(
          'func' => 'theme_field',
        ),
      ),
    ),
    'field_prv_total_no_of_staff' => array(
      'formatter_settings' => array(
        'ft' => array(
          'func' => 'theme_field',
        ),
      ),
    ),
    'field_prv_inv_fam_reunification' => array(
      'formatter_settings' => array(
        'ft' => array(
          'func' => 'theme_field',
        ),
      ),
    ),
    'field_prv_address' => array(
      'formatter_settings' => array(
        'ft' => array(
          'func' => 'theme_field',
        ),
      ),
    ),
    'field_prv_ages_genders_served' => array(
      'formatter_settings' => array(
        'ft' => array(
          'func' => 'theme_field',
        ),
      ),
    ),
    'field_prv_type_health_orgs' => array(
      'formatter_settings' => array(
        'ft' => array(
          'func' => 'theme_field',
        ),
      ),
    ),
    'field_prv_cost_service_provision' => array(
      'formatter_settings' => array(
        'ft' => array(
          'func' => 'theme_field',
        ),
      ),
    ),
    'field_prv_website' => array(
      'formatter_settings' => array(
        'ft' => array(
          'func' => 'theme_field',
        ),
      ),
    ),
    'field_prv_location' => array(
      'formatter_settings' => array(
        'ft' => array(
          'func' => 'theme_field',
        ),
      ),
    ),
    'field_prv_shelter_capacity' => array(
      'formatter_settings' => array(
        'ft' => array(
          'func' => 'theme_field',
        ),
      ),
    ),
    'field_prv_services_offered' => array(
      'formatter_settings' => array(
        'ft' => array(
          'func' => 'theme_field',
        ),
      ),
    ),
  );
  $export['provider|simple|full_private'] = $ds_fieldsetting;

  return $export;
}

/**
 * Implements hook_ds_layout_settings_info().
 */
function mcaet_provider_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'provider|simple|default';
  $ds_layout->entity_type = 'provider';
  $ds_layout->bundle = 'simple';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'simplelayout';
  $ds_layout->settings = array(
    'regions' => array(
      'main' => array(
        0 => 'name_field',
        1 => 'field_prv_location',
        2 => 'field_prv_services_offered',
      ),
    ),
    'fields' => array(
      'name_field' => 'main',
      'field_prv_location' => 'main',
      'field_prv_services_offered' => 'main',
    ),
    'classes' => array(
      'layout_class' => array(
        '' => '',
      ),
      'main' => array(
        'layout-block' => 'layout-block',
        'layout-separator' => 'layout-separator',
      ),
    ),
    'wrappers' => array(
      'main' => 'div',
      'ds_hidden' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['provider|simple|default'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'provider|simple|full';
  $ds_layout->entity_type = 'provider';
  $ds_layout->bundle = 'simple';
  $ds_layout->view_mode = 'full';
  $ds_layout->layout = 'simplelayout';
  $ds_layout->settings = array(
    'regions' => array(
      'main' => array(
        0 => 'name_field',
        1 => 'field_prv_location',
        2 => 'field_prv_catchment_area',
        3 => 'field_prv_services_offered',
        4 => 'field_prv_type_clients_served',
        5 => 'field_prv_cost_service_provision',
        6 => 'field_prv_no_shelters_operated',
        7 => 'field_prv_shelter_capacity',
        8 => 'field_prv_ages_genders_served',
        9 => 'field_prv_shelter_serv_offered',
        10 => 'field_prv_trafficking_surv_only',
        11 => 'field_prv_how_to_contact',
        12 => 'field_prv_website',
      ),
    ),
    'fields' => array(
      'name_field' => 'main',
      'field_prv_location' => 'main',
      'field_prv_catchment_area' => 'main',
      'field_prv_services_offered' => 'main',
      'field_prv_type_clients_served' => 'main',
      'field_prv_cost_service_provision' => 'main',
      'field_prv_no_shelters_operated' => 'main',
      'field_prv_shelter_capacity' => 'main',
      'field_prv_ages_genders_served' => 'main',
      'field_prv_shelter_serv_offered' => 'main',
      'field_prv_trafficking_surv_only' => 'main',
      'field_prv_how_to_contact' => 'main',
      'field_prv_website' => 'main',
    ),
    'classes' => array(
      'layout_class' => array(
        'clearfix' => 'clearfix',
        'layout-separator' => 'layout-separator',
      ),
      'main' => array(
        'layout-block' => 'layout-block',
      ),
    ),
    'wrappers' => array(
      'main' => 'div',
      'ds_hidden' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
    'hide_page_title' => '1',
    'page_option_title' => '',
  );
  $export['provider|simple|full'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'provider|simple|full_private';
  $ds_layout->entity_type = 'provider';
  $ds_layout->bundle = 'simple';
  $ds_layout->view_mode = 'full_private';
  $ds_layout->layout = 'simplelayout';
  $ds_layout->settings = array(
    'regions' => array(
      'main' => array(
        0 => 'name_field',
        1 => 'field_prv_location',
        2 => 'field_prv_catchment_area',
        3 => 'field_prv_services_offered',
        4 => 'field_prv_type_clients_served',
        5 => 'field_prv_cost_service_provision',
        6 => 'field_prv_no_shelters_operated',
        7 => 'field_prv_shelter_capacity',
        8 => 'field_prv_ages_genders_served',
        9 => 'field_prv_shelter_serv_offered',
        10 => 'field_prv_trafficking_surv_only',
        11 => 'field_prv_how_to_contact',
        12 => 'field_prv_website',
        13 => 'field_prv_address',
        14 => 'field_prv_funding_sources',
        15 => 'field_prv_total_no_of_staff',
        16 => 'field_prv_inv_fam_reunification',
        17 => 'field_prv_inv_surv_repatriation',
        18 => 'field_prv_type_health_orgs',
        19 => 'field_prv_domain_scores',
      ),
    ),
    'fields' => array(
      'name_field' => 'main',
      'field_prv_location' => 'main',
      'field_prv_catchment_area' => 'main',
      'field_prv_services_offered' => 'main',
      'field_prv_type_clients_served' => 'main',
      'field_prv_cost_service_provision' => 'main',
      'field_prv_no_shelters_operated' => 'main',
      'field_prv_shelter_capacity' => 'main',
      'field_prv_ages_genders_served' => 'main',
      'field_prv_shelter_serv_offered' => 'main',
      'field_prv_trafficking_surv_only' => 'main',
      'field_prv_how_to_contact' => 'main',
      'field_prv_website' => 'main',
      'field_prv_address' => 'main',
      'field_prv_funding_sources' => 'main',
      'field_prv_total_no_of_staff' => 'main',
      'field_prv_inv_fam_reunification' => 'main',
      'field_prv_inv_surv_repatriation' => 'main',
      'field_prv_type_health_orgs' => 'main',
      'field_prv_domain_scores' => 'main',
    ),
    'classes' => array(
      'layout_class' => array(
        'clearfix' => 'clearfix',
        'layout-separator' => 'layout-separator',
      ),
      'main' => array(
        'layout-block' => 'layout-block',
      ),
    ),
    'wrappers' => array(
      'main' => 'div',
      'ds_hidden' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['provider|simple|full_private'] = $ds_layout;

  return $export;
}
