<?php
/**
 * @file
 * mcaet_provider.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function mcaet_provider_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'delete terms in location'.
  $permissions['delete terms in location'] = array(
    'name' => 'delete terms in location',
    'roles' => array(
      'administrator user' => 'administrator user',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit terms in location'.
  $permissions['edit terms in location'] = array(
    'name' => 'edit terms in location',
    'roles' => array(
      'administrator user' => 'administrator user',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'provider entity access overview'.
  $permissions['provider entity access overview'] = array(
    'name' => 'provider entity access overview',
    'roles' => array(
      'administrator user' => 'administrator user',
    ),
    'module' => 'mcaentity_provider',
  );

  // Exported permission: 'provider entity administer'.
  $permissions['provider entity administer'] = array(
    'name' => 'provider entity administer',
    'roles' => array(),
    'module' => 'mcaentity_provider',
  );

  // Exported permission: 'provider entity create'.
  $permissions['provider entity create'] = array(
    'name' => 'provider entity create',
    'roles' => array(
      'administrator user' => 'administrator user',
    ),
    'module' => 'mcaentity_provider',
  );

  // Exported permission: 'provider entity delete'.
  $permissions['provider entity delete'] = array(
    'name' => 'provider entity delete',
    'roles' => array(
      'administrator user' => 'administrator user',
    ),
    'module' => 'mcaentity_provider',
  );

  // Exported permission: 'provider entity edit'.
  $permissions['provider entity edit'] = array(
    'name' => 'provider entity edit',
    'roles' => array(
      'administrator user' => 'administrator user',
    ),
    'module' => 'mcaentity_provider',
  );

  // Exported permission: 'provider entity view'.
  $permissions['provider entity view'] = array(
    'name' => 'provider entity view',
    'roles' => array(
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'mcaentity_provider',
  );

  return $permissions;
}
