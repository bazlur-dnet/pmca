<?php

/**
 * Controller for generating some basic metadata for CRUD entity types.
 */
class EmbeddedContentMetadataController extends EntityDefaultMetadataController {
  public function entityPropertyInfo() {
    $info = parent::entityPropertyInfo();

    foreach ($info['embeddedcontent'] as $bundle => &$bundle_info) {
      $bundle_info['title']['label'] = t('Title');

      if (isset($bundle_info['status'])) {
        $bundle_info['status']['type'] = 'boolean';
      }

      if (isset($bundle_info['uid'])) {
        $bundle_info['uid']['label'] = t('User ID');
      }
    }

    return $info;
  }
}
