<?php

/**
 * @file
 * Defines the class for the Service Provider Entity.
 */

/**
 * Main class for Embedded Content entities.
 */
class EmbeddedContentEntity extends Entity {
  // Define some default values.
  public $ecid = NULL;
  public $title = '';
  public $machine_name = '';
  public $type = '';
  public $language = LANGUAGE_NONE;
  public $uid = NULL;
  public $status = TRUE;
  public $created = NULL;
  public $changed = NULL;

  /**
   * URI method for entity_class_uri().
   */
  protected function defaultUri() {
    if (!isset($this->ecid)) {
      return NULL;
    }

    return array(
      'path' => EMBEDDEDCONTENT_UI_PATH . '/' . $this->identifier(),
    );
  }
}
