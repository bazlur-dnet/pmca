<?php

/**
 * Implements hook_views_data().
 */
function embeddedcontent_views_data() {
  $data = array();

  $data['embeddedcontent__global']['table']['group'] = t('Embedded Content');
  $data['embeddedcontent__global']['table']['join'] = array(
    // #global let's it appear all the time.
    '#global' => array(),
  );

  $data['embeddedcontent__global']['embeddedvar'] = array(
    'title' => t('Rendered Entity from embeddedvar'),
    'help' => t('Displays a single chosen entity that is referenced in a provided variable.'),
    'area' => array(
      'handler' => 'embeddedcontent_views_handler_area_entity',
    ),
  );

  return $data;
}
