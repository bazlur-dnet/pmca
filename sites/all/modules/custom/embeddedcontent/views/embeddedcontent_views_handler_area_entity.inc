<?php
/**
 * @file
 * Renders a full entity in a views area.
 */

class embeddedcontent_views_handler_area_entity extends entity_views_handler_area_entity {
  public function option_definition() {
    $options = parent::option_definition();

    unset($options['entity_id']);
    $options['machine_name'] = array('default' => '');
    $options['replace_page_title'] = array('default' => FALSE);

    return $options;
  }

  /**
   * Default options form that provides the label widget that all fields
   * should have.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    unset($form['entity_id']);
    $form['machine_name'] = array(
      '#type' => 'textfield',
      '#title' => t('Entity machine name'),
      '#description' => t('Choose the entity you want to display in the area.'),
      '#default_value' => $this->options['machine_name'],
    );

    $form['replace_page_title'] = array(
      '#type' => 'checkbox',
      '#title' => t('Replace Page Title'),
      '#description' => t('Select if you want the entity title to be used as page title.'),
      '#default_value' => $this->options['replace_page_title'],
    );

    return $form;
  }

  /**
   * Provide extra data to the administration form
   */
  public function admin_summary() {
    $label = parent::admin_summary();
    if (!empty($this->options['machine_name'])) {
      return t('@label @entity_type: @machine_name', array(
        '@label' => $label,
        '@entity_type' => $this->options['entity_type'],
        '@machine_name' => $this->options['machine_name'],
      ));
    }
  }

  /**
   * @param bool|FALSE $empty
   * @return string
   */
  public function render($empty = FALSE) {
    if (!$empty || !empty($this->options['empty'])) {
      return $this->render_entity($this->options['entity_type'], $this->options['machine_name'], $this->options['view_mode']);
    }
    return '';
  }

  /**
   * Render an entity using the view mode.
   *
   * @param $entity_type
   * @param $machine_name
   * @param $view_mode
   *
   * @return string
   */
  public function render_entity($entity_type, $machine_name, $view_mode) {
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', $entity_type);

    $query->propertyCondition('machine_name', $machine_name);

    $results = $query->execute();
    if (!empty($results[$entity_type])) {
      $entity_id = key($results[$entity_type]);

      if (is_numeric($entity_id)) {
        if ($this->options['replace_page_title']) {
          embeddedcontent_alternative_page_title_set_status(TRUE);
        }

        $output = parent::render_entity($entity_type, $entity_id, $view_mode);

        $attributes = array(
          'class' => array(
            'views-embeddedcontent-wrapper',
            drupal_html_class('views-embeddedcontent-' . $machine_name),
            'clearfix',
          ),
        );

        return '<div ' . drupal_attributes($attributes) . '>' . $output . '</div>';
      }
    }

    return '';
  }
}
