<?php

/**
 * Implements hook_field_formatter_info().
 */
function mcaentity_score_field_formatter_info() {
  return array(
    'entityreference_score_info' => array(
      'label' => t('Score Entity Info'),
      'description' => t('Display the information of the referenced score entities.'),
      'field types' => array('entityreference'),
      'settings' => array(
        'list_type' => 'ul',
        'icon_size' => '2em',
      ),
    ),
  );
}

/**
 * Implements hook_field_formatter_settings_form().
 */
function mcaentity_score_field_formatter_settings_form($field, $instance, $view_mode, $form, &$form_state) {
  $element = array();

  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];

  if ($display['type'] == 'entityreference_score_info') {
    if ($field['cardinality'] != '1') {
      $element['list_type'] = array(
        '#type' => 'radios',
        '#title' => t('list type'),
        '#options' => array(
          'ul' => t('Unordered list'),
          'ol' => t('Ordered list'),
        ),
        '#default_value' => $settings['list_type'],
      );

      $element['icon_size'] = array(
        '#type' => 'textfield',
        '#title' => t('Icon size'),
        '#description' => t('Provide the size of the icon in EMs. Eg: 2em'),
        '#default_value' => $settings['icon_size'],
      );
    }
  }

  return $element;
}

/**
 * Implements hook_field_formatter_settings_summary().
 */
function mcaentity_score_field_formatter_settings_summary($field, $instance, $view_mode) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];

  $summary = array();

  if ($display['type'] == 'entityreference_score_info') {
    $summary[] = t('List Type') . ': "' . check_plain($settings['list_type']) . '"';
    $summary[] = t('Icon size') . ': "' . check_plain($settings['icon_size']) . '"';
  }

  return implode('<br />', $summary);
}

/**
 * Implements hook_field_formatter_view().
 */
function mcaentity_score_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $result = array();

  $function = '__' . __FUNCTION__ . '__' . $display['type'];

  switch ($display['type']) {
    case 'entityreference_score_info':
      if (function_exists($function)) {
        $result = $function($entity_type, $entity, $field, $instance, $langcode, $items, $display);
      }
      break;
  }

  return $result;
}

/**
 * Implements hook_field_formatter_view() for 'entityreference_score_info'.
 */
function __mcaentity_score_field_formatter_view__entityreference_score_info($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  if (empty($items)) {
    return array();
  }

  $settings = $display['settings'];

  // Prepare a list of score entities.
  $score_entity_ids = array();
  foreach ($items as $delta => $item) {
    if (!empty($item['target_id'])) {
      $score_entity_ids[] = $item['target_id'];
    }
  }
  $score_entities = entity_load('score', $score_entity_ids);

  // Prepare a list of domain entities and add scores to the items list.
  $domain_ids = array();
  foreach ($items as $delta => &$item) {
    if (empty($item['target_id'])) {
      continue;
    }

    $score_id = $item['target_id'];
    if (empty($score_entities[$score_id])) {
      continue;
    }

    /** @var ScoreEntity $entity */
    $score_entity = $score_entities[$score_id];
    $domain_ids[$delta] = $score_entity->domain_id;

    $item['domain_score'] = $score_entity->score;
    $item['domain_id'] = $score_entity->domain_id;

    unset($score_id);
    unset($score_entity);
  }
  unset($score_entities);

  $domain_entities = entity_load('domain', $domain_ids);
  foreach ($items as $delta => &$item) {
    $domain_id = $item['domain_id'];
    if (empty($domain_entities[$domain_id])) {
      continue;
    }

    /** @var DomainEntity $entity */
    $domain_entity = $domain_entities[$domain_id];
    $item['domain_name'] = $domain_entity->label();
    if (isset($domain_entity->icon)) {
      $item['domain_icon'] = $domain_entity->icon;
    }

    unset($domain_id);
    unset($domain_entity);
  }
  unset($domain_entities);

  $list_items = array();
  foreach ($items as $delta => &$item) {
    if (empty($item['domain_name'])) {
      continue;
    }

    $icon_options = array(
      'width' => $settings['icon_size'],
      'height' => $settings['icon_size'],
      'sprite_name' => 'domains',
      'class' => array('domain-icon'),
    );
    $icon_markup = helpertheme_get_svg_icons($item['domain_icon'], $icon_options);

    $item_markup = '<span class="domain-score-wrapper">';
    $item_markup .= '<span class="domain-icon">' . $icon_markup . '</span> ';
    $item_markup .= '<span class="domain-name">' . check_plain($item['domain_name']) . '</span> ';
    $item_markup .= '<span class="domain-score">' . check_plain($item['domain_score']) . '</span>';
    $item_markup .= '</span>';

    $list_items[$delta] = $item_markup;
  }

  if (empty($list_items)) {
    return array();
  }

  $result[] = array(
    '#theme' => 'item_list',
    '#items' => $list_items,
    '#title' => NULL,
    '#type' => $settings['list_type'],
  );

  return $result;
}
