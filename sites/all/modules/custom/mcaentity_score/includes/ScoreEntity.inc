<?php

/**
 * @file
 * Defines the class for the Service Provider Score Entity.
 */

/**
 * Main class for Service Provider entities.
 */
class ScoreEntity extends Entity {
  // Define some default values.
  public $sid = NULL;
  public $domain_id = NULL;
  public $score = NULL;
  //public $user_id = NULL;
  public $status = TRUE;
  public $created = NULL;
  public $changed = NULL;

  /**
   * URI method for entity_class_uri().
   */
  protected function defaultUri() {
    return NULL;
  }
}
