<?php

/**
 * @file
 * Defines the inline entity form controller for Service Provider Scores.
 */

class ScoreInlineEntityFormController extends EntityInlineEntityFormController {

  /**
  * Overrides EntityInlineEntityFormController::defaultLabels().
   */
  public function defaultLabels() {
    $labels = array(
      'singular' => t('score'),
      'plural' => t('scores'),
    );
    return $labels;
  }

  /**
   * Returns an array of default settings in the form of key => value.
   */
  public function defaultSettings() {
    $defaults = parent::defaultSettings();
    return $defaults;
  }

  /**
   * Overrides EntityInlineEntityFormController::tableFields().
   */
  public function tableFields($bundles) {
    $fields = array();

    $fields['domain'] = array(
      'type' => 'callback',
      'render_callback' => 'mcaentity_score_get_domain_label',
      'label' => t('Domain'),
      'weight' => 20,
    );

    $fields['score'] = array(
      'type' => 'property',
      'label' => t('Score'),
      'weight' => 30,
    );

    return $fields;
  }

  /**
   * Overrides EntityInlineEntityFormController::entityForm().
   *
   * @param $entity_form
   * @param $form_state
   * @return array
   */
  public function entityForm($entity_form, &$form_state) {
    /** @var ScoreEntity $entity */
    $entity = $entity_form['#entity'];

    // Get a list of domains sorted by weight and name.
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'domain');
    $query->propertyOrderBy('number');
    $query->propertyOrderBy('name');
    $results = $query->execute();

    $domain_options = array();
    foreach ($results['domain'] as $result) {
      // @TODO: Optimize!
      $domain = entity_load_single('domain', $result->did);

      $domain_options[$domain->identifier()] = filter_xss($domain->label());
    }

    $entity_form['domain_id'] = array(
      '#type' => 'select',
      '#required' => TRUE,
      '#title' => t('Domain'),
      '#default_value' => $entity->domain_id,
      '#options' => $domain_options,
      '#description' => t('The domain.'),
    );

    $entity_form['score'] = array(
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => t('Score'),
      '#default_value' => $entity->score,
      '#description' => t('The domain score.'),
    );

    return $entity_form;
  }

  /**
   * Overrides EntityInlineEntityFormController::entityFormSubmit().
   *
   * TODO: Deal with internationalization.
   */
  public function entityFormValidate($entity_form, &$form_state) {
    $parents_path = implode('][', $entity_form['#parents']);
    $score_form_values = drupal_array_get_nested_value($form_state['values'], $entity_form['#parents']);

    if (empty($score_form_values)) {
      return;
    }

    // Extra validation for the score value.
    if (!is_numeric($score_form_values['score']) || $score_form_values['score'] < 0 || $score_form_values['score'] > 100 ) {
      form_set_error($parents_path . '][score', t('You must specify a number between 0 and 100 for the domain score.'));
    }

    // Extra validation for the domain value, it needs to be unique in the form.
    $inline_entity_form = reset($form_state['inline_entity_form']);
    foreach($inline_entity_form['entities'] as $delta => $entry) {
      // Skip current entry.
      if ($delta === $entity_form['#ief_row_delta']) {
        continue;
      }

      if (empty($entry['entity'])) {
        continue;
      }

      if ($entry['entity']->identifier() == $score_form_values['domain_id']) {
        form_set_error($parents_path . '][domain_id', t('Only one score per domain is allowed.'));
      }
    }
  }

  /**
   * Overrides EntityInlineEntityFormController::entityFormSubmit().
   */
  public function entityFormSubmit(&$entity_form, &$form_state) {
    parent::entityFormSubmit($entity_form, $form_state);
  }

  /**
   * @param $entity_type
   * @param ScoreEntity $entity
   *
   * @return string
   */
  public static function getDomainName($entity_type, $entity) {
    if (!empty($entity->domain_id)) {
      /** @var DomainEntity $domain_entity */
      $domain_entity = entity_load_single('domain', $entity->domain_id);
      if (!empty($domain_entity)) {
        return filter_xss($domain_entity->label());
      }
    }

    return NULL;
  }
}
