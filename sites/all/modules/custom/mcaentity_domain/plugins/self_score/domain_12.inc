<?php
/**
 * @file
 * Self Scoring plugin.
 */

$plugin = array(
  'domain id' => '12',
  'title' => t('Self Scoring Widget for Domain 12'),
  'handler' => 'SelfScoreWidgetDomain12',
);


class SelfScoreWidgetDomain12 extends mcaEntityDomainSelfScoreWidgetBase {

  public function buildForm(&$form, &$form_state) {
    parent::buildForm($form, $form_state);

    $form['hr_policy'] = array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
    );
    $form['hr_policy'][] = array(
      '#type' => 'radios',
      '#title' => t('1. Do you have a formal HR Policy?'),
      '#default_value' => FALSE,
      '#options' => array(
        'yes' => t('Yes'),
        'no' => t('No'),
      ),
      '#score_points' => 25,
    );


    $form['external_monitoring'] = array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
    );
    $form['external_monitoring'][] = array(
      '#type' => 'radios',
      '#title' => t('2. Is your organisation monitored by an external agent, such as donors or government agencies?'),
      '#default_value' => FALSE,
      '#options' => array(
        'yes' => t('Yes'),
        'no' => t('No'),
      ),
      '#score_points' => 25,
    );


    $form['survivors_participate'] = array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
    );
    $form['survivors_participate'][] = array(
      '#type' => 'radios',
      '#title' => t('3. Do survivors participate in the monitoring?'),
      '#default_value' => FALSE,
      '#options' => array(
        'yes' => t('Yes'),
        'no' => t('No'),
      ),
      '#score_points' => 10,
    );


    $form['stress'] = array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
    );
    $form['stress'][] = array(
      '#type' => 'radios',
      '#title' => t('4. Do you have a mechanism to help care-givers who work directly with the survivors deal with the stress of their activities?'),
      '#default_value' => FALSE,
      '#options' => array(
        'yes' => t('Yes'),
        'no' => t('No'),
      ),
      '#score_points' => 20,
    );


    $form['complaint_redress'] = array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
    );
    $form['complaint_redress'][] = array(
      '#type' => 'radios',
      '#title' => t('5. Are complaint redress systems in place?'),
      '#default_value' => FALSE,
      '#options' => array(
        'yes' => t('Yes'),
        'no' => t('No'),
      ),
      '#score_points' => 20,
    );


    return $form;
  }
}
