<?php
/**
 * @file
 * Self Scoring plugin.
 */

$plugin = array(
  'domain id' => '3',
  'title' => t('Self Scoring Widget for Domain 3'),
  'handler' => 'SelfScoreWidgetDomain3',
);


class SelfScoreWidgetDomain3 extends mcaEntityDomainSelfScoreWidgetBase {

  public function buildForm(&$form, &$form_state) {
    parent::buildForm($form, $form_state);

    $form['hazardous_areas'] = array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
    );
    $form['hazardous_areas'][] = array(
      '#type' => 'radios',
      '#title' => t('1. The shelter is removed from places of exploitation or other hazardous areas, such as industrial sites, transportation hubs etc.'),
      '#default_value' => FALSE,
      '#options' => array(
        'yes' => t('Yes'),
        'no' => t('No'),
      ),
      '#score_points' => 50,
    );


    $form['location'] = array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
    );
    $form['location'][] = array(
      '#type' => 'radios',
      '#title' => t('2. The shelter is close (a short walking distance) to doctor’s clinic/hospital, police station and market?'),
      '#default_value' => FALSE,
      '#options' => array(
        'yes' => t('Yes'),
        'no' => t('No'),
      ),
      '#score_points' => 30,
    );


    $form['transportation_support'] = array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
    );
    $form['transportation_support'][] = array(
      '#type' => 'radios',
      '#title' => t('3. If the above-mentioned are not closely located, does the shelter have an internal vehicle for providing transportation support?'),
      '#default_value' => FALSE,
      '#options' => array(
        'yes' => t('Yes'),
        'no' => t('No'),
      ),
      '#score_points' => 20,
    );

    return $form;
  }
}
