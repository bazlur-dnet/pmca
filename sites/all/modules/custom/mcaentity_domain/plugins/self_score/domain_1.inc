<?php
/**
 * @file
 * Self Scoring plugin.
 */

$plugin = array(
  'domain id' => '1',
  'title' => t('Self Scoring Widget for Domain 1'),
  'handler' => 'SelfScoreWidgetDomain1',
);


class SelfScoreWidgetDomain1 extends mcaEntityDomainSelfScoreWidgetBase {
  public function buildForm(&$form, &$form_state) {
    parent::buildForm($form, $form_state);

    $form['processes_protocols'] = array(
      '#type' => 'fieldset',
      '#description' => t('1. Please outline the processes/protocols followed while preparing for, organizing and following rescues'),
      '#tree' => TRUE,
    );

    $form['processes_protocols']['preparation'] = array(
      '#type' => 'fieldset',
      '#title' => t('Preparation'),
    );
    $form['processes_protocols']['preparation'][] = array(
      '#type' => 'checkbox',
      '#title' => t('SP utlizes networks of informants who will provide specific information about trafficked person.'),
      '#score_points' => 2.0,
    );
    $form['processes_protocols']['preparation'][] = array(
      '#type' => 'checkbox',
      '#title' => t('Trained rescue team consists of the designated Police Officer, social/child workers, SP representative.'),
      '#score_points' => 2.0,
    );
    $form['processes_protocols']['preparation'][] = array(
      '#type' => 'checkbox',
      '#title' => t('The team will ensure that the facial and other identity of the rescued victim is not revealed to anyone except those who are legally competent to know the same.'),
      '#score_points' => 2.0,
    );
    $form['processes_protocols']['preparation'][] = array(
      '#type' => 'checkbox',
      '#title' => t('Processes are in place to prevent leakage of information, prior to the actual rescue operation and any other mode(s) of communication belonging to the rescue operation team members should be taken in custody by the rescue team leader.'),
      '#score_points' => 2.0,
    );
    $form['processes_protocols']['preparation'][] = array(
      '#type' => 'checkbox',
      '#title' => t('Before conducting rescue operations, all police formalities have been completed.'),
      '#score_points' => 2.0,
    );
    $form['processes_protocols']['preparation'][] = array(
      '#type' => 'checkbox',
      '#title' => t('SP has checked /verified vacancies available in Government and other certified shelters, so that the rescued victims can be taken to the appropriate locations for safe custody.'),
      '#score_points' => 2.0,
    );

    $form['processes_protocols']['during'] = array(
      '#type' => 'fieldset',
      '#title' => t('During'),
    );
    $form['processes_protocols']['during'][] = array(
      '#type' => 'checkbox',
      '#title' => t('During the rescue operations, rescue team members have formal protocols and processes for handling the victim with dignity, care and respect.'),
      '#score_points' => 2.0,
    );
    $form['processes_protocols']['during'][] = array(
      '#type' => 'checkbox',
      '#title' => t('While taking the victim to the government facilities/police station, he/she is taken in a separate vehicle from perpetrators and is accompanied at all times by a social worker/appropriate staff member.'),
      '#score_points' => 2.0,
    );

    $form['processes_protocols']['following'] = array(
      '#type' => 'fieldset',
      '#title' => t('Following'),
    );
    $form['processes_protocols']['following'][] = array(
      '#type' => 'checkbox',
      '#title' => t('Victim is removed from the rescue location as quickly as possible. He/She should collect all his/her belongings (so long as it is safe to do so). In case she has a child or children of her own, every effort is made to ensure non separation from children. Thereafter he/she is immediately produced to the competent legal authority.'),
      '#score_points' => 2.0,
    );

    $form['processes_protocols']['other'] = array(
      '#type' => 'fieldset',
      '#title' => t('Other'),
    );
    $form['processes_protocols']['other'][] = array(
      '#type' => 'checkbox',
      '#title' => t('Team members are trained in seizing/collecting all records showing expenses/income/payment/financial transactions and any other important document from the location, as they would form important material evidence in Court.'),
      '#score_points' => 2.0,
    );


    $form['informed_decision'] = array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
    );

    $form['informed_decision'][] = array(
      '#type' => 'radios',
      '#title' => t('2. Do you inform the processes from rescue to reintegration and facilitate the taking of an informed decision by the victim?'),
      '#default_value' => FALSE,
      '#options' => array(
        'yes' => t('Yes'),
        'no' => t('No'),
      ),
      '#score_points' => 20.0,
    );


    $form['collaboration_during'] = array(
      '#type' => 'fieldset',
      '#description' => t('3. Outline the key stakeholders that you need to collaborate with for conducting rescues.'),
      '#tree' => TRUE,
    );

    $form['collaboration_during']['stakeholders'] = array(
      '#type' => 'fieldset',
      '#title' => t('Stakeholders'),
    );
    $form['collaboration_during']['stakeholders'][] = array(
      '#type' => 'checkbox',
      '#title' => t('1. Police'),
      '#score_points' => 2.5,
    );
    $form['collaboration_during']['stakeholders'][] = array(
      '#type' => 'checkbox',
      '#title' => t('2. Ministry of Welfare (or equivalent)'),
      '#score_points' => 2.5,
    );
    $form['collaboration_during']['stakeholders'][] = array(
      '#type' => 'checkbox',
      '#title' => t('3. Other Government ministry'),
      '#score_points' => 1.25,
    );
    $form['collaboration_during']['stakeholders'][] = array(
      '#type' => 'checkbox',
      '#title' => t('4. International NGO'),
      '#score_points' => 1.25,
    );
    $form['collaboration_during']['stakeholders'][] = array(
      '#type' => 'checkbox',
      '#title' => t('5. Local NGO'),
      '#score_points' => 1.25,
    );
    $form['collaboration_during']['stakeholders'][] = array(
      '#type' => 'checkbox',
      '#title' => t('6. Other'),
      '#score_points' => 1.25,
    );

    $form['collaboration_during']['roles'] = array(
      '#type' => 'fieldset',
      '#title' => t('Roles'),
    );
    $form['collaboration_during']['roles'][] = array(
      '#type' => 'checkbox',
      '#title' => t('Conduct/cooperate on regular research/learning of areas having high probability of trafficking'),
      '#score_points' => 3.0,
    );
    $form['collaboration_during']['roles'][] = array(
      '#type' => 'checkbox',
      '#title' => t('Actively participate in rescue preparation/implementation'),
      '#score_points' => 3.0,
    );
    $form['collaboration_during']['roles'][] = array(
      '#type' => 'checkbox',
      '#title' => t('Post rescue coordination with NGOs for accommodation, transportation, coordination of legal aid, food and clothing and medical check-up.'),
      '#score_points' => 1.3,
    );
    $form['collaboration_during']['roles'][] = array(
      '#type' => 'checkbox',
      '#title' => t('Coordinate/provide security during searches/raids/rescues and afterwards'),
      '#score_points' => 1.5,
    );
    $form['collaboration_during']['roles'][] = array(
      '#type' => 'checkbox',
      '#title' => t('Information and communication only'),
      '#score_points' => 0.6,
    );
    $form['collaboration_during']['roles'][] = array(
      '#type' => 'checkbox',
      '#title' => t('No defined coordination role'),
      '#score_points' => 0.6,
    );


    $form['collaboration_after'] = array(
      '#type' => 'fieldset',
      '#description' => t('4. Outline the key stakeholders that you need to collaborate with immediately post rescue. Also enumerate the role of these stakeholders.'),
      '#tree' => TRUE,
    );

    $form['collaboration_after']['stakeholders'] = array(
      '#type' => 'fieldset',
      '#title' => t('Stakeholders'),
    );
    $form['collaboration_after']['stakeholders'][] = array(
      '#type' => 'checkbox',
      '#title' => t('1. Police'),
      '#score_points' => 3.5,
    );
    $form['collaboration_after']['stakeholders'][] = array(
      '#type' => 'checkbox',
      '#title' => t('2. Ministry of Welfare (or equivalent)'),
      '#score_points' => 2.5,
    );
    $form['collaboration_after']['stakeholders'][] = array(
      '#type' => 'checkbox',
      '#title' => t('3. Other Government ministry'),
      '#score_points' => 1.5,
    );
    $form['collaboration_after']['stakeholders'][] = array(
      '#type' => 'checkbox',
      '#title' => t('4. International NGO'),
      '#score_points' => 0.5,
    );
    $form['collaboration_after']['stakeholders'][] = array(
      '#type' => 'checkbox',
      '#title' => t('5. Local NGO'),
      '#score_points' => 0.5,
    );
    $form['collaboration_after']['stakeholders'][] = array(
      '#type' => 'checkbox',
      '#title' => t('6. Hospital/Doctor'),
      '#score_points' => 0.5,
    );
    $form['collaboration_after']['stakeholders'][] = array(
      '#type' => 'checkbox',
      '#title' => t('7. Doctor'),
      '#score_points' => 0.5,
    );
    $form['collaboration_after']['stakeholders'][] = array(
      '#type' => 'checkbox',
      '#title' => t('8. Other'),
      '#score_points' => 0.5,
    );

    $form['collaboration_after']['roles'] = array(
      '#type' => 'fieldset',
      '#title' => t('Roles'),
    );
    $form['collaboration_after']['roles'][] = array(
      '#type' => 'checkbox',
      '#title' => t('Coordinate for speedy repatriation of rescued survivors'),
      '#score_points' => 1.5,
    );
    $form['collaboration_after']['roles'][] = array(
      '#type' => 'checkbox',
      '#title' => t('Coordinate with stakeholders/implementors (e.g. Social Welfare and other Departments, NGOs, etc) for temporary shelter for the rescued survivors'),
      '#score_points' => 1.5,
    );
    $form['collaboration_after']['roles'][] = array(
      '#type' => 'checkbox',
      '#title' => t('Make arrangements for safe transportation of rescued children to home States/families'),
      '#score_points' => 1.5,
    );
    $form['collaboration_after']['roles'][] = array(
      '#type' => 'checkbox',
      '#title' => t('Support/implement safe restoration and rehabilitation of rescued child labour in their home places'),
      '#score_points' => 1.5,
    );
    $form['collaboration_after']['roles'][] = array(
      '#type' => 'checkbox',
      '#title' => t('Evidence collection and booking of middlemen and agents under appropriate criminal laws'),
      '#score_points' => 1.5,
    );
    $form['collaboration_after']['roles'][] = array(
      '#type' => 'checkbox',
      '#title' => t('Provide psychiatristric/medical care'),
      '#score_points' => 1.5,
    );
    $form['collaboration_after']['roles'][] = array(
      '#type' => 'checkbox',
      '#title' => t('Information and communication only'),
      '#score_points' => 1.0,
    );
    $form['collaboration_after']['roles'][] = array(
      '#type' => 'checkbox',
      '#title' => t('No defined coordination role'),
      '#score_points' => 0.0,
    );



    $form['ensured_while'] = array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
    );
    $form['ensured_while'][] = array(
      '#type' => 'radios',
      '#title' => t('5. Are safety and security ensured while conducting rescues?'),
      '#default_value' => FALSE,
      '#options' => array(
        'yes' => t('Yes'),
        'no' => t('No'),
      ),
      '#score_points' => 10,
    );


    $form['ensured_post'] = array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
    );
    $form['ensured_post'][] = array(
      '#type' => 'radios',
      '#title' => t('6. Are safety and security ensured in the post rescue stage?'),
      '#default_value' => FALSE,
      '#options' => array(
        'yes' => t('Yes'),
        'no' => t('No'),
      ),
      '#score_points' => 10,
    );
  }
}
