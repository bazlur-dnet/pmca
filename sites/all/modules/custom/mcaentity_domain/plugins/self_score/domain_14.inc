<?php
/**
 * @file
 * Self Scoring plugin.
 */

$plugin = array(
  'domain id' => '14',
  'title' => t('Self Scoring Widget for Domain 14'),
  'handler' => 'SelfScoreWidgetDomain14',
);


class SelfScoreWidgetDomain14 extends mcaEntityDomainSelfScoreWidgetBase {

  public function buildForm(&$form, &$form_state) {
    parent::buildForm($form, $form_state);

    $form['hand_over_documents_country'] = array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
    );
    $form['hand_over_documents_country'][] = array(
      '#type' => 'radios',
      '#title' => t('1. Do you hand over documents related to case management of survivors to the organisation responsible for the survivor in the other country?'),
      '#default_value' => FALSE,
      '#options' => array(
        'yes' => t('Yes'),
        'no' => t('No'),
      ),
      '#score_points' => 10,
    );


    $form['documents_post_repatriation'] = array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
    );
    $form['documents_post_repatriation'][] = array(
      '#type' => 'radios',
      '#title' => t('2. Similarly are you given documents/information related to case management of specific survivors by other partner organisations post repatriation?'),
      '#default_value' => FALSE,
      '#options' => array(
        'yes' => t('Yes'),
        'no' => t('No'),
      ),
      '#score_points' => 10,
    );


    $form['victim_opinion'] = array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
    );
    $form['victim_opinion'][] = array(
      '#type' => 'radios',
      '#title' => t('3. Do you take into account the opinion of the victim (whether they would like to go back) while repatriating them? '),
      '#default_value' => FALSE,
      '#options' => array(
        'yes' => t('Yes'),
        'no' => t('No'),
      ),
      '#score_points' => 20,
    );

    $form['child_special_cases'] = array(
      '#type' => 'fieldset',
      '#description' => t('4. What do you in those cases where child survivors refuse to go back/families cannot be traced/nationalities cannot be verified?'),
      '#tree' => TRUE,
    );
    $form['child_special_cases'][] = array(
      '#type' => 'checkbox',
      '#title' => t('Conduct additional family assessments where such details are available and inform the child of the results.'),
      '#score_points' => 20/7,
    );
    $form['child_special_cases'][] = array(
      '#type' => 'checkbox',
      '#title' => t('Locate alternative families (relatives) and implement assessments for placement.'),
      '#score_points' => 20/7,
    );
    $form['child_special_cases'][] = array(
      '#type' => 'checkbox',
      '#title' => t('Based on the assessment facilitate reunification only if the child wishes.'),
      '#score_points' => 20/7,
    );
    $form['child_special_cases'][] = array(
      '#type' => 'checkbox',
      '#title' => t('Provide interim care to the child (shelter, food, education).'),
      '#score_points' => 20/7,
    );
    $form['child_special_cases'][] = array(
      '#type' => 'checkbox',
      '#title' => t('Return proceedings to court.'),
      '#score_points' => 20/7,
    );
    $form['child_special_cases'][] = array(
      '#type' => 'checkbox',
      '#title' => t('Continue family tracing efforts.'),
      '#score_points' => 20/7,
    );
    $form['child_special_cases'][] = array(
      '#type' => 'checkbox',
      '#title' => t('Should the child refuse to return/family is not currently traceable, medium/longer term options such as long term shelter, foster care, adoption, guardianship,  alternatives to natal family reintegration are evaluated and discussed with child.'),
      '#score_points' => 20/7,
    );


    $form['adult_special_cases'] = array(
      '#type' => 'fieldset',
      '#description' => t('5. What do you do in those cases where adult survivors refuse to go back/families cannot be traced/nationalities cannot be verified?'),
      '#tree' => TRUE,
    );
    $form['adult_special_cases'][] = array(
      '#type' => 'checkbox',
      '#title' => t('Conduct additional family assessments where such details are available and inform the adult of the results.'),
      '#score_points' => 20/7,
    );
    $form['adult_special_cases'][] = array(
      '#type' => 'checkbox',
      '#title' => t('Locate alternative families (relatives) and implement assessments for placement.'),
      '#score_points' => 20/7,
    );
    $form['adult_special_cases'][] = array(
      '#type' => 'checkbox',
      '#title' => t('Based on the assessment facilitate reunification only if the adult wishes.'),
      '#score_points' => 20/7,
    );
    $form['adult_special_cases'][] = array(
      '#type' => 'checkbox',
      '#title' => t('Provide interim support (shelter/food/financial/training/employment/edication) to the adult.'),
      '#score_points' => 20/7,
    );
    $form['adult_special_cases'][] = array(
      '#type' => 'checkbox',
      '#title' => t('Return proceedings to court.'),
      '#score_points' => 20/7,
    );
    $form['adult_special_cases'][] = array(
      '#type' => 'checkbox',
      '#title' => t('Continue family tracing efforts.'),
      '#score_points' => 20/7,
    );
    $form['adult_special_cases'][] = array(
      '#type' => 'checkbox',
      '#title' => t('Should the adult refuse to return/family is not currently traceable, medium/longer term options such as group/individual counselling, long term shelter, transition care, referral to similar supportive options, transition to independent life support is evaluated with adult.'),
      '#score_points' => 20/7,
    );


    $form['security_during_repatriation'] = array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
    );
    $form['security_during_repatriation'][] = array(
      '#type' => 'radios',
      '#title' => t('6. Do you ensure safety and security of the survivors during repatriation?  '),
      '#default_value' => FALSE,
      '#options' => array(
        'yes' => t('Yes'),
        'no' => t('No'),
      ),
      '#score_points' => 10,
    );


    $form['measures_taken'] = array(
      '#type' => 'fieldset',
      '#description' => t('7. If yes, what measures are taken?'),
      '#tree' => TRUE,
    );
    $form['measures_taken'][] = array(
      '#type' => 'checkbox',
      '#title' => t('Initiate/facilitate cooperation between governmental authorities because the repatriation, receipt and reintegration of trafficked victims needs to be implemented by the cooperation of multiple sectors.'),
      '#score_points' => 10/3,
    );
    $form['measures_taken'][] = array(
      '#type' => 'checkbox',
      '#title' => t('Coordinate with grassroots level organizations in receiving and the reintegration of trafficked victims.'),
      '#score_points' => 10/3,
    );
    $form['measures_taken'][] = array(
      '#type' => 'checkbox',
      '#title' => t('Ensure the survivor is informed in a clear manner of the date for repatriation to his or her country of nationality or residence, and the means by which such repatriation will occur.'),
      '#score_points' => 10/3,
    );

    return $form;
  }
}
