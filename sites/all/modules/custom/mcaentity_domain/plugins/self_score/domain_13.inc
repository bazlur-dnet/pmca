<?php
/**
 * @file
 * Self Scoring plugin.
 */

$plugin = array(
  'domain id' => '13',
  'title' => t('Self Scoring Widget for Domain 13'),
  'handler' => 'SelfScoreWidgetDomain13',
);


class SelfScoreWidgetDomain13 extends mcaEntityDomainSelfScoreWidgetBase {

  public function buildForm(&$form, &$form_state) {
    parent::buildForm($form, $form_state);


    $form['confidentiality_privacy'] = array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
    );
    $form['confidentiality_privacy'][] = array(
      '#type' => 'radios',
      '#title' => t('1. Confidentiality and privacy'),
      '#default_value' => FALSE,
      '#options' => array(
        '0' => t('None'),
        '1' => t('Verbal'),
        '2' => t('Written policy(ies) in place but not visible to the children'),
        '3' => t('Written policy(ies) available and visible to the children'),
        '4' => t('Policy(ies) written in consultation with/by children but not visible to the children'),
        '5' => t('Policy(ies) written in consultation with/by the children and visible to the children'),
      ),
      '#score_points' => array(
        '0' => 0,
        '1' => 1.25,
        '2' => 2.25,
        '3' => 3.0,
        '4' => 4.5,
        '5' => 100/18,
      ),
    );


    $form['records_access'] = array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
    );
    $form['records_access'][] = array(
      '#type' => 'radios',
      '#title' => t('2. Access to case records and other personal records'),
      '#default_value' => FALSE,
      '#options' => array(
        '0' => t('None'),
        '1' => t('Verbal'),
        '2' => t('Written policy(ies) in place but not visible to the children'),
        '3' => t('Written policy(ies) available and visible to the children'),
        '4' => t('Policy(ies) written in consultation with/by children but not visible to the children'),
        '5' => t('Policy(ies) written in consultation with/by the children and visible to the children'),
      ),
      '#score_points' => array(
        '0' => 0,
        '1' => 1.25,
        '2' => 2.25,
        '3' => 3.0,
        '4' => 4.5,
        '5' => 100/18,
      ),
    );


    $form['info_release_authorisation'] = array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
    );
    $form['info_release_authorisation'][] = array(
      '#type' => 'radios',
      '#title' => t('3. No information is released without appropriate authorisation (e.g. from government/judicial authorities)'),
      '#default_value' => FALSE,
      '#options' => array(
        '0' => t('None'),
        '1' => t('Verbal'),
        '2' => t('Written policy(ies) in place but not visible to the children'),
        '3' => t('Written policy(ies) available and visible to the children'),
        '4' => t('Policy(ies) written in consultation with/by children but not visible to the children'),
        '5' => t('Policy(ies) written in consultation with/by the children and visible to the children'),
      ),
      '#score_points' => array(
        '0' => 0,
        '1' => 1.25,
        '2' => 2.25,
        '3' => 3.0,
        '4' => 4.5,
        '5' => 100/18,
      ),
    );


    $form['own_records'] = array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
    );
    $form['own_records'][] = array(
      '#type' => 'radios',
      '#title' => t('4. The right child survivors have to their records'),
      '#default_value' => FALSE,
      '#options' => array(
        '0' => t('None'),
        '1' => t('Verbal'),
        '2' => t('Written policy(ies) in place but not visible to the children'),
        '3' => t('Written policy(ies) available and visible to the children'),
        '4' => t('Policy(ies) written in consultation with/by children but not visible to the children'),
        '5' => t('Policy(ies) written in consultation with/by the children and visible to the children'),
      ),
      '#score_points' => array(
        '0' => 0,
        '1' => 1.25,
        '2' => 2.25,
        '3' => 3.0,
        '4' => 4.5,
        '5' => 100/18,
      ),
    );


    $form['police_records'] = array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
    );
    $form['police_records'][] = array(
      '#type' => 'radios',
      '#title' => t('5. The right police have to the records'),
      '#default_value' => FALSE,
      '#options' => array(
        '0' => t('None'),
        '1' => t('Verbal'),
        '2' => t('Written policy(ies) in place but not visible to the children'),
        '3' => t('Written policy(ies) available and visible to the children'),
        '4' => t('Policy(ies) written in consultation with/by children but not visible to the children'),
        '5' => t('Policy(ies) written in consultation with/by the children and visible to the children'),
      ),
      '#score_points' => array(
        '0' => 0,
        '1' => 1.25,
        '2' => 2.25,
        '3' => 3.0,
        '4' => 4.5,
        '5' => 100/18,
      ),
    );


    $form['doctors_records'] = array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
    );
    $form['doctors_records'][] = array(
      '#type' => 'radios',
      '#title' => t('6. The right doctors have to the records'),
      '#default_value' => FALSE,
      '#options' => array(
        '0' => t('None'),
        '1' => t('Verbal'),
        '2' => t('Written policy(ies) in place but not visible to the children'),
        '3' => t('Written policy(ies) available and visible to the children'),
        '4' => t('Policy(ies) written in consultation with/by children but not visible to the children'),
        '5' => t('Policy(ies) written in consultation with/by the children and visible to the children'),
      ),
      '#score_points' => array(
        '0' => 0,
        '1' => 1.25,
        '2' => 2.25,
        '3' => 3.0,
        '4' => 4.5,
        '5' => 100/18,
      ),
    );


    $form['staff_records'] = array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
    );
    $form['staff_records'][] = array(
      '#type' => 'radios',
      '#title' => t('7. The right selected staff have to the records'),
      '#default_value' => FALSE,
      '#options' => array(
        '0' => t('None'),
        '1' => t('Verbal'),
        '2' => t('Written policy(ies) in place but not visible to the children'),
        '3' => t('Written policy(ies) available and visible to the children'),
        '4' => t('Policy(ies) written in consultation with/by children but not visible to the children'),
        '5' => t('Policy(ies) written in consultation with/by the children and visible to the children'),
      ),
      '#score_points' => array(
        '0' => 0,
        '1' => 1.25,
        '2' => 2.25,
        '3' => 3.0,
        '4' => 4.5,
        '5' => 100/18,
      ),
    );


    $form['family_records'] = array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
    );
    $form['family_records'][] = array(
      '#type' => 'radios',
      '#title' => t('8. The right family members have to the records'),
      '#default_value' => FALSE,
      '#options' => array(
        '0' => t('None'),
        '1' => t('Verbal'),
        '2' => t('Written policy(ies) in place but not visible to the children'),
        '3' => t('Written policy(ies) available and visible to the children'),
        '4' => t('Policy(ies) written in consultation with/by children but not visible to the children'),
        '5' => t('Policy(ies) written in consultation with/by the children and visible to the children'),
      ),
      '#score_points' => array(
        '0' => 0,
        '1' => 1.25,
        '2' => 2.25,
        '3' => 3.0,
        '4' => 4.5,
        '5' => 100/18,
      ),
    );


    $form['outsider_observation'] = array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
    );
    $form['outsider_observation'][] = array(
      '#type' => 'radios',
      '#title' => t('9. Observation of survivors by outside persons, including interviews and photographs'),
      '#default_value' => FALSE,
      '#options' => array(
        '0' => t('None'),
        '1' => t('Verbal'),
        '2' => t('Written policy(ies) in place but not visible to the children'),
        '3' => t('Written policy(ies) available and visible to the children'),
        '4' => t('Policy(ies) written in consultation with/by children but not visible to the children'),
        '5' => t('Policy(ies) written in consultation with/by the children and visible to the children'),
      ),
      '#score_points' => array(
        '0' => 0,
        '1' => 1.25,
        '2' => 2.25,
        '3' => 3.0,
        '4' => 4.5,
        '5' => 100/18,
      ),
    );


    $form['case_history'] = array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
    );
    $form['case_history'][] = array(
      '#type' => 'radios',
      '#title' => t('10. Publication of survivor’s case history or photographs (e.g. in service provider publications such are reports, promotional materials or website?'),
      '#default_value' => FALSE,
      '#options' => array(
        '0' => t('None'),
        '1' => t('Verbal'),
        '2' => t('Written policy(ies) in place but not visible to the children'),
        '3' => t('Written policy(ies) available and visible to the children'),
        '4' => t('Policy(ies) written in consultation with/by children but not visible to the children'),
        '5' => t('Policy(ies) written in consultation with/by the children and visible to the children'),
      ),
      '#score_points' => array(
        '0' => 0,
        '1' => 1.25,
        '2' => 2.25,
        '3' => 3.0,
        '4' => 4.5,
        '5' => 100/18,
      ),
    );


    $form['use_survivors'] = array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
    );
    $form['use_survivors'][] = array(
      '#type' => 'radios',
      '#title' => t('11. Use of survivors in presentations, seminars and publicity events'),
      '#default_value' => FALSE,
      '#options' => array(
        '0' => t('None'),
        '1' => t('Verbal'),
        '2' => t('Written policy(ies) in place but not visible to the children'),
        '3' => t('Written policy(ies) available and visible to the children'),
        '4' => t('Policy(ies) written in consultation with/by children but not visible to the children'),
        '5' => t('Policy(ies) written in consultation with/by the children and visible to the children'),
      ),
      '#score_points' => array(
        '0' => 0,
        '1' => 1.25,
        '2' => 2.25,
        '3' => 3.0,
        '4' => 4.5,
        '5' => 100/18,
      ),
    );


    $form['access_survivor_belongings'] = array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
    );
    $form['access_survivor_belongings'][] = array(
      '#type' => 'radios',
      '#title' => t('12. Entry without permission into ‘private space’ and access to survivor’s personal belongings'),
      '#default_value' => FALSE,
      '#options' => array(
        '0' => t('None'),
        '1' => t('Verbal'),
        '2' => t('Written policy(ies) in place but not visible to the children'),
        '3' => t('Written policy(ies) available and visible to the children'),
        '4' => t('Policy(ies) written in consultation with/by children but not visible to the children'),
        '5' => t('Policy(ies) written in consultation with/by the children and visible to the children'),
      ),
      '#score_points' => array(
        '0' => 0,
        '1' => 1.25,
        '2' => 2.25,
        '3' => 3.0,
        '4' => 4.5,
        '5' => 100/18,
      ),
    );


    $form['privacy_personal_matters'] = array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
    );
    $form['privacy_personal_matters'][] = array(
      '#type' => 'radios',
      '#title' => t('13. Privacy for personal matters, including bathing and hygiene'),
      '#default_value' => FALSE,
      '#options' => array(
        '0' => t('None'),
        '1' => t('Verbal'),
        '2' => t('Written policy(ies) in place but not visible to the children'),
        '3' => t('Written policy(ies) available and visible to the children'),
        '4' => t('Policy(ies) written in consultation with/by children but not visible to the children'),
        '5' => t('Policy(ies) written in consultation with/by the children and visible to the children'),
      ),
      '#score_points' => array(
        '0' => 0,
        '1' => 1.25,
        '2' => 2.25,
        '3' => 3.0,
        '4' => 4.5,
        '5' => 100/18,
      ),
    );


    $form['privacy_interpersonal_matters'] = array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
    );
    $form['privacy_interpersonal_matters'][] = array(
      '#type' => 'radios',
      '#title' => t('14. Privacy for interpersonal matters, such as discussions with family and friends'),
      '#default_value' => FALSE,
      '#options' => array(
        '0' => t('None'),
        '1' => t('Verbal'),
        '2' => t('Written policy(ies) in place but not visible to the children'),
        '3' => t('Written policy(ies) available and visible to the children'),
        '4' => t('Policy(ies) written in consultation with/by children but not visible to the children'),
        '5' => t('Policy(ies) written in consultation with/by the children and visible to the children'),
      ),
      '#score_points' => array(
        '0' => 0,
        '1' => 1.25,
        '2' => 2.25,
        '3' => 3.0,
        '4' => 4.5,
        '5' => 100/18,
      ),
    );


    $form['disciplinary_measures'] = array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
    );
    $form['disciplinary_measures'][] = array(
      '#type' => 'radios',
      '#title' => t('15. Disciplinary measures for staff for breaches of confidentiality and privacy'),
      '#default_value' => FALSE,
      '#options' => array(
        '0' => t('None'),
        '1' => t('Verbal'),
        '2' => t('Written policy(ies) in place but not visible to the children'),
        '3' => t('Written policy(ies) available and visible to the children'),
        '4' => t('Policy(ies) written in consultation with/by children but not visible to the children'),
        '5' => t('Policy(ies) written in consultation with/by the children and visible to the children'),
      ),
      '#score_points' => array(
        '0' => 0,
        '1' => 1.25,
        '2' => 2.25,
        '3' => 3.0,
        '4' => 4.5,
        '5' => 100/18,
      ),
    );


    $form['code_conduct_rescue'] = array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
    );
    $form['code_conduct_rescue'][] = array(
      '#type' => 'radios',
      '#title' => t('16. Code of Conduct for Rescue'),
      '#default_value' => FALSE,
      '#options' => array(
        '0' => t('None'),
        '1' => t('Verbal'),
        '2' => t('Written policy(ies) in place but not visible to the children'),
        '3' => t('Written policy(ies) available and visible to the children'),
        '4' => t('Policy(ies) written in consultation with/by children but not visible to the children'),
        '5' => t('Policy(ies) written in consultation with/by the children and visible to the children'),
      ),
      '#score_points' => array(
        '0' => 0,
        '1' => 1.25,
        '2' => 2.25,
        '3' => 3.0,
        '4' => 4.5,
        '5' => 100/18,
      ),
    );


    $form['code_conduct_repatriation'] = array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
    );
    $form['code_conduct_repatriation'][] = array(
      '#type' => 'radios',
      '#title' => t('17. Code of Conduct for Repatriation'),
      '#default_value' => FALSE,
      '#options' => array(
        '0' => t('None'),
        '1' => t('Verbal'),
        '2' => t('Written policy(ies) in place but not visible to the children'),
        '3' => t('Written policy(ies) available and visible to the children'),
        '4' => t('Policy(ies) written in consultation with/by children but not visible to the children'),
        '5' => t('Policy(ies) written in consultation with/by the children and visible to the children'),
      ),
      '#score_points' => array(
        '0' => 0,
        '1' => 1.25,
        '2' => 2.25,
        '3' => 3.0,
        '4' => 4.5,
        '5' => 100/18,
      ),
    );


    $form['sexual_harassment_policy'] = array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
    );
    $form['sexual_harassment_policy'][] = array(
      '#type' => 'radios',
      '#title' => t('18. Sexual harassment policy'),
      '#default_value' => FALSE,
      '#options' => array(
        '0' => t('None'),
        '1' => t('Verbal'),
        '2' => t('Written policy(ies) in place but not visible to the children'),
        '3' => t('Written policy(ies) available and visible to the children'),
        '4' => t('Policy(ies) written in consultation with/by children but not visible to the children'),
        '5' => t('Policy(ies) written in consultation with/by the children and visible to the children'),
      ),
      '#score_points' => array(
        '0' => 0,
        '1' => 1.25,
        '2' => 2.25,
        '3' => 3.0,
        '4' => 4.5,
        '5' => 100/18,
      ),
    );


    return $form;
  }
}
