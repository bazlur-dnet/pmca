<?php
/**
 * @file
 * Self Scoring plugin.
 */

$plugin = array(
  'domain id' => '11',
  'title' => t('Self Scoring Widget for Domain 11'),
  'handler' => 'SelfScoreWidgetDomain11',
);


class SelfScoreWidgetDomain11 extends mcaEntityDomainSelfScoreWidgetBase {

  public function buildForm(&$form, &$form_state) {
    parent::buildForm($form, $form_state);

    $form['cater_training_needs'] = array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
    );
    $form['cater_training_needs'][] = array(
      '#type' => 'radios',
      '#title' => t('1. Do you have an internal department - or outsourcing facility - that caters to the training needs of staffs through on-job trainings, issue-based trainings and skill enhancement?'),
      '#default_value' => FALSE,
      '#options' => array(
        'yes' => t('Yes'),
        'no' => t('No'),
      ),
      '#score_points' => 10,
    );


    $form['received_training'] = array(
      '#type' => 'fieldset',
      '#description' => t('2. Have your relevant staff members received training in any of the following issues:'),
      '#tree' => TRUE,
    );
    $form['received_training'][] = array(
      '#type' => 'checkbox',
      '#title' => t('1 Supportive modes of interaction with survivors'),
      '#score_points' => 5,
    );
    $form['received_training'][] = array(
      '#type' => 'checkbox',
      '#title' => t('2 Issues and concerns in adolescent and child development'),
      '#score_points' => 5,
    );
    $form['received_training'][] = array(
      '#type' => 'checkbox',
      '#title' => t('3 Basic communication and listening skills'),
      '#score_points' => 5,
    );
    $form['received_training'][] = array(
      '#type' => 'checkbox',
      '#title' => t('4 Group dynamics'),
      '#score_points' => 5,
    );
    $form['received_training'][] = array(
      '#type' => 'checkbox',
      '#title' => t('5 Anger management and conflict resolution'),
      '#score_points' => 5,
    );
    $form['received_training'][] = array(
      '#type' => 'checkbox',
      '#title' => t('6 Gender and sexuality (focus on issues and concerns of children, adolescents and young adults) '),
      '#score_points' => 5,
    );
    $form['received_training'][] = array(
      '#type' => 'checkbox',
      '#title' => t('7 Detection of symptoms (‘warning signals’) of trauma, depression, self-injury or suicidal tendencies, etc.'),
      '#score_points' => 5,
    );
    $form['received_training'][] = array(
      '#type' => 'checkbox',
      '#title' => t('8 Rescue process for victims/survivors of trafficking followed by the organisation '),
      '#score_points' => 5,
    );
    $form['received_training'][] = array(
      '#type' => 'checkbox',
      '#title' => t('9 Child rights'),
      '#score_points' => 5,
    );
    $form['received_training'][] = array(
      '#type' => 'checkbox',
      '#title' => t('10 Child and/or Victim Protection'),
      '#score_points' => 5,
    );
    $form['received_training'][] = array(
      '#type' => 'checkbox',
      '#title' => t('11 Issues and concerns with regard to abuse, violence and exploitation of children'),
      '#score_points' => 5,
    );
    $form['received_training'][] = array(
      '#type' => 'checkbox',
      '#title' => t('12 Laws and legal procedures and protocols'),
      '#score_points' => 5,
    );
    $form['received_training'][] = array(
      '#type' => 'checkbox',
      '#title' => t('13 Case management (including confidentiality procedures)'),
      '#score_points' => 5,
    );
    $form['received_training'][] = array(
      '#type' => 'checkbox',
      '#title' => t('14 VCT counselling'),
      '#score_points' => 5,
    );
    $form['received_training'][] = array(
      '#type' => 'checkbox',
      '#title' => t('15 Family tracing?'),
      '#score_points' => 5,
    );
    $form['received_training'][] = array(
      '#type' => 'checkbox',
      '#title' => t('16 Reintegration support?'),
      '#score_points' => 5,
    );


    $form['provided_trainings'] = array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
    );
    $form['provided_trainings'][] = array(
      '#type' => 'radios',
      '#title' => t('3. If staff members do not meet the designated qualifications or are not trained as per the requirement of the job profile, are they provided with on-job trainings and necessary support to continue work in the organisation?'),
      '#default_value' => FALSE,
      '#options' => array(
        'yes' => t('Yes'),
        'no' => t('No'),
      ),
      '#score_points' => 10,
    );

    return $form;
  }
}
