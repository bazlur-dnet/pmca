<?php
/**
 * @file
 * Self Scoring plugin.
 */

$plugin = array(
  'domain id' => '15',
  'title' => t('Self Scoring Widget for Domain 15'),
  'handler' => 'SelfScoreWidgetDomain15',
);


class SelfScoreWidgetDomain15 extends mcaEntityDomainSelfScoreWidgetBase {

  public function buildForm(&$form, &$form_state) {
    parent::buildForm($form, $form_state);

    $form['family_tracing'] = array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
    );
    $form['family_tracing'][] = array(
      '#type' => 'radios',
      '#title' => t('1. Does your organisation undertake family tracing?'),
      '#default_value' => FALSE,
      '#options' => array(
        'yes' => t('Yes'),
        'no' => t('No'),
      ),
      '#score_points' => 10,
    );


    $form['family_tracing_steps'] = array(
      '#type' => 'fieldset',
      '#description' => t('2. If yes, please outline the steps that you undertake for family tracing:'),
      '#tree' => TRUE,
    );
    $form['family_tracing_steps'][] = array(
      '#type' => 'checkbox',
      '#title' => t('Collect exhaustive information regarding various aspect of the survivor’s life: from the social, economic and cultural context of the family of origin.'),
      '#score_points' => 5,
    );
    $form['family_tracing_steps'][] = array(
      '#type' => 'checkbox',
      '#title' => t('Implement family assessment prior to departure including , the family’s expectations concerning the reunification process if supported'),
      '#score_points' => 5,
    );
    $form['family_tracing_steps'][] = array(
      '#type' => 'checkbox',
      '#title' => t('Facilitate the issue of  travel documents and attestations to facilitate the reunification.'),
      '#score_points' => 5,
    );
    $form['family_tracing_steps'][] = array(
      '#type' => 'checkbox',
      '#title' => t('Provide constant attention to the child and value his or her own contribution to the process, especially considering that the definition of best interest changes.'),
      '#score_points' => 5,
    );


    $form['custom_tool'] = array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
    );
    $form['custom_tool'][] = array(
      '#type' => 'radios',
      '#title' => t('3. For the HIR/HIS, do you use a tool developed by your organization?'),
      '#default_value' => FALSE,
      '#options' => array(
        'yes' => t('Yes'),
        'no' => t('No'),
      ),
      '#score_points' => 10,
    );


    $form['family_reunification'] = array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
    );
    $form['family_reunification'][] = array(
      '#type' => 'radios',
      '#title' => t('4. Does your organisation undertake family reunification?'),
      '#default_value' => FALSE,
      '#options' => array(
        'yes' => t('Yes'),
        'no' => t('No'),
      ),
      '#score_points' => 10,
    );


    $form['family_reunification_steps'] = array(
      '#type' => 'fieldset',
      '#description' => t('5. Please outline the steps that you follow for family reunification:'),
      '#tree' => TRUE,
    );
    $form['family_reunification_steps'][] = array(
      '#type' => 'checkbox',
      '#title' => t('Clarify the condition of family members traced and conduct assessment for reunification so that no rescued victim is sent back to the family without ensuring social acceptance and family support to the victim.'),
      '#score_points' => 5,
    );
    $form['family_reunification_steps'][] = array(
      '#type' => 'checkbox',
      '#title' => t('Provide constant attention to the survivor and value his/her own contribution to the process, especially considering that the definition of best interest changes.'),
      '#score_points' => 5,
    );
    $form['family_reunification_steps'][] = array(
      '#type' => 'checkbox',
      '#title' => t('Implement evaluation of the survivor’s trafficking/protection needs.'),
      '#score_points' => 5,
    );
    $form['family_reunification_steps'][] = array(
      '#type' => 'checkbox',
      '#title' => t('Conduct outreach/ support activities, or oversee the delegation of support/reintegration activities to other organizations or individuals in accordance with the Reintegration Plan and in agreement with the victim.'),
      '#score_points' => 5,
    );

    $form['safety_security'] = array(
      '#type' => 'fieldset',
      '#description' => t('6. How do you address issues of safety and security of the survivors in the process of family tracing and reunification? '),
      '#tree' => TRUE,
    );
    $form['safety_security'][] = array(
      '#type' => 'checkbox',
      '#title' => t('Consider carefully the issue of safe, voluntary reunification/return with all stakeholders and family involved in cooperation with the survivor.'),
      '#score_points' => 7.5,
    );
    $form['safety_security'][] = array(
      '#type' => 'checkbox',
      '#title' => t('Consider carefully the safety of the returnee and prevention of punishment/discrimination upon arrival/return.'),
      '#score_points' => 7.5,
    );
    $form['safety_security'][] = array(
      '#type' => 'checkbox',
      '#title' => t('Evaluate/plan for reintegration services to be provided to survivor.'),
      '#score_points' => 7.5,
    );
    $form['safety_security'][] = array(
      '#type' => 'checkbox',
      '#title' => t('Upon discharge, each victim is provided with his/her educational records, medical records, legal documents, and other means such as savings and personal belongings.'),
      '#score_points' => 7.5,
    );

    return $form;
  }
}
