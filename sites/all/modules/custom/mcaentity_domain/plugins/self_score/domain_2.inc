<?php
/**
 * @file
 * Self Scoring plugin.
 */

$plugin = array(
  'domain id' => '2',
  'title' => t('Self Scoring Widget for Domain 2'),
  'handler' => 'SelfScoreWidgetDomain2',
);


class SelfScoreWidgetDomain2 extends mcaEntityDomainSelfScoreWidgetBase {

  public function buildForm(&$form, &$form_state) {
    parent::buildForm($form, $form_state);

    $form['services'] = array(
      '#type' => 'fieldset',
      '#description' => t('1. Services that your organization provides at the shelter (that you run or provide services in) include:'),
      '#tree' => TRUE,
    );
    $form['services'][] = array(
      '#type' => 'checkbox',
      '#title' => t('HIV related health services'),
      '#score_points' => 100 / 9,
    );
    $form['services'][] = array(
      '#type' => 'checkbox',
      '#title' => t('Counseling Services'),
      '#score_points' => 100 / 9,
    );
    $form['services'][] = array(
      '#type' => 'checkbox',
      '#title' => t('Food, clothing and basic introduction kit'),
      '#score_points' => 100 / 9,
    );
    $form['services'][] = array(
      '#type' => 'checkbox',
      '#title' => t('Non-formal Education'),
      '#score_points' => 100 / 9,
    );
    $form['services'][] = array(
      '#type' => 'checkbox',
      '#title' => t('Life skill Education'),
      '#score_points' => 100 / 9,
    );
    $form['services'][] = array(
      '#type' => 'checkbox',
      '#title' => t('Vocational Training'),
      '#score_points' => 100 / 9,
    );
    $form['services'][] = array(
      '#type' => 'checkbox',
      '#title' => t('Repatriation'),
      '#score_points' => 100 / 9,
    );
    $form['services'][] = array(
      '#type' => 'checkbox',
      '#title' => t('Legal aid and prosecution services'),
      '#score_points' => 100 / 9,
    );
    $form['services'][] = array(
      '#type' => 'checkbox',
      '#title' => t('Family unification'),
      '#score_points' => 100 / 9,
    );

    return $form;
  }
}
