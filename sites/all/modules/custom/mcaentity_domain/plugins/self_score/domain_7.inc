<?php
/**
 * @file
 * Self Scoring plugin.
 */

$plugin = array(
  'domain id' => '7',
  'title' => t('Self Scoring Widget for Domain 7'),
  'handler' => 'SelfScoreWidgetDomain7',
);


class SelfScoreWidgetDomain7 extends mcaEntityDomainSelfScoreWidgetBase {

  public function buildForm(&$form, &$form_state) {
    parent::buildForm($form, $form_state);

    $form['psychological_testing'] = array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
    );
    $form['psychological_testing'][] = array(
      '#type' => 'radios',
      '#title' => t('1. Is psychological testing/psychological profiling done for each of the survivors of cross-border trafficking/smuggling?'),
      '#default_value' => FALSE,
      '#options' => array(
        'yes' => t('Yes'),
        'no' => t('No'),
      ),
      '#score_points' => 20,
    );


    $form['sessions_documented'] = array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
    );
    $form['sessions_documented'][] = array(
      '#type' => 'radios',
      '#title' => t('2. Are records of counselling sessions documented in a systematic manner so that survivors do not have to recount negative experiences repetitively?'),
      '#default_value' => FALSE,
      '#options' => array(
        'yes' => t('Yes'),
        'no' => t('No'),
      ),
      '#score_points' => 20,
    );


    $form['counselling_private'] = array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
    );
    $form['counselling_private'][] = array(
      '#type' => 'radios',
      '#title' => t('3. Does the counselling take place in a private area?'),
      '#default_value' => FALSE,
      '#options' => array(
        'yes' => t('Yes'),
        'no' => t('No'),
      ),
      '#score_points' => 20,
    );


    $form['handle_differences'] = array(
      '#type' => 'fieldset',
      '#description' => t('4. How does the organization handle differences in culture, age and language among survivors?'),
      '#tree' => TRUE,
    );
    $form['handle_differences'][] = array(
      '#type' => 'checkbox',
      '#title' => t('SP has protocols and training in place for providing orientation to staff about culture/age/language of survivors.'),
      '#score_points' => 1.5,
    );
    $form['handle_differences'][] = array(
      '#type' => 'checkbox',
      '#title' => t('Psycho-social interventions are conducted in a private and confidential setting, and take into account the recipient’s language, culture, age, sex, ethnicity, class and religion.'),
      '#score_points' => 1.5,
    );
    $form['handle_differences'][] = array(
      '#type' => 'checkbox',
      '#title' => t('Staff are encouraged to identify important sources of cultural identity in survivors’ lives, to initially access the cultural backgrounds of individual victims or groups they serve.'),
      '#score_points' => 1.5,
    );
    $form['handle_differences'][] = array(
      '#type' => 'checkbox',
      '#title' => t('There are systems or opportunities in place for survivors to practice and share their culture.'),
      '#score_points' => 1.5,
    );
    $form['handle_differences'][] = array(
      '#type' => 'checkbox',
      '#title' => t('Translation facilities are available when required.'),
      '#score_points' => 1.5,
    );
    $form['handle_differences'][] = array(
      '#type' => 'checkbox',
      '#title' => t('Provisions are made for young children and older/immobile/disabled people to share their concerns/needs.'),
      '#score_points' => 1.5,
    );

    $form['specialised_psychological_services'] = array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
    );
    $form['specialised_psychological_services'][] = array(
      '#type' => 'radios',
      '#title' => t('5. In case of the need for specialised psychological services and therapies, do you approach and refer to the mainstream systems and structures?'),
      '#default_value' => FALSE,
      '#options' => array(
        'yes' => t('Yes'),
        'no' => t('No'),
      ),
      '#score_points' => 15,
    );

    $form['professional_staff'] = array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
    );
    $form['professional_staff'][] = array(
      '#type' => 'radios',
      '#title' => t('6. Do you have professional psychologist/trained counsellors in-house to cater to complex issues?'),
      '#default_value' => FALSE,
      '#options' => array(
        'yes' => t('Yes'),
        'no' => t('No'),
      ),
      '#score_points' => 15,
    );

    return $form;
  }
}
