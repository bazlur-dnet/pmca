<?php
/**
 * @file
 * Self Scoring plugin.
 */

$plugin = array(
  'domain id' => '16',
  'title' => t('Self Scoring Widget for Domain 16'),
  'handler' => 'SelfScoreWidgetDomain16',
);


class SelfScoreWidgetDomain16 extends mcaEntityDomainSelfScoreWidgetBase {

  public function buildForm(&$form, &$form_state) {
    parent::buildForm($form, $form_state);

    $form['icp'] = array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
    );
    $form['icp'][] = array(
      '#type' => 'radios',
      '#title' => t('1. Do you have Individual Case Plan for each survivor?'),
      '#default_value' => FALSE,
      '#options' => array(
        'yes' => t('Yes'),
        'no' => t('No'),
      ),
      '#score_points' => 20,
    );


    $form['icp_reintegration'] = array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
    );
    $form['icp_reintegration'][] = array(
      '#type' => 'radios',
      '#title' => t('2. Does this ICP include a plan for reintegration?'),
      '#default_value' => FALSE,
      '#options' => array(
        'yes' => t('Yes'),
        'no' => t('No'),
      ),
      '#score_points' => 10,
    );


    $form['icp_consulted'] = array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
    );
    $form['icp_consulted'][] = array(
      '#type' => 'radios',
      '#title' => t('3. Are the survivors consulted while preparing the ICP?'),
      '#default_value' => FALSE,
      '#options' => array(
        'yes' => t('Yes'),
        'no' => t('No'),
      ),
      '#score_points' => 15,
    );


    $form['reintegrating_steps'] = array(
      '#type' => 'fieldset',
      '#description' => t('4. Post-repatriation, what are the main steps in the process of reintegrating survivors with their families and communities?'),
      '#tree' => TRUE,
    );
    $form['reintegrating_steps'][] = array(
      '#type' => 'checkbox',
      '#title' => t('Conduct follow-up visits to monitor the situation of the reintegration process and continue for some time depending on how well the process is processing.'),
      '#score_points' => 25/7,
    );
    $form['reintegrating_steps'][] = array(
      '#type' => 'checkbox',
      '#title' => t('Ensure survivors have continued access to legal, medical and counselling services and their own relevant documents.'),
      '#score_points' => 25/7,
    );
    $form['reintegrating_steps'][] = array(
      '#type' => 'checkbox',
      '#title' => t('Special provisions are arranged to those who have contracted HIV/AIDS/medical concerns.'),
      '#score_points' => 25/7,
    );
    $form['reintegrating_steps'][] = array(
      '#type' => 'checkbox',
      '#title' => t('Provide survivor with names, address & telephone numbers of those who will be able to provide assistance if needed. Give information about health providers and help transfer any medical records (if needed). Link with others having similar experience if agreeable.'),
      '#score_points' => 25/7,
    );
    $form['reintegrating_steps'][] = array(
      '#type' => 'checkbox',
      '#title' => t('Ensure victims of trafficking have access both formal and non-formal education structures as relevant.'),
      '#score_points' => 25/7,
    );
    $form['reintegrating_steps'][] = array(
      '#type' => 'checkbox',
      '#title' => t('Involve families of victims and the community by enhancing their awareness about trafficking in general and the impact of trafficking on the individual.'),
      '#score_points' => 25/7,
    );
    $form['reintegrating_steps'][] = array(
      '#type' => 'checkbox',
      '#title' => t('Follow-up and feedback to agency and former host country as relevant.'),
      '#score_points' => 25/7,
    );


    $form['adult_alternatives'] = array(
      '#type' => 'fieldset',
      '#description' => t('5. In cases where the survivor is NOT a minor and does not want to return home, what other alternatives are provided?'),
      '#tree' => TRUE,
    );
    $form['adult_alternatives'][] = array(
      '#type' => 'checkbox',
      '#title' => t('Locate alternative families (relatives) and implement assessments for placement in agreement with the adult survivor.'),
      '#score_points' => 2,
    );
    $form['adult_alternatives'][] = array(
      '#type' => 'checkbox',
      '#title' => t('Based on the assessment facilitate reunification only if the adult wishes and the evaluation supports this.'),
      '#score_points' => 2,
    );
    $form['adult_alternatives'][] = array(
      '#type' => 'checkbox',
      '#title' => t('Provide interim support (shelter/food/financial/training/employment/education) to the adult.'),
      '#score_points' => 2,
    );
    $form['adult_alternatives'][] = array(
      '#type' => 'checkbox',
      '#title' => t('Return proceedings to court as necessary/required especially in cross border situations.'),
      '#score_points' => 2,
    );
    $form['adult_alternatives'][] = array(
      '#type' => 'checkbox',
      '#title' => t('Arrange for medium/longer term options such as group/individual counselling, long term shelter, transition care, referral to similar supportive options, transition to independent life support is evaluated with adult.'),
      '#score_points' => 2,
    );


    $form['minor_alternatives'] = array(
      '#type' => 'fieldset',
      '#description' => t('6. If the survivor IS a minor and does not want to return to the family, what alternatives are provided?'),
      '#tree' => TRUE,
    );
    $form['minor_alternatives'][] = array(
      '#type' => 'checkbox',
      '#title' => t('Locate alternative families (relatives) and implement assessments for placement.'),
      '#score_points' => 4,
    );
    $form['minor_alternatives'][] = array(
      '#type' => 'checkbox',
      '#title' => t('Based on the assessment facilitate reunification only if the child wishes and the decision is supported by the assessment team.'),
      '#score_points' => 4,
    );
    $form['minor_alternatives'][] = array(
      '#type' => 'checkbox',
      '#title' => t('Provide interim care to the child (shelter, food, education).'),
      '#score_points' => 4,
    );
    $form['minor_alternatives'][] = array(
      '#type' => 'checkbox',
      '#title' => t('Return proceedings to court.'),
      '#score_points' => 4,
    );
    $form['minor_alternatives'][] = array(
      '#type' => 'checkbox',
      '#title' => t('Offer medium/longer term options such as long term shelter, foster care, adoption, guardianship, alternatives to natal family reintegration which are evaluated and discussed with child.'),
      '#score_points' => 4,
    );

    return $form;
  }
}
