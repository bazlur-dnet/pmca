<?php
/**
 * @file
 * Self Scoring plugin.
 */

$plugin = array(
  'domain id' => '17',
  'title' => t('Self Scoring Widget for Domain 17'),
  'handler' => 'SelfScoreWidgetDomain17',
);


class SelfScoreWidgetDomain17 extends mcaEntityDomainSelfScoreWidgetBase {

  public function buildForm(&$form, &$form_state) {
    parent::buildForm($form, $form_state);

    $form['legal_aid_direct'] = array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
    );
    $form['legal_aid_direct'][] = array(
      '#type' => 'radios',
      '#title' => t('1. Do you provide legal aid directly to survivors/their families?'),
      '#default_value' => FALSE,
      '#options' => array(
        'yes' => t('Yes'),
        'no' => t('No'),
      ),
      '#score_points' => 50,
    );


    $form['legal_aid_indirect'] = array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
    );
    $form['legal_aid_indirect'][] = array(
      '#type' => 'radios',
      '#title' => t('2. Do you provide legal aid via referrals to survivors and/or their families?'),
      '#default_value' => FALSE,
      '#options' => array(
        'yes' => t('Yes'),
        'no' => t('No'),
      ),
      '#score_points' => 25,
    );


    $form['organisation_role'] = array(
      '#type' => 'fieldset',
      '#description' => t('3. Outline the role your organisation plays in the legal processes involved from rescue to repatriation and post repatriation to reintegration:'),
      '#tree' => TRUE,
    );
    $form['organisation_role'][] = array(
      '#type' => 'checkbox',
      '#title' => t('Advocacy for harmonizing legal definitions, procedures and cooperation at the national and regional levels in accordance with international standards.'),
      '#score_points' => 3.75,
    );
    $form['organisation_role'][] = array(
      '#type' => 'checkbox',
      '#title' => t('Direct involvement in investigations, criminal proceedings and prosecution.'),
      '#score_points' => 3.75,
    );
    $form['organisation_role'][] = array(
      '#type' => 'checkbox',
      '#title' => t('Directly facilitating the right of trafficking victims to pursue civil claims against alleged traffickers is enshrined in law.'),
      '#score_points' => 3.75,
    );
    $form['organisation_role'][] = array(
      '#type' => 'checkbox',
      '#title' => t('Guaranteeing protection for witnesses and support to victims in law.'),
      '#score_points' => 3.75,
    );
    $form['organisation_role'][] = array(
      '#type' => 'checkbox',
      '#title' => t('Making appropriate efforts to protect individual trafficked victims and other witnesses (including their families) during the investigation and trial process and any subsequent period when their safety so requires.  Appropriate protection programmes may include some or all of the following elements: access to independent legal counsel; protection of identity during legal proceedings; in-camera trials.'),
      '#score_points' => 3.75,
    );
    $form['organisation_role'][] = array(
      '#type' => 'checkbox',
      '#title' => t('Facilitation of compensation/restitution.'),
      '#score_points' => 3.75,
    );
    $form['organisation_role'][] = array(
      '#type' => 'checkbox',
      '#title' => t('Information/communication only'),
      '#score_points' => 2.5,
    );

    return $form;
  }
}
