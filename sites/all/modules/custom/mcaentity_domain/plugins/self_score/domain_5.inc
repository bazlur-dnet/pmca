<?php
/**
 * @file
 * Self Scoring plugin.
 */

$plugin = array(
  'domain id' => '5',
  'title' => t('Self Scoring Widget for Domain 5'),
  'handler' => 'SelfScoreWidgetDomain5',
);


class SelfScoreWidgetDomain5 extends mcaEntityDomainSelfScoreWidgetBase {

  public function buildForm(&$form, &$form_state) {
    parent::buildForm($form, $form_state);

    $form['child_protection_policy'] = array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
    );
    $form['child_protection_policy'][] = array(
      '#type' => 'radios',
      '#title' => t('1. Does the facility have a child protection policy?'),
      '#default_value' => FALSE,
      '#options' => array(
        'yes' => t('Yes'),
        'no' => t('No'),
      ),
      '#score_points' => 10,
    );

    $form['meet_visitors'] = array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
    );
    $form['meet_visitors'][] = array(
      '#type' => 'radios',
      '#title' => t('2. Can survivors choose not to meet visitors?'),
      '#default_value' => FALSE,
      '#options' => array(
        'yes' => t('Yes'),
        'no' => t('No'),
      ),
      '#score_points' => 10,
    );

    $form['visitor_rules'] = array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
    );
    $form['visitor_rules'][] = array(
      '#type' => 'radios',
      '#title' => t('3. Does the shelter have Dos and Don’ts for visitors?'),
      '#default_value' => FALSE,
      '#options' => array(
        'yes' => t('Yes'),
        'no' => t('No'),
      ),
      '#score_points' => 5,
    );

    $form['visitor_rules_visible'] = array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
    );
    $form['visitor_rules_visible'][] = array(
      '#type' => 'radios',
      '#title' => t('4. Is it visible to the children?'),
      '#default_value' => FALSE,
      '#options' => array(
        'yes' => t('Yes'),
        'no' => t('No'),
      ),
      '#score_points' => 5,
    );

    $form['staff_rules'] = array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
    );
    $form['staff_rules'][] = array(
      '#type' => 'radios',
      '#title' => t('5. Does the shelter have Dos and Don’ts for the staff?'),
      '#default_value' => FALSE,
      '#options' => array(
        'yes' => t('Yes'),
        'no' => t('No'),
      ),
      '#score_points' => 10,
    );

    $form['staff_rules_visible'] = array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
    );
    $form['staff_rules_visible'][] = array(
      '#type' => 'radios',
      '#title' => t('6. Is it visible to the children?'),
      '#default_value' => FALSE,
      '#options' => array(
        'yes' => t('Yes'),
        'no' => t('No'),
      ),
      '#score_points' => 5,
    );

    $form['mandatory_programmes'] = array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
    );
    $form['mandatory_programmes'][] = array(
      '#type' => 'radios',
      '#title' => t('7. Can survivors choose not to participate in programmes?'),
      '#default_value' => FALSE,
      '#options' => array(
        'yes' => t('Yes'),
        'no' => t('No'),
      ),
      '#score_points' => 5,
    );

    $form['child_movement_restrictions'] = array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
    );
    $form['child_movement_restrictions'][] = array(
      '#type' => 'radios',
      '#title' => t('8. Are there restrictions on the movement of child survivors within and from the facility?'),
      '#default_value' => FALSE,
      '#options' => array(
        'yes' => t('Yes'),
        'no' => t('No'),
      ),
      '#score_points' => 10,
    );

    $form['engagement_policy'] = array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
    );
    $form['engagement_policy'][] = array(
      '#type' => 'radios',
      '#title' => t('9. Is there a policy explaining the conditions for engagement between the survivors while in shelters and their parents/family?'),
      '#default_value' => FALSE,
      '#options' => array(
        'yes' => t('Yes'),
        'no' => t('No'),
      ),
      '#score_points' => 5,
    );

    $form['engagement_policy_awareness'] = array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
    );
    $form['engagement_policy_awareness'][] = array(
      '#type' => 'radios',
      '#title' => t('10. Are survivors aware of this policy?'),
      '#default_value' => FALSE,
      '#options' => array(
        'yes' => t('Yes'),
        'no' => t('No'),
      ),
      '#score_points' => 10,
    );

    $form['education_offered'] = array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
    );
    $form['education_offered'][] = array(
      '#type' => 'radios',
      '#title' => t('11. Is education offered to child survivors regarding how to keep themselves safe when in the community?'),
      '#default_value' => FALSE,
      '#options' => array(
        'yes' => t('Yes'),
        'no' => t('No'),
      ),
      '#score_points' => 10,
    );

    $form['sexual_harassment_policy'] = array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
    );
    $form['sexual_harassment_policy'][] = array(
      '#type' => 'radios',
      '#title' => t('12. Is there a sexual harassment policy?'),
      '#default_value' => FALSE,
      '#options' => array(
        'yes' => t('Yes'),
        'no' => t('No'),
      ),
      '#score_points' => 10,
    );

    $form['sexual_harassment_committee'] = array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
    );
    $form['sexual_harassment_committee'][] = array(
      '#type' => 'radios',
      '#title' => t('13. Does the shelter have a sexual harassment committee?'),
      '#default_value' => FALSE,
      '#options' => array(
        'yes' => t('Yes'),
        'no' => t('No'),
      ),
      '#score_points' => 5,
    );

    return $form;
  }
}
