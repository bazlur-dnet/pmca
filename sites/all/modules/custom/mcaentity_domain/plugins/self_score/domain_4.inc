<?php
/**
 * @file
 * Self Scoring plugin.
 */

$plugin = array(
  'domain id' => '4',
  'title' => t('Self Scoring Widget for Domain 4'),
  'handler' => 'SelfScoreWidgetDomain4',
);


class SelfScoreWidgetDomain4 extends mcaEntityDomainSelfScoreWidgetBase {

  public function buildForm(&$form, &$form_state) {
    parent::buildForm($form, $form_state);

    $form['residents_policy'] = array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
    );
    $form['residents_policy'][] = array(
      '#type' => 'radios',
      '#title' => t('1. Does the shelter have a policy for participation of its residents?'),
      '#default_value' => FALSE,
      '#options' => array(
        'yes' => t('Yes'),
        'no' => t('No'),
      ),
      '#score_points' => 15,
    );


    $form['separate_policy'] = array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
    );
    $form['separate_policy'][] = array(
      '#type' => 'radios',
      '#title' => t('2. If yes, is there a separate policy for adults and children?'),
      '#default_value' => FALSE,
      '#options' => array(
        'yes' => t('Yes'),
        'no' => t('No'),
      ),
      '#score_points' => 15,
    );


    $form['children_say'] = array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
    );
    $form['children_say'][] = array(
      '#type' => 'radios',
      '#title' => t('3. Do children have a say in the day-to-day running of the shelter?'),
      '#default_value' => FALSE,
      '#options' => array(
        'yes' => t('Yes'),
        'no' => t('No'),
      ),
      '#score_points' => 15,
    );


    $form['children_meetings'] = array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
    );
    $form['children_meetings'][] = array(
      '#type' => 'radios',
      '#title' => t('4. Do children attend or are represented at the shelter meetings?'),
      '#default_value' => FALSE,
      '#options' => array(
        'yes' => t('Yes'),
        'no' => t('No'),
      ),
      '#score_points' => 15,
    );


    $form['children_participate'] = array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
    );
    $form['children_participate'][] = array(
      '#type' => 'radios',
      '#title' => t('5. Do children actively participate in their own case management procedures? E.g. through attending meetings, regarding their own cases and their input on decisions being solicited.'),
      '#default_value' => FALSE,
      '#options' => array(
        'yes' => t('Yes'),
        'no' => t('No'),
      ),
      '#score_points' => 15,
    );


    $form['discipline'] = array(
      '#type' => 'fieldset',
      '#description' => t('6. How do you enforce discipline among children in the shelter?'),
      '#tree' => TRUE,
    );
    $form['discipline'][] = array(
      '#type' => 'checkbox',
      '#title' => t('There is a clear written policy, procedures and guidelines for staff, comprising of a code of conduct setting out the permissible control, disciplinary and restraint measures.'),
      '#score_points' => 3.0,
    );
    $form['discipline'][] = array(
      '#type' => 'checkbox',
      '#title' => t('Peer group discussions are implemented to raise relevant issues; bullying, abuse, fighting, sexual exploitation'),
      '#score_points' => 3.0,
    );
    $form['discipline'][] = array(
      '#type' => 'checkbox',
      '#title' => t('Ensure that the members of staff respond positively to acceptable behavior, and that where the behavior of the victim residents is regarded as unacceptable by staff, it is responded to by constructive, acceptable and known disciplinary measures approved by the competent authority.'),
      '#score_points' => 3.0,
    );
    $form['discipline'][] = array(
      '#type' => 'checkbox',
      '#title' => t('Any measures taken to respond to unacceptable behavior should be appropriate to the age, understanding and individual needs of the victim.'),
      '#score_points' => 3.0,
    );
    $form['discipline'][] = array(
      '#type' => 'checkbox',
      '#title' => t('The staff and the victims and their minor dependents (living in the shelter) shall be made aware that each individual has rights and responsibilities in relation to those who live in the home, those who work there, as well as the open community.'),
      '#score_points' => 3.0,
    );


    $form['feedback_mechanisms'] = array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
    );
    $form['feedback_mechanisms'][] = array(
      '#type' => 'radios',
      '#title' => t('7. Do you have mechanisms for survivors to provide feedback on services, quality and appropriateness of programs and quality and helpfulness of staff in the shelter home?'),
      '#default_value' => FALSE,
      '#options' => array(
        'yes' => t('Yes'),
        'no' => t('No'),
      ),
      '#score_points' => 10,
    );

    return $form;
  }
}
