<?php
/**
 * @file
 * Self Scoring plugin.
 */

$plugin = array(
  'domain id' => '9',
  'title' => t('Self Scoring Widget for Domain 9'),
  'handler' => 'SelfScoreWidgetDomain9',
);


class SelfScoreWidgetDomain9 extends mcaEntityDomainSelfScoreWidgetBase {

  public function buildForm(&$form, &$form_state) {
    parent::buildForm($form, $form_state);

    $form['education_provided'] = array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
    );
    $form['education_provided'][] = array(
      '#type' => 'radios',
      '#title' => t('1. Is formal/non-formal education provided to survivors in the shelter home?'),
      '#default_value' => FALSE,
      '#options' => array(
        'yes' => t('Yes'),
        'no' => t('No'),
      ),
      '#score_points' => 100/3,
    );


    $form['training_classes'] = array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
    );
    $form['training_classes'][] = array(
      '#type' => 'radios',
      '#title' => t('2. Are there vocational training classes conducted for the survivors?'),
      '#default_value' => FALSE,
      '#options' => array(
        'yes' => t('Yes'),
        'no' => t('No'),
      ),
      '#score_points' => 100/3,
    );


    $form['progress_timeline'] = array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
    );
    $form['progress_timeline'][] = array(
      '#type' => 'radios',
      '#title' => t('3. Post-vocational training and reintegration with family, are survivors provided with apprenticeship/employment opportunities, in collaboration with partner organisations or directly?'),
      '#default_value' => FALSE,
      '#options' => array(
        'yes' => t('Yes'),
        'no' => t('No'),
      ),
      '#score_points' => 100/3,
    );

    return $form;
  }
}
