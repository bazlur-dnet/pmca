<?php
/**
 * @file
 * Self Scoring plugin.
 */

$plugin = array(
  'domain id' => '8',
  'title' => t('Self Scoring Widget for Domain 8'),
  'handler' => 'SelfScoreWidgetDomain8',
);


class SelfScoreWidgetDomain8 extends mcaEntityDomainSelfScoreWidgetBase {

  public function buildForm(&$form, &$form_state) {
    parent::buildForm($form, $form_state);

    $form['right_to_refuse'] = array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
    );
    $form['right_to_refuse'][] = array(
      '#type' => 'radios',
      '#title' => t('1. Do the child survivors have the right to refuse services at any time?'),
      '#default_value' => FALSE,
      '#options' => array(
        'yes' => t('Yes'),
        'no' => t('No'),
      ),
      '#score_points' => 20,
    );


    $form['identity_confidential'] = array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
    );
    $form['identity_confidential'][] = array(
      '#type' => 'radios',
      '#title' => t('2. Do you ensure that the identity of each survivor is kept confidential at every step of case management?'),
      '#default_value' => FALSE,
      '#options' => array(
        'yes' => t('Yes'),
        'no' => t('No'),
      ),
      '#score_points' => 20,
    );


    $form['progress_timeline'] = array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
    );
    $form['progress_timeline'][] = array(
      '#type' => 'radios',
      '#title' => t('3. Is there a specific timeline for progress from emergency stage to stabilisation stage to exit planning stage, with measurable baselines and goals?'),
      '#default_value' => FALSE,
      '#options' => array(
        'yes' => t('Yes'),
        'no' => t('No'),
      ),
      '#score_points' => 20,
    );


    $form['individual_case_plans'] = array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
    );
    $form['individual_case_plans'][] = array(
      '#type' => 'radios',
      '#title' => t('4. Are individual case plans with regard to livelihood and financial needs of the survivor prepared?'),
      '#default_value' => FALSE,
      '#options' => array(
        'yes' => t('Yes'),
        'no' => t('No'),
      ),
      '#score_points' => 20,
    );


    $form['legal_assistance'] = array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
    );
    $form['legal_assistance'][] = array(
      '#type' => 'radios',
      '#title' => t('5. Is legal assistance with obtaining relevant documentation and remedies, including compensation, through criminal, civil and administrative channels available for the survivors at different levels of case management?'),
      '#default_value' => FALSE,
      '#options' => array(
        'yes' => t('Yes'),
        'no' => t('No'),
      ),
      '#score_points' => 20,
    );

    return $form;
  }
}
