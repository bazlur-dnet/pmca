<?php
/**
 * @file
 * Self Scoring plugin.
 */

$plugin = array(
  'domain id' => '6',
  'title' => t('Self Scoring Widget for Domain 6'),
  'handler' => 'SelfScoreWidgetDomain6',
);


class SelfScoreWidgetDomain6 extends mcaEntityDomainSelfScoreWidgetBase {

  public function buildForm(&$form, &$form_state) {
    parent::buildForm($form, $form_state);

    $form['medical_tests'] = array(
      '#type' => 'fieldset',
      '#description' => t('1. What are the medical tests done for the survivors at your shelter?'),
      '#tree' => TRUE,
    );
    $form['medical_tests'][] = array(
      '#type' => 'checkbox',
      '#title' => t('General health check'),
      '#score_points' => 6.0,
    );
    $form['medical_tests'][] = array(
      '#type' => 'checkbox',
      '#title' => t('STD, VDRL test'),
      '#score_points' => 6.0,
    );
    $form['medical_tests'][] = array(
      '#type' => 'checkbox',
      '#title' => t('TB'),
      '#score_points' => 6.0,
    );
    $form['medical_tests'][] = array(
      '#type' => 'checkbox',
      '#title' => t('Diabetes'),
      '#score_points' => 6.0,
    );
    $form['medical_tests'][] = array(
      '#type' => 'checkbox',
      '#title' => t('Elisa Test (HIV)'),
      '#score_points' => 6.0,
    );


    $form['complete_health_assessment'] = array(
      '#type' => 'fieldset',
      '#description' => t('2. No complete health assessment is administered after admission into facility.'),
      '#tree' => TRUE,
    );
    $form['complete_health_assessment'][] = array(
      '#type' => 'checkbox',
      '#title' => t('Confirm'),
      '#default_value' => FALSE,
      '#score_points' => 0,
    );

    $form['hiv_support_systems'] = array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
    );
    $form['hiv_support_systems'][] = array(
      '#type' => 'radios',
      '#title' => t('3. In cases when the survivors are detected with HIV, do you link them with mainstream care and support systems?'),
      '#default_value' => FALSE,
      '#options' => array(
        'yes' => t('Yes'),
        'no' => t('No'),
      ),
      '#score_points' => 10,
    );


    $form['regular_health_checks'] = array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
    );
    $form['regular_health_checks'][] = array(
      '#type' => 'radios',
      '#title' => t('4. Are regular health checks by registered medical practitioners conducted for the survivors?'),
      '#default_value' => FALSE,
      '#options' => array(
        'yes' => t('Yes'),
        'no' => t('No'),
      ),
      '#score_points' => 20,
    );


    $form['sufficient_stock'] = array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
    );
    $form['sufficient_stock'][] = array(
      '#type' => 'radios',
      '#title' => t('5. Does the shelter have sufficient stock of basic and emergency medicines and first aid equipment?'),
      '#default_value' => FALSE,
      '#options' => array(
        'yes' => t('Yes'),
        'no' => t('No'),
      ),
      '#score_points' => 20,
    );

    $form['trained_staff'] = array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
    );
    $form['trained_staff'][] = array(
      '#type' => 'radios',
      '#title' => t('6. Does the shelter home have trained staff member(s) for handling first aid and emergencies?'),
      '#default_value' => FALSE,
      '#options' => array(
        'yes' => t('Yes'),
        'no' => t('No'),
      ),
      '#score_points' => 20,
    );

    return $form;
  }
}
