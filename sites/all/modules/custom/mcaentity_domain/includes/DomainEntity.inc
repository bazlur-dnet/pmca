<?php
/**
 * @file
 * Class extending the basic Entity class.
 */

/**
 * Main class for Domain entities.
 */
class DomainEntity extends Entity {
  // Define some default values.
  public $did = NULL;
  public $name = '';
  public $language = LANGUAGE_NONE;
  public $type = '';
  public $uid = NULL;
  public $status = TRUE;
  public $created = NULL;
  public $changed = NULL;
  public $icon = '';
  public $number = '';

  /**
   * URI method for entity_class_uri().
   */
  protected function defaultUri() {
    if (!isset($this->did)) {
      return NULL;
    }

    return array(
      'path' => MCAENTITY_DOMAIN_UI_PATH . '/' . $this->identifier(),
    );
  }
}
