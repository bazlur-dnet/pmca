<?php

/**
 */
class DomainMetadataController extends EntityDefaultMetadataController {
  public function entityPropertyInfo() {
    $info = parent::entityPropertyInfo();

    // Group can be either a bundle, or the "properties" keyword.
    foreach ($info[$this->type] as $group => &$group_info) {
      $group_info['name']['label'] = 'Name';

      if (isset($group_info['status'])) {
        $group_info['status']['type'] = 'boolean';
      }
      if (isset($group_info['uid'])) {
        $group_info['uid']['label'] = 'User ID';
      }
    }

    return $info;
  }
}
