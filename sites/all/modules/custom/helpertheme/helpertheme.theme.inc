<?php

/**
 * Returns HTML for the footer items.
 */
function theme_footer_items() {
  global $theme;

  $elements = array(
    '#type' => 'container',
    '#attributes' => array(
      'class' => array(
        'footer-items',
      ),
    ),
  );

  $elements['plan_logo_wrapper'] = array(
    '#type' => 'container',
    '#attributes' => array(
      'class' => array(
        'footer-plan-logo-wrapper',
      ),
    ),
  );
  $elements['plan_logo_wrapper']['logo'] = array(
    '#theme' => 'image',
    '#path' => url(drupal_get_path('theme', $theme) . '/images/footer-plan-logo.png'),
    '#alt' => t('An initiative by Plan International'),
  );

  $site_name = filter_xss_admin(variable_get('site_name', 'Drupal'));
  $elements['logo_wrapper'] = array(
    '#type' => 'container',
    '#attributes' => array(
      'class' => array(
        'footer-logo-wrapper',
      ),
    ),
  );
  $elements['logo_wrapper']['logo'] = array(
    '#theme' => 'image',
    '#path' => url(drupal_get_path('theme', $theme) . '/logo.png'),
    '#alt' => $site_name,
  );

  // Add the footer copyright text.
  $copyright_text = filter_xss_admin(variable_get('helpertheme_footer_copyright_text', ''));
  $elements['footer_copyright'] = array(
    '#prefix' => '<span class="footer-copyright">',
    '#markup' => token_replace(t($copyright_text)),
    '#suffix' => '</span> ',
  );

  // Get the menu links.
  $menu_name = 'menu-footer';
  if ($links = helpertheme_menu_navigation_links($menu_name)) {
    $elements['footer_menu'] = array(
      '#type' => 'container',
      '#attributes' => array(
        'class' => array(
          'contextual-links-region',
          'footer-links-wrapper',
        ),
      ),
    );

    // Initialize the template variable as a render-able array.
    $elements['footer_menu']['contextual_links'] = array(
      '#type' => 'contextual_links',
      '#contextual_links' => array(
        'menu' => array('admin/structure/menu/manage', array('main-menu')),
      ),
    );

    $elements['footer_menu']['menu'] = array(
      '#theme' => 'links__enhanced',
      '#links' => $links,
      '#attributes' => array(
        'class' => array(
          'footer-links',
        ),
      ),
    );
  }

  // Render the HTML.
  return drupal_render($elements);
}

/**
 * Returns HTML for the header items.
 */
function theme_header_items() {
  $elements = array(
    '#type' => 'container',
    '#attributes' => array(
      'class' => array(
        'header-items',
      ),
    ),
  );

  // Get the menu links.
  $menu_name = 'user-menu';
  if ($links = helpertheme_menu_navigation_links($menu_name, 1)) {
    $elements['header'] = array(
      '#type' => 'html_tag',
      '#tag' => 'h2',
      '#value' => 'User Menu',
      '#attributes' => array('class' => array('element-invisible')),
    );

    $elements['nav']['#prefix'] = '<nav role="navigation" id="user-navigation" class="user-navigation">';
    $elements['nav']['#suffix'] = '</nav>';

    $elements['nav']['links'] = array(
      '#theme' => 'links__enhanced',
      '#links' => $links,
      '#links_prefix' => 'action-link',
      '#attributes' => array(
        'class' => array(
          'header-links',
          'links',
          'clearfix',
        )
      ),
    );
  }

  // Render the HTML.
  $output = drupal_render($elements);

  // Remove whitespace between items. Helps with styling in-line elements.
  return preg_replace('/\>\s+\</', '><', $output);
}

/**
 * Returns HTML for the header logo.
 */
function theme_header_logo() {
  global $theme;

  $elements = array(
    '#type' => 'container',
    '#attributes' => array(
      'class' => array(
        'site-name-wrapper',
        'contextual-links-region',
      ),
    ),
  );

  // Text for homepage header logo link.
  $site_link_title = t(variable_get('site_link_title', 'Visit the main page'));
  $site_name = filter_xss_admin(variable_get('site_name', 'Drupal'));

  // Initialize the template variable as a renderable array.
  $elements['contextual_links'] = array(
    '#type' => 'contextual_links',
    '#contextual_links' => array(
      'menu' => array('admin/settings/generic', array('edit')),
    ),
  );

  $elements['header']['#prefix'] = '<h1 class="site-name">';
  $elements['header']['#suffix'] = '</h1>';

  $elements['header']['link'] = array(
    '#prefix' => '<a href="' . url('<front>') . '" title="' . $site_link_title . '" class="link" rel="home">',
    '#suffix' => "</a>",
  );

  // Replace the PNG logo with a SVG one.
  //$items['header']['link']['svg'] = array(
  //  '#theme' => 'image',
  //  '#path' => url(drupal_get_path('theme', $theme) . '/logo.svg'),
  //  '#alt' => $site_name,
  //  '#attributes' => array(
  //    'role' => 'presentation',
  //  ),
  //);
  $elements['header']['link']['png'] = array(
    '#theme' => 'image',
    '#path' => url(drupal_get_path('theme', $theme) . '/logo.png'),
    '#alt' => $site_name,
  );

  // Render the HTML.
  return drupal_render($elements);
}

/**
 * Returns HTML for main navigation menu.
 */
function theme_main_navigation() {
  $menu_elements = array(
    '#type' => 'container',
    '#attributes' => array(
      'class' => array(
        'navigation-toplevel',
      ),
    ),
  );

  // Initialize the template variable as a render-able array.
  $menu_elements['contextual_links'] = array(
    '#type' => 'contextual_links',
    '#contextual_links' => array(
      'menu' => array('admin/structure/menu/manage', array('main-menu')),
    ),
  );
  // Mark this element as potentially having contextual links attached to it.
  $menu_elements['#attributes']['class'][] = 'contextual-links-region';


  $menu_elements['header'] = array(
    '#type' => 'html_tag',
    '#tag' => 'h2',
    '#value' => 'Main Menu',
    '#attributes' => array('class' => array('element-invisible')),
  );

  $menu_tree_callback = (function_exists('i18n_menu_translated_tree')) ? 'i18n_menu_translated_tree' : 'menu_tree';
  $menu_elements['navigation'] = $menu_tree_callback('main-menu');

  // Change the wrapper class.
  $menu_elements['navigation']['#theme_wrappers'] = array('menu_tree__main_menu__toplevel');

  // Render the menu.
  $output = render($menu_elements);

  // Remove whitespace between items. Helps with styling in-line elements.
  return preg_replace('/\>[\n\ ]+\</', '><', $output);
}

/**
 * Returns HTML for the svg icons.
 */
function theme_svg_sprite(array $elements) {
  if (empty($elements['symbol'])) {
    return NULL;
  }

  // Provide a fallback sprite.
  if (empty($elements['sprite_name'])) {
    $elements['sprite_name'] = 'icons';
  }

  // Set the sprite path.
  $sprite_file_name = $elements['sprite_name'] . '.svg';
  $elements['path'] = drupal_get_path('module', 'helpertheme') . '/sprites/' . $sprite_file_name;

  // Set a default attributes.
  foreach (array('class', 'height', 'width', 'viewBox') as $key) {
    if (array_key_exists($key, $elements) && is_null($elements[$key])) {
      continue;
    }

    if (!empty($elements[$key])) {
      $elements['#attributes'][$key] = $elements[$key];
      continue;
    }

    switch ($key) {
      case 'width':
      case 'height':
        $elements['#attributes'][$key] = '2em';
        break;
    }
  }

  // @TODO: improve accessibility.
  $elements['#attributes']['role'] = 'presentation';

  $args = array();
  if (!is_null($elements['symbol'])) {
    $args['fragment'] = $elements['symbol'];
  }
  $elements['#use']['xlink:href'] = url($elements['path'], $args);

  return '<svg' . drupal_attributes($elements['#attributes']) . '><use'. drupal_attributes($elements['#use']) . '/></svg>';
}
