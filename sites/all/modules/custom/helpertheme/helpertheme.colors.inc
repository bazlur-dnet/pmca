<?php
/**
 * @file
 * Define helper functions.
 */

/**
 * Returns an array of colors for a navigation menu.
 *
 * Variables are lazy-loaded so we can reference them when creating variants.
 *
 * NOTE: An overview can bee seen at `/admin/reports/helpertheme/colors`.
 */
function helpertheme_color_palette() {
  $colors = array();

  // Primary colors.
  $colors['primary'] = array(
    '#title' => 'Primary colors',
    '#weight' => 10,
  );

  $colors['primary']['clmain'] = array(
    '#value' => '#f3f3f3',
    '#desc' => 'page background',
    '#variants' => array(
      'clmain__separator' => array(
        '#value' => '#d7d7d7',
        '#desc' => 'page content separator',
      ),
    ),
  );

  $colors['primary']['clcontent'] = array(
    '#value' => '#ffffff',
    '#desc' => 'content background',
    '#variants' => array(
    ),
  );

  $colors['primary']['clnormal'] = array(
    '#value' => '#5e5e5e',
    '#desc' => 'normal text',
    '#variants' => array(
      'clnormal__dark' => array(
        '#value' => 'mix(#000000, @clnormal, 20%)',
        '#desc' => 'important text',
      ),
      'clnormal__light' => array(
        '#value' => 'mix(#ffffff, @clnormal, 20%)',
        '#desc' => 'important text',
      ),
    ),
  );

  $colors['primary']['cldetail'] = array(
    '#value' => '#fdb50a',
    '#desc' => 'featured content',
    '#variants' => array(
      'cldetail__alt' => array(
        '#value' => '#f89200',
        '#desc' => 'actionable content',
      ),
      'cldetail__highlighted' => array(
        '#value' => '#ff3300',
        '#desc' => 'links, highlighted items',
      ),
    ),
  );


  // Secondary colors.
  $colors['secondary'] = array(
    '#title' => 'Secondary colors',
    '#weight' => 20,
  );

  $colors['secondary']['clpassive'] = array(
    '#value' => '#b5b5b5',
    '#desc' => 'passive/disabled highlighted items',
    '#variants' => array(
    ),
  );

  $colors['secondary']['clrowactive'] = array(
    '#value' => '#eff5fe',
    '#desc' => 'linked blocks background?!?',
    '#variants' => array(
    ),
  );


  // HTML tag specific colors.
  $colors['htmltag'] = array(
    '#title' => 'HTML tag specific colors',
    '#weight' => 40,
  );

  $colors['htmltag']['clanchor'] = array(
    '#value' => '@cldetail__highlighted',
    '#desc' => 'anchor tags',
    '#variants' => array(
      'clanchor__visited' => array(
        '#value' => 'mix(#000000, @clanchor, 15%)',
        '#desc' => 'visited anchor tags',
      ),
      'clanchor__hover' => array(
        '#value' => 'mix(#000000, @clanchor, 30%)',
        '#desc' => 'focused anchor tags',
      ),
      'clanchor__active' => array(
        '#value' => 'mix(#000000, @clanchor, 45%)',
        '#desc' => 'active anchor tags',
      ),
    ),
  );


  // Notification colors.
  $colors['notification'] = array(
    '#title' => 'Notification message colors',
    '#weight' => 50,
  );

  $colors['notification']['clstatus'] = array(
    '#value' => '#6bc93f',
    '#desc' => 'success message base color',
    '#variants' => array(
      'clstatus__dark' => array(
        '#value' => 'darken(@clstatus, 40%)',
        '#desc' => 'text',
      ),
      'clstatus__alt' => array(
        '#value' => 'desaturate(@clstatus, 18)', // #73b355
        '#desc' => 'icons and box shadow',
      ),
      'clstatus__faded' => array(
        '#value' => 'lighten(@clstatus__alt, 18%)',
        '#desc' => 'box border',
      ),
      'clstatus__light' => array(
        '#value' => 'lighten(@clstatus, 40%)',
        '#desc' => 'box background',
      ),
    ),
  );

  $colors['notification']['clwarning'] = array(
    '#value' => '#d4920e',
    '#desc' => 'warning message base color',
    '#variants' => array(
      'clwarning__dark' => array(
        '#value' => 'darken(@clwarning, 24%)',
        '#desc' => 'text color',
      ),
      'clwarning__alt' => array(
        '#value' => 'saturate(@clwarning, 18)', // #e29700
        '#desc' => 'icons and box shadow',
      ),
      'clwarning__faded' => array(
        '#value' => 'lighten(@clwarning__alt, 36%)',
        '#desc' => 'box border',
      ),
      'clwarning__light' => array(
        '#value' => 'lighten(@clwarning, 52%)',
        '#desc' => 'box background',
      ),
    ),
  );

  $colors['notification']['clerror'] = array(
    '#value' => '#d53615',
    '#desc' => 'error message base color',
    '#variants' => array(
      'clerror__dark' => array(
        '#value' => 'darken(@clerror, 24%)',
        '#desc' => 'text color',
      ),
      'clerror__alt' => array(
        '#value' => 'saturate(@clerror, 18)', // #ea2800
        '#desc' => 'icons and box shadow',
      ),
      'clerror__faded' => array(
        '#value' => 'lighten(@clerror__alt, 36%)',
        '#desc' => 'box border',
      ),
      'clerror__light' => array(
        '#value' => 'lighten(@clerror, 50%)',
        '#desc' => 'box background',
      ),
    ),
  );

  return $colors;
}
