<?php

/**
 */
class ProviderMetadataController extends EntityDefaultMetadataController {
  public function entityPropertyInfo() {
    $info = parent::entityPropertyInfo();

    foreach ($info['provider'] as $bundle => &$bundle_info) {
      $bundle_info['name']['label'] = 'Name';

      if (isset($bundle_info['status'])) {
        $bundle_info['status']['type'] = 'boolean';
      }
      if (isset($bundle_info['uid'])) {
        $bundle_info['uid']['label'] = 'User ID';
      }
    }

    return $info;
  }
}
