<?php

/**
 * @file
 * Defines the class for the Service Provider Entity.
 */

/**
 * Main class for Service Provider entities.
 */
class ProviderEntity extends Entity {
  // Define some default values.
  public $pid = NULL;
  public $name = '';
  public $language = LANGUAGE_NONE;
  public $type = '';
  public $uid = NULL;
  public $status = TRUE;
  public $created = NULL;
  public $changed = NULL;

  /**
   * URI method for entity_class_uri().
   */
  protected function defaultUri() {
    if (!isset($this->pid)) {
      return NULL;
    }

    return array(
      'path' => MCAENTITY_PROVIDER_UI_PATH . '/' . $this->identifier(),
    );
  }
}
